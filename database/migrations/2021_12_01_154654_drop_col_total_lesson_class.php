<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColTotalLessonClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_class_students', function (Blueprint $table) {
            DB::statement('ALTER TABLE tutor_class_students DROP COLUMN total_lesson_class;');
            DB::statement('ALTER TABLE tutor_class_students ADD COLUMN program_id INT(11) DEFAULT 0;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
