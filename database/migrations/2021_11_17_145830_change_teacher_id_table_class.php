<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTeacherIdTableClass extends Migration
{
    protected $table = 'tutor_class';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE teacher_id teacher_id  INT(11) NULL DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE teacher_id teacher_id  INT(11) NOT NULL;');
    }
}
