<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSubjectIdTutorTeacher extends Migration
{
    protected $table = 'tutor_teacher';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::statement('ALTER TABLE ' . $this->table . ' DROP FOREIGN KEY tutor_teacher_target_subject_id_foreign;');
//        DB::statement('ALTER TABLE ' . $this->table . ' DROP INDEX tutor_teacher_target_subject_id_foreign;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE  target_subject_id target_subject_ids varchar(100);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE '.$this->table.' CHANGE  target_subject_ids target_subject_id int(11);');
//        DB::statement('ALTER TABLE '.$this->table.' ADD CONSTRAINT tutor_teacher_target_subject_id_foreign FOREIGN KEY (target_subject_id) REFERENCES coc_dm_mon_hoc(id);');
    }
}
