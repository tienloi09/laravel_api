<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ChangeRateTableTutorClassLesson extends Migration
{
    protected $table = 'tutor_class_lesson';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `total_rate_point` `total_rate_point` INTEGER DEFAULT 0;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `total_num_of_rate` `total_num_of_rate` INTEGER DEFAULT 0;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `total_rate_point` `total_rate_point` INTEGER NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `total_num_of_rate` `total_num_of_rate` INTEGER NULL DEFAULT NULL;');
    }
}
