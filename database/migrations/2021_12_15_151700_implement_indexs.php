<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImplementIndexs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('core_user_device', function (Blueprint $table) {
            $table->index('user_id');
        });

        Schema::table('tutor_announcement', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('status');
        });

        Schema::table('tutor_class', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('status');
        });

        Schema::table('tutor_class_lesson', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('tutor_program_lesson_id');
        });

        Schema::table('tutor_class_lesson_section', function (Blueprint $table) {
            $table->index('deleted');
        });

        Schema::table('tutor_class_program', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('apply_end_datetime');
        });

        Schema::table('tutor_class_schedule', function (Blueprint $table) {
            $table->index('deleted');
        });

        Schema::table('tutor_class_students', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('program_id');
        });

        Schema::table('tutor_mission_completed', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('status');
        });

        Schema::table('tutor_prio_box', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('status');
        });

        Schema::table('tutor_product', function (Blueprint $table) {
            $table->index('deleted');
        });

        Schema::table('tutor_program', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('status');
            $table->index('product_code');
        });

        Schema::table('tutor_program_lesson', function (Blueprint $table) {
            $table->index('deleted');
        });

        Schema::table('tutor_program_lesson_section', function (Blueprint $table) {
            $table->index('deleted');
        });

        Schema::table('tutor_rating_comment', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('status');
        });

        Schema::table('tutor_result', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('lms_id');
            $table->index('lesson_section_id');
        });

        Schema::table('tutor_student', function (Blueprint $table) {
            $table->index('deleted');
        });

        Schema::table('tutor_teacher', function (Blueprint $table) {
            $table->index('deleted');
            $table->index('status');
            $table->index('phone');
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
