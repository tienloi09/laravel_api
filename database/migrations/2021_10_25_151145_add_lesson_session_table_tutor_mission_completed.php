<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLessonSessionTableTutorMissionCompleted extends Migration
{
    protected $table = 'tutor_mission_completed';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function ($table) {
            $table->integer('lesson_section_id');
            $table->foreign('lesson_section_id')->references('id')->on('tutor_program_lesson_section')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function ($table) {
            $table->dropColumn('lesson_section_id');
        });
    }
}
