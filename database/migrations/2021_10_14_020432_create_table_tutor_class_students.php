<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorClassStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_class_students', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('class_id');
            $table->foreign('class_id')->references('id')->on('tutor_class')->onDelete('cascade');
            $table->integer('student_id');
            $table->foreign('student_id')->references('id')->on('tutor_student')->onDelete('cascade');
            $table->integer('total_num_join_lesson');
            $table->integer('total_lesson_class');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_class_students');
    }
}
