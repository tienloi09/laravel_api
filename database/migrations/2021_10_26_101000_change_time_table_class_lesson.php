<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTimeTableClassLesson extends Migration
{
    protected $table = 'tutor_class_lesson';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `lesson_start_time` `lesson_start_time` DATETIME NOT NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `lesson_end_time` `lesson_end_time` DATETIME NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `lesson_start_time` `lesson_start_time` DATE NOT NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `lesson_end_time` `lesson_end_time` DATE NOT NULL;');
    }
}
