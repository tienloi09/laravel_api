<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnTableTutorClassLessonSection extends Migration
{
    protected $table = 'tutor_class_lesson_section';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE image image VARCHAR(100) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE software_used software_used VARCHAR(100) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE `desc` `desc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE lms_id lms_id INT(11) NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE link_study link_study VARCHAR(100) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE start_time start_time DATETIME NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE end_time end_time DATETIME NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE image image VARCHAR(100) NOT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE software_used software_used VARCHAR(100) NOT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE `desc` `desc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL NOT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE lms_id lms_id INT(11) NOT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE link_study link_study VARCHAR(100) NOT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE start_time start_time DATETIME NOT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE end_time end_time DATETIME NOT NULL;');
    }
}
