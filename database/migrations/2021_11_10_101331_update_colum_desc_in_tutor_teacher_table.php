<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumDescInTutorTeacherTable extends Migration
{
    protected $table = 'tutor_teacher';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_teacher', function (Blueprint $table) {
            DB::statement('ALTER TABLE ' . $this->table . ' CHANGE `desc` `desc` VARCHAR(200) DEFAULT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_teacher', function (Blueprint $table) {
            DB::statement('ALTER TABLE ' . $this->table . ' CHANGE `desc` `desc` VARCHAR(200) NOT NULL;');
        });
    }
}
