<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeImageTutorTeacher extends Migration
{
    protected $table = 'tutor_teacher';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `image` `image` varchar(100)  NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `image` `image` varchar(100) NOT NULL;');
    }
}
