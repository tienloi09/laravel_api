<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorClassSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_class_schedule', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('class_id');
            $table->foreign('class_id')->references('id')->on('tutor_class')->onDelete('cascade');
            $table->integer('day_of_week');
            $table->dateTime('start_time');
            $table->dateTime('end_time');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_class_schedule');
    }
}
