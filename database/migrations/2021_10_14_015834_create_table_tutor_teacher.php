<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorTeacher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_teacher', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('name',50);
            $table->integer('level');
            $table->string('target_grade_ids',100);
            $table->integer('target_subject_id');
//            $table->foreign('target_subject_id')->references('id')->on('coc_dm_mon_hoc')->onDelete('cascade');
            $table->integer('status');
            $table->string('desc',200);
            $table->float('total_rate_point');
            $table->integer('total_rate_time');
            $table->string('image',100);
            $table->integer('total_num_of_lesson');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_teacher');
    }
}
