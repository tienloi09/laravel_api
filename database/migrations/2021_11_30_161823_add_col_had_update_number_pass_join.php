<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColHadUpdateNumberPassJoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_class_lesson_section', function (Blueprint $table) {
            DB::statement('ALTER TABLE tutor_class_lesson_section ADD COLUMN had_update_num_of_pass_lesson TINYINT DEFAULT 0;');
            DB::statement('ALTER TABLE tutor_class_lesson_section ADD COLUMN had_update_num_of_join_student TINYINT DEFAULT 0;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
