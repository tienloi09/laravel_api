<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeColumsAvatarToTutorPrioBoxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `tutor_prio_box` CHANGE `avatar_url` `avatar_url` VARCHAR(300) DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `tutor_prio_box` CHANGE `avatar_url` `avatar_url` VARCHAR(300) NOT NULL;');
    }
}
