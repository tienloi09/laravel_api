<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumsToTutorPrioBoxTable extends Migration
{
    protected $table = 'tutor_prio_box';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE start_date start_date DATETIME DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE end_date end_date DATETIME DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_grade_id target_grade_id VARCHAR(255) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_subject_id target_subject_id VARCHAR(255) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_class_id target_class_id VARCHAR(255) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE level level INTEGER DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_user_id target_user_id VARCHAR(255) DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE start_date start_date DATETIME DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE end_date end_date DATETIME DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_grade_id target_grade_id VARCHAR(255) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_subject_id target_subject_id VARCHAR(255) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_class_id target_class_id VARCHAR(255) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE level level INTEGER DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_user_id target_user_id VARCHAR(255) DEFAULT NULL;');
    }
}
