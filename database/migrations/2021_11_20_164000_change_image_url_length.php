<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeImageUrlLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `tutor_announcement` CHANGE `image` `image` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_class` CHANGE `logo` `logo` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_class` CHANGE `logo_thumb` `logo_thumb` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_class_lesson_section` CHANGE `image` `image` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_program_lesson_section` CHANGE `image` `image` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_teacher` CHANGE `image` `image` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `tutor_announcement` CHANGE `image` `image` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_class` CHANGE `logo` `logo` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_class` CHANGE `logo_thumb` `logo_thumb` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_class_lesson_section` CHANGE `image` `image` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_program_lesson_section` CHANGE `image` `image` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_teacher` CHANGE `image` `image` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
    }
}
