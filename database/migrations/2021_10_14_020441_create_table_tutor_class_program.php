<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorClassProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_class_program', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('class_id');
            $table->foreign('class_id')->references('id')->on('tutor_class')->onDelete('cascade');
            $table->integer('program_id');
            $table->foreign('program_id')->references('id')->on('tutor_program')->onDelete('cascade');
            $table->date('apply_start_datetime');
            $table->date('apply_end_datetime')->nullable();
            $table->integer('lesson_start_at');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_class_program');
    }
}
