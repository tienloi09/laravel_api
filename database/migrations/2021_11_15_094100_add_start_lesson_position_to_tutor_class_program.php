<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStartLessonPositionToTutorClassProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_class_program', function (Blueprint $table) {
            $table->integer('start_lesson_position')->nullable();
        });
        Schema::table('tutor_class_lesson', function (Blueprint $table) {
            $table->integer('is_changed_parent')->nullable();
        });
        Schema::disableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_class_program', function (Blueprint $table) {
            $table->dropColumn('start_lesson_position');
        });
        Schema::table('tutor_class_lesson', function (Blueprint $table) {
            $table->dropColumn('is_changed_parent');
        });
        Schema::disableForeignKeyConstraints();
    }
}
