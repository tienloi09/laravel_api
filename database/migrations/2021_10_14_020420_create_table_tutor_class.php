<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_class', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('title',200);
            $table->integer('target_grade_id');
            $table->foreign('target_grade_id')->references('id')->on('coc_dm_khoi')->onDelete('cascade');
            $table->integer('target_subject_id');
            $table->foreign('target_subject_id')->references('id')->on('coc_dm_mon_hoc')->onDelete('cascade');
            $table->integer('level');
            $table->integer('status');
            $table->integer('teacher_id');
            $table->foreign('teacher_id')->references('id')->on('tutor_teacher')->onDelete('cascade');
            $table->integer('slot');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('num_of_lesson');
            $table->integer('num_of_lesson_passed');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_class');
    }
}
