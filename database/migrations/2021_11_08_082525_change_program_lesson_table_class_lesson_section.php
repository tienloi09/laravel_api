<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeProgramLessonTableClassLessonSection extends Migration
{
    protected $table = 'tutor_class_lesson_section';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropForeign(['program_lesson_section_id']);
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table($this->table, function (Blueprint $table) {
            $table->foreign('program_lesson_section_id')
                ->references('id')
                ->on('tutor_program_lesson_section')
                ->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }
}
