<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeImageTutorProgramLesson extends Migration
{
    protected $table = 'tutor_program_lesson_section';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `image` `image` varchar(100)  NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `link_study` `link_study` varchar(100)  NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `lms_id` `lms_id` int NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `start_time` `start_time` DATETIME NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `end_time` `end_time` DATETIME NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `image` `image` varchar(100) NOT NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `link_study` `link_study` varchar(100) NOT NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `lms_id` `lms_id` int NOT NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `start_time` `start_time` DATETIME NOT NULL;');
        DB::statement('ALTER TABLE `'.$this->table.'` CHANGE COLUMN `end_time` `end_time` DATETIME NOT NULL;');
    }
}
