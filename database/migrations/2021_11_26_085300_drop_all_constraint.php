<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropAllConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement('ALTER TABLE tutor_class CHANGE slot slot INT(11) NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE tutor_announcement DROP FOREIGN KEY tutor_announcement_teacher_id_foreign;');
        DB::statement('ALTER TABLE tutor_class DROP FOREIGN KEY tutor_class_target_grade_id_foreign;');
        DB::statement('ALTER TABLE tutor_class DROP FOREIGN KEY tutor_class_target_subject_id_foreign;');
        DB::statement('ALTER TABLE tutor_class DROP FOREIGN KEY tutor_class_teacher_id_foreign;');
        DB::statement('ALTER TABLE tutor_class_lesson DROP FOREIGN KEY tutor_class_lesson_class_id_foreign;');
        DB::statement('ALTER TABLE tutor_class_lesson_section DROP FOREIGN KEY tutor_class_lesson_section_lesson_id_foreign;');
        DB::statement('ALTER TABLE tutor_class_program DROP FOREIGN KEY tutor_class_program_class_id_foreign;');
        DB::statement('ALTER TABLE tutor_class_program DROP FOREIGN KEY tutor_class_program_program_id_foreign;');
        DB::statement('ALTER TABLE tutor_class_schedule DROP FOREIGN KEY tutor_class_schedule_class_id_foreign;');
        DB::statement('ALTER TABLE tutor_class_students DROP FOREIGN KEY tutor_class_students_class_id_foreign;');
        DB::statement('ALTER TABLE tutor_class_students DROP FOREIGN KEY tutor_class_students_student_id_foreign;');
        DB::statement('ALTER TABLE tutor_mission_completed DROP FOREIGN KEY tutor_mission_completed_class_id_foreign;');
        DB::statement('ALTER TABLE tutor_mission_completed DROP FOREIGN KEY tutor_mission_completed_lesson_id_foreign;');
        DB::statement('ALTER TABLE tutor_mission_completed DROP FOREIGN KEY tutor_mission_completed_lesson_section_id_foreign;');
        DB::statement('ALTER TABLE tutor_mission_completed DROP FOREIGN KEY tutor_mission_completed_student_id_foreign;');
        DB::statement('ALTER TABLE tutor_program DROP FOREIGN KEY tutor_program_target_grade_id_foreign;');
        DB::statement('ALTER TABLE tutor_program DROP FOREIGN KEY tutor_program_target_subject_id_foreign;');
        DB::statement('ALTER TABLE tutor_program_lesson DROP FOREIGN KEY tutor_program_lesson_program_id_foreign;');
        DB::statement('ALTER TABLE tutor_program_lesson_section DROP FOREIGN KEY tutor_program_lesson_section_lesson_id_foreign;');
        DB::statement('ALTER TABLE tutor_program_lesson_section DROP FOREIGN KEY tutor_program_lesson_section_program_id_foreign;');
        DB::statement('ALTER TABLE tutor_rating_comment DROP FOREIGN KEY tutor_rating_comment_class_id_foreign;');
        DB::statement('ALTER TABLE tutor_rating_comment DROP FOREIGN KEY tutor_rating_comment_lesson_id_foreign;');
        DB::statement('ALTER TABLE tutor_rating_comment DROP FOREIGN KEY tutor_rating_comment_student_id_foreign;');
        DB::statement('ALTER TABLE tutor_result DROP FOREIGN KEY tutor_result_class_id_foreign;');
        DB::statement('ALTER TABLE tutor_result DROP FOREIGN KEY tutor_result_lesson_id_foreign;');
        DB::statement('ALTER TABLE tutor_result DROP FOREIGN KEY tutor_result_student_id_foreign;');
        DB::statement('ALTER TABLE tutor_student DROP FOREIGN KEY tutor_student_lms_core_user_id_foreign;');
        Schema::disableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE tutor_class CHANGE slot slot INT(11) NOT NULL;');
    }
}
