<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColScoreIsCorrectTutorResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_result', function (Blueprint $table) {
            DB::statement('ALTER TABLE tutor_result DROP COLUMN score;');
            DB::statement('ALTER TABLE tutor_result DROP COLUMN is_correct;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
