<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProgramLessonIdToTutorClassLesson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_class_lesson', function (Blueprint $table) {
            $table->integer('tutor_program_lesson_id')->nullable();
        });
        Schema::disableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_class_lesson', function (Blueprint $table) {
            $table->dropColumn('tutor_program_lesson_id');
        });
        Schema::disableForeignKeyConstraints();
    }
}
