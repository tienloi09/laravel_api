<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColSectionLmsDuration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_program_lesson_section', function (Blueprint $table) {
            $table->string('lms_duration', 20)->nullable();
        });
        Schema::table('tutor_class_lesson_section', function (Blueprint $table) {
            $table->string('lms_duration', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
