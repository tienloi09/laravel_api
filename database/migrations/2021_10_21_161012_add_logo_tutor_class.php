<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogoTutorClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_class', function (Blueprint $table) {
            $table->string('logo', 128)->nullable();
            $table->string('logo_thumb', 128)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_class', function($table) {
            $table->dropColumn('logo');
            $table->dropColumn('logo_thumb');
        });
    }
}
