<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_result', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('class_id');
            $table->foreign('class_id')->references('id')->on('tutor_class')->onDelete('cascade');
            $table->integer('lesson_id');
            $table->foreign('lesson_id')->references('id')->on('tutor_class_lesson')->onDelete('cascade');
            $table->integer('student_id');
            $table->integer('score');
            $table->integer('is_correct');
            $table->foreign('student_id')->references('id')->on('tutor_student')->onDelete('cascade');
            $table->integer('lms_id');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_result');
    }
}
