<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorPrioBox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_prio_box', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('title',200);
            $table->integer('type');
            $table->integer('status');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('target_grade_id');
            $table->string('target_subject_id');
            $table->string('target_class_id');
            $table->integer('level');
            $table->string('target_user_id');
            $table->integer('teacher_id');
            $table->string('content',500);
            $table->string('avatar_url',300);

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_prio_box');
    }
}
