<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSoftwareUsedTutorProgramLessonSection extends Migration
{
    protected $table = 'tutor_program_lesson_section';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE software_used software_used VARCHAR(100) DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_program_lesson_section` CHANGE `desc` `desc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE software_used software_used VARCHAR(100) NOT NULL;');
        DB::statement('ALTER TABLE `tutor_program_lesson_section` CHANGE `desc` `desc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL NOT NULL;');
    }
}
