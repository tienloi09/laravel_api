<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorProgramLessonSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_program_lesson_section', function (Blueprint $table) {

            $table->integer('id',true);
            $table->integer('program_id');
            $table->foreign('program_id')->references('id')->on('tutor_program')->onDelete('cascade');
            $table->integer('lesson_id');
            $table->foreign('lesson_id')->references('id')->on('tutor_class_lesson')->onDelete('cascade');
            $table->integer('type');
            $table->string('title',200);
            $table->string('image',100);
            $table->integer('position');
            $table->string('software_used',100);
            $table->text('desc');
            $table->integer('lms_id');
            $table->integer('is_required');
            $table->string('link_study',100);
            $table->datetime('start_time');
            $table->datetime('end_time');

            loadMigrationDefault($table);
        });

}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_program_lesson_section');
    }
}
