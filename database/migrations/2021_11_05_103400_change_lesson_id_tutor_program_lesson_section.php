<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeLessonIdTutorProgramLessonSection extends Migration
{
    protected $table = 'tutor_program_lesson_section';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_program_lesson_section', function (Blueprint $table) {
        $table->dropForeign(['lesson_id']);

        $table->foreign('lesson_id')
            ->references('id')
            ->on('tutor_program_lesson')
            ->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_program_lesson_section', function (Blueprint $table) {
        $table->dropForeign(['lesson_id']);

        $table->foreign('lesson_id')
            ->references('id')
            ->on('tutor_class_lesson')
            ->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }
}
