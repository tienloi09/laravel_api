<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorProgramLesson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_program_lesson', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('program_id');
            $table->foreign('program_id')->references('id')->on('tutor_program')->onDelete('cascade');
            $table->string('title',200);
            $table->string('desc',200);
            $table->integer('position');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_program_lesson');
    }
}
