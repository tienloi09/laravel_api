<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLmsNumQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_class_lesson_section', function (Blueprint $table) {
            $table->integer('lms_num_question')->default(0);
        });
        Schema::table('tutor_program_lesson_section', function (Blueprint $table) {
            $table->integer('lms_num_question')->default(0);
        });
        Schema::table('tutor_result', function (Blueprint $table) {
            $table->integer('lesson_section_id')->default(0);
        });
        Schema::disableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tutor_class_lesson_section', function (Blueprint $table) {
            $table->dropColumn('lms_num_question');
        });
        Schema::table('tutor_program_lesson_section', function (Blueprint $table) {
            $table->dropColumn('lms_num_question');
        });
        Schema::table('tutor_result', function (Blueprint $table) {
            $table->dropColumn('lesson_section_id');
        });
        Schema::disableForeignKeyConstraints();
    }
}
