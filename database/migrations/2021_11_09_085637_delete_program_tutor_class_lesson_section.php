
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteProgramTutorClassLessonSection extends Migration
{
    protected $table = 'tutor_class_lesson_section';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropColumn('program_lesson_section_id');

        });
        Schema::disableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table($this->table, function (Blueprint $table) {
            $table->integer('program_lesson_section_id')->nullable();
        });
        Schema::disableForeignKeyConstraints();
    }
}