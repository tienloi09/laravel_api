<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableTutorStudents extends Migration
{
    protected $table = 'tutor_student';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_grade_ids target_grade_ids VARCHAR(100) DEFAULT NULL;');
        DB::statement('ALTER TABLE ' . $this->table . ' CHANGE target_subject_ids target_subject_ids VARCHAR(100) DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
