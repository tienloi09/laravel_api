<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_student', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('lms_core_user_id');
            $table->integer('level');
            $table->string('target_grade_ids',100);
            $table->string('target_subject_ids',100);

            loadMigrationDefault($table);
            $table->foreign('lms_core_user_id')->references('id')->on('core_user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_student');
    }
}
