<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorClassLessonSection extends Migration
{
    protected $table = 'tutor_class_lesson_section';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->integer('program_lesson_section_id');
            $table->foreign('program_lesson_section_id')->references('id')->on('tutor_program_lesson_section')->onDelete('cascade');
            $table->integer('lesson_id');
            $table->foreign('lesson_id')->references('id')->on('tutor_class_lesson')->onDelete('cascade');
            $table->integer('type');
            $table->string('title',200);
            $table->string('image',100);
            $table->integer('position');
            $table->string('software_used',100);
            $table->text('desc');
            $table->integer('lms_id');
            $table->integer('is_required');
            $table->string('link_study',100);
            $table->datetime('start_time');
            $table->datetime('end_time');
            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
