<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorAnnouncement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_announcement', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('title',50);
            $table->integer('type');
            $table->integer('status');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('target_grade_ids');
            $table->text('target_subject_ids');
            $table->text('target_class_ids');
            $table->text('target_level_ids');
            $table->text('target_user_ids');
            $table->integer('teacher_id');
            $table->string('content',500);
            $table->string('image',100);
            loadMigrationDefault($table);
            $table->foreign('teacher_id')->references('id')->on('lms_teacher')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_announcement');
    }
}
