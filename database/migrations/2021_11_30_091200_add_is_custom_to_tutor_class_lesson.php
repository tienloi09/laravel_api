<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsCustomToTutorClassLesson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_class_lesson', function (Blueprint $table) {
            $table->integer('is_custom')->nullable();
            $table->dropColumn('is_changed_parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_class_lesson', function (Blueprint $table) {
            $table->integer('is_changed_parent')->nullable();
            $table->dropColumn('is_custom');
        });
    }
}
