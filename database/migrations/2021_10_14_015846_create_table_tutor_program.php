<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_program', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('name',100);
            $table->integer('target_grade_id');
            $table->foreign('target_grade_id')->references('id')->on('coc_dm_khoi')->onDelete('cascade');
            $table->integer('target_subject_id');
            $table->foreign('target_subject_id')->references('id')->on('coc_dm_mon_hoc')->onDelete('cascade');
            $table->integer('total_num_of_rate');
            $table->integer('total_rate_point');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_program');
    }
}
