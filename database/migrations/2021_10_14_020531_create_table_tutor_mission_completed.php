<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTutorMissionCompleted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_mission_completed', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('class_id');
            $table->foreign('class_id')->references('id')->on('tutor_class')->onDelete('cascade');
            $table->integer('lesson_id');
            $table->foreign('lesson_id')->references('id')->on('tutor_class_lesson')->onDelete('cascade');
            $table->integer('status');
            $table->integer('student_id');
            $table->foreign('student_id')->references('id')->on('tutor_student')->onDelete('cascade');

            loadMigrationDefault($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_mission_completed');
    }
}
