<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsProductCodeToTutorProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `tutor_product` CHANGE `product_id` `product_id` INTEGER DEFAULT NULL;');
        DB::statement('ALTER TABLE `tutor_product` ADD COLUMN `product_code` VARCHAR(255) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_product', function (Blueprint $table) {
        });
    }
}
