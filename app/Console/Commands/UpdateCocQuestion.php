<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CocQuestion;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateCocQuestion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coc_question:update {--start=}';
    // run: php artisan coc_question:update --start=1

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = $this->option('start');
        if(!file_exists(storage_path($start .'_file.csv'))) {
            echo "File not exist";
            return;
        }
        $file = fopen(storage_path($start .'_file.csv'), 'r');
        $fails = [];
        while (($line = fgetcsv($file)) !== FALSE) {
            $id = $line[0];
            $fields = (array) json_decode($line[1], true);
            
            $row = CocQuestion::select(['id', 'content', 'comment', 'answers'])->where('id', '=', $id)->first();

            $f = new \SplFileObject( storage_path('logs/'. $start. '_backup.csv'), 'a');
            $f->fputcsv($row->toArray());
            $f = null;
            
            
            if(isset($fields['content'])) {
                foreach($fields['content'] as $from_to) {
                    if($from_to['to'] != '') {
                        $row->content = str_replace($from_to['from'], $from_to['to'], $row->content);
                    } else {
                        $fails[] = [$id, 'content', $from_to['from']];
                    }
                }
            }
            if(isset($fields['comment'])) {
                foreach($fields['comment'] as $from_to) {
                    if($from_to['to'] != '') {
                        $row->comment = str_replace($from_to['from'], $from_to['to'], $row->comment);
                    } else {
                        $fails[] = [$id, 'comment', $from_to['from']];
                    }
                }
            }
            if(isset($fields['answers'])) {
                // var_dump($row->answers);
                // var_dump($fields['answers']);
                $decoded = json_decode($row->answers);
                $ans = [];
                if(!empty($decoded)) {
                    foreach($decoded as $line) {
                        $anAns = (array) $line;
                        foreach($fields['answers'] as $from_to) {
                            $html = $anAns['content'] ?? '';
                            
                            if($from_to['to'] != '') {
                                $anAns['content'] = str_replace($from_to['from'], $from_to['to'], $html);
                            } else {
                                $fails[] = [$id, 'answers', $from_to['from']];
                            }
                        }
                        
                        $ans[] = $anAns;
                    }
                }
                $row->answers = json_encode($ans);
            }
            $row->save();
            echo "updated : ". $row->id . "\n";
        }
        fclose($file);

        $file = new \SplFileObject( storage_path($start .'_fail.csv'), 'a');
        foreach ($fails as $line) {
            $file->fputcsv($line);
        }
        
    }

}
