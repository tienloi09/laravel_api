<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TutorPrioBox;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateStatusOfPrioBox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lesson:update_status_off_priobox';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currentDate = date('Y/m/d H:i:s', time());
        DB::beginTransaction();
        try {
            $currentTime = date('Y-m-d H:i:s', time());
            $all = TutorPrioBox::whereRaw('end_date < "' . $currentTime . '" and status = ' . TutorPrioBox::ACTICE)->get()->toArray();
            if (count($all) > 0) {
                foreach ($all as $notify) {
                    $endDateByItem = Carbon::parse($notify['end_date'])->format('Y/m/d H:i:s');
                    if ($endDateByItem <= $currentDate) {
                        $update = TutorPrioBox::find($notify['id']);
                        $update->update(['status' => TutorPrioBox::INACTICE]);
                    }
                }
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return -1;
        }
        return 0;
    }
}
