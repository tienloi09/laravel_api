<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CocQuestion;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CheckCocQuestion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coc_question:check {--start=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $finalCDN = '';
    protected $original = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->finalCDN = env('FINAL_CDN');
        $this->original = env('ORIGNAL_DOMAIN');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $finalCDN = $this->finalCDN;
        $start = $this->option('start');
        $all = CocQuestion::select(['id', 'content', 'comment', 'answers'])->where('id', '>=', $start)->limit(1000)->get()->toArray();
        $needReplace = [];
        $id = 0;
        foreach($all as $row) {
            $id = $row['id'];
            $fields = ['content', 'comment', 'answers'];
            foreach($fields as $field) {
                $html = $row[$field];
                if($field == 'answers') {
                    $decoded = json_decode($html);
                    if(!empty($decoded)) {
                        foreach($decoded as $line) {
                            $html = $line->content ?? '';
                            preg_match_all( '@src="([^"]+)"@' , $html, $match);
                            $images = array_pop($match);
                            foreach($images as $img) {
                                if($this->is_base64($img)) {
                                    continue 2; // thuc hien xu ly bien anh thanh file, va upload
                                }
                                $parsed = parse_url($img);
                                $host = $parsed['host'] ?? '';
                                
                                if($host != $finalCDN) {
                                    $fromTo = [
                                        'from' => $img,
                                        'to' => $this->newImageUrl($img, $id)
                                    ];
                                    $needReplace[$id][$field][] = $fromTo;
                                }
                            }
                        }
                    }
                } else {
                    preg_match_all( '@src="([^"]+)"@' , $html, $match);
                    $images = array_pop($match);
                    foreach($images as $img) {
                        if($this->is_base64($img)) {
                            continue 2; // thuc hien xu ly bien anh thanh file, va upload
                        }
                        $parsed = parse_url($img);
                        $host = $parsed['host'] ?? '';
                        
                        if($host != $finalCDN) {
                            $fromTo = [
                                'from' => $img,
                                'to' => $this->newImageUrl($img, $id)
                            ];
                            $needReplace[$id][$field][] = $fromTo;
                        }
                    }
                }
            }
        }
        
        if(count($needReplace) > 0) {
            $file = new \SplFileObject( storage_path($start .'_file.csv'), 'a');
            foreach ($needReplace as $id => $fields) {
                $line = [$id , json_encode($fields)];
                $file->fputcsv($line);
            }
        }
        echo $id;
    }

    protected function is_base64($s)
    {
        return strpos($s, 'base64,');
    }

    protected function newImageUrl($img, $id = '') 
    {
        $parsed = parse_url($img);
        $host = $parsed['host'] ?? '';
        $newUrl = '';
        if($host == '' || $host == $this->original) {
            if(strpos($img, 'module/filesystem/service/default/preview') !== false) {
                $newUrl = 'https://' . $this->original . '/' . ltrim($img, '/');
            } else {
                if($host == '') {
                    $img = 'https://' . $this->original . '/' . ltrim($img, '/');
                }
                $newUrl = $this->downloadAndUploadToS3($img);
            }
        } else {
            if($host != $this->finalCDN) {
                $newUrl = $this->downloadAndUploadToS3($img);
            }
        }
        echo $newUrl . " --- " . $id  . "\n";
        return $newUrl;
    }

    protected function downloadAndUploadToS3($file_url) 
    {
        $curlCh = curl_init();
        curl_setopt($curlCh, CURLOPT_URL, $file_url);
        curl_setopt($curlCh, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlCh, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($curlCh, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curlCh, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlCh, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
        curl_setopt($curlCh, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curlCh, CURLOPT_TIMEOUT, 3);
        // curl_setopt($curlCh, CURLOPT_VERBOSE, true);
        // curl_setopt($curlCh, CURLOPT_REFERER, 'https://www.abc.com/');
        $curlData = curl_exec ($curlCh);
        $st_code = curl_getinfo($curlCh, CURLINFO_HTTP_CODE);
        curl_close ($curlCh);
        $name = $this->normalizeString(basename($file_url));
        $name = substr($name, strlen($name)-30, strlen($name));
        
        $downloadPath = storage_path($name);
        $file = fopen($downloadPath, "w+");
        fputs($file, $curlData);
        fclose($file);

        $ret = '';

        

        if($st_code == 200){

            $linkImage = 'upload/pull/' . date('Y/m/d') . '/' . substr(md5(microtime()), 0, 6) . '_' . $name;
            $mime = mime_content_type($downloadPath);
            if(in_array($mime, [
                'image/png',
                'image/jpeg',
                'image/gif',
                'image/bmp'
            ])) {
                Storage::disk()->getDriver()->getAdapter()->getClient()->putObject([
                    'Bucket' => env('AWS_BUCKET'),
                    'Key' => $linkImage,
                    'Body' => file_get_contents($downloadPath),
                    'ContentType' => mime_content_type($downloadPath)
                ]);
                $linkImage = env('AWS_URL') . $linkImage;
                
                $ret = $linkImage;
            }
            unlink($downloadPath);
        }

        //usleep(50);
        return $ret;
    }

    public function normalizeString ($str = '')
    {
        $str = strip_tags($str); 
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        return $str;
    }
}
