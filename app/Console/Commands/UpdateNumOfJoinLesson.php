<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\TutorClassLessonSectionRepository;
use App\Repositories\TutorClassStudentRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateNumOfJoinLesson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lesson:update_num_off_join';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sRepo = app(TutorClassStudentRepository::class);
        $clsRepo = app(TutorClassLessonSectionRepository::class);
        $now = Carbon::now();
        DB::beginTransaction();
        try {
            $rows = $clsRepo->getLivestreamNotUpdateNumOfJoin($now);
            if(count($rows) > 0) {
                $toUpdateLessonIds = [];
                $toUpdateSectionIds = [];
                foreach($rows as $row) {
                    $toUpdateLessonIds[] = $row['lesson_id'];
                    $toUpdateSectionIds[] = $row['id'];
                }
                
                $sRepo->incNumOfLessonJoin($toUpdateLessonIds);
                $clsRepo->markUpdatedNumOfJoin($toUpdateSectionIds);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return -1;
        }
        return 0;
    }
}
