<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdateNumOfPassLesson::class,
        Commands\UpdateNumOfJoinLesson::class,
        Commands\UpdateStatusOfPrioBox::class,
        Commands\CheckCocQuestion::class,
        Commands\UpdateCocQuestion::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('lesson:update_num_off_pass')->everyMinute()->withoutOverlapping();
        $schedule->command('lesson:update_num_off_join')->everyMinute()->withoutOverlapping();
        $schedule->command('lesson:update_status_off_priobox')->hourly()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
