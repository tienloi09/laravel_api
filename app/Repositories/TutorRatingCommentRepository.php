<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorRatingComment;
use App\Validations\TutorRatingComment as TutorRatingCommentValidation;

class TutorRatingCommentRepository extends BaseRepository
{
    public function __construct(TutorRatingComment $tutorRatingComment, TutorRatingCommentValidation $tutorRatingCommentValidation)
    {
        $this->setModel($tutorRatingComment);
        $this->setValidate($tutorRatingCommentValidation);
    }


}
