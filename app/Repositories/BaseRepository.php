<?php

namespace App\Repositories;

class BaseRepository
{
    protected $validate;
    protected $user;
    protected $student;
    public function setUser($user){
        $this->user = $user;
    }
    public function getUser(){
        return $this->user;
    }
    public function setStudent($student){
        $this->student = $student;
    }
    public function getStudent(){
        return $this->student;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getValidate(){
        return $this->validate;
    }

    public function setValidate($validation)
    {
        $this->validate = $validation;
    }

    public function search($query = [], $columns = [])
    {
        $tableName = $this->getModel()->getTable();
        if(empty($columns)) $columns = [$tableName.'.*'];
        $builder = $this->getModel()->select($columns);
        $builder->where($tableName . '.' . getConfig('deleted_name'), getConfig('deleted_failed'));

        foreach ($query as $key => $value) {
            $builder->where($key, $value);
        }
        return $builder;
    }

    public function getListData($query = [])
    {
        $builder = $this->search($query);
        return $this->getModel()->getListRow($builder);
    }

    public function findById($id)
    {
        return $this->getModel()->findById($id);
    }

    public function create($data)
    {
        return $this->getModel()->create($data);
    }

    public function find($data)
    {
        return $this->getModel()->find($data);
    }

    public function searchQuery($query, $queryRaw = [], $columns = [])
    {
        if(empty($columns)) $columns = [$this->getModel()->qualifyColumn('*')];
        $builder = $this->getModel()->select($columns);
        foreach ($query as $item) {
            $builder->where($item['key'], $item['type'], $item['value']);
        }
        if(!empty($queryRaw)){
            foreach ($queryRaw as $valueRaw){
                $builder->whereRaw($valueRaw);

            }
        }
        return $builder;
    }
    public function save($data)
    {
        return $this->getModel()->save($data);
    }
}
