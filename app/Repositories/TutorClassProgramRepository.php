<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorClassProgram;
use App\Models\TutorClass;
use App\Validations\TutorClassProgram as TutorClassProgramValidation;

class TutorClassProgramRepository extends BaseRepository
{
    protected $tutorClass;
    public function __construct(TutorClassProgram $tutorClassProgram, TutorClassProgramValidation $tutorClassProgramValidation, TutorClass $tutorClass)
    {
        $this->setModel($tutorClassProgram);
        $this->setValidate($tutorClassProgramValidation);
        $this->tutorClass = $tutorClass;
    }

    public function getByClassAndProgram($classId, $programId)
    {
        return $this->getModel()->where('class_id', $classId)->where('program_id', $programId)->first();
    }

    public function getCurrentProgram($classId)
    {
        return $this->getModel()
        ->where('class_id', $classId)
        ->whereNull('apply_end_datetime')->first();
    }
}
