<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorClassSchedule;
use App\Models\TutorClassLesson;
use App\Models\TutorClass;
use App\Models\TutorClassStudent;
use Carbon\Carbon;

class TutorClassScheduleRepository extends BaseRepository
{
    protected $modelClassLesson;
    protected $modelClass;
    protected $modelClassStudent;
    public function __construct(TutorClassSchedule $tutorClassSchedule,TutorClassLesson $tutorClassLesson, TutorClass $tutorClass, TutorClassStudent $tutorClassStudent)
    {
        $this->setModel($tutorClassSchedule);
        $this->modelClassLesson = $tutorClassLesson;
        $this->modelClassStudent = $tutorClassStudent;
        $this->modelClass = $tutorClass;
    }

    public function getListByClassId($classId)
    {
//        $dateNow = formatDateTime('','Y-m-d H:i');
        $data = $this->search()
            ->selectRaw("DATE_FORMAT(`start_time`,'%d-%m-%Y %H:%i') as start_time,DATE_FORMAT(`end_time`,'%d-%m-%Y %H:%i') as end_time")
            ->where('class_id', $classId)
//            ->whereRaw("DATE_FORMAT(start_time,'%Y-%m-%d %H:%i') < '" . $dateNow . "' AND DATE_FORMAT(end_time,'%Y-%m-%d %H:%i') > '" . $dateNow . "'")
            ->orderBy('day_of_week')
            ->get();
        return $data;
    }
    public function getListByWeek($currentDate = null)
    {
        $currentDate = $currentDate ? $currentDate: formatDateTime('','Y/m/d');
        $dateNextWeek = Carbon::parse(strtotime($currentDate))->add(7,'day')->format('Y/m/d');

        return $this->search()
            ->join('tutor_class_lesson as tcls',function ($join) {
                $join->on('tcls.id', '=', 'tutor_class_schedule.class_id');
                whereColumnDeleted($join, 'tcls');
            })
            ->whereRaw("DATE_FORMAT(start_time,'%Y/%m/%d') >= '" . $currentDate . "' AND DATE_FORMAT(start_time,'%Y/%m/%d') < '" . $dateNextWeek . "'")
            ->orderBy('end_time', 'ASC')
            ->get();
    }

    public function insert($listData)
    {
        return TutorClassSchedule::insert($listData);
    }

    public function getCountTeachingByTeacher($idTeacher, $querySearch = '')
    {
        $modelClass = $this->modelClass;
        $modelSchedule = $this->getModel();
        return $this->search()
            ->join($modelClass->getTable(), function ($join) use ($modelClass, $modelSchedule, $idTeacher) {
                $join->on($modelClass->qualifyColumn('id'), '=', $modelSchedule->qualifyColumn('class_id'));
                $join->where($modelClass->qualifyColumn('teacher_id'), $idTeacher);
                whereColumnDeleted($join, $modelClass->getTable());
            })
            ->whereRaw($querySearch)
            ->count();
    }

    public function getCountStudyByStudent($idStudent, $querySearch = '')
    {
        $modelClass = $this->modelClass;
        $modelSchedule = $this->getModel();
        $modelClassStudent = $this->modelClassStudent;
        return $this->search()
            ->join($modelClass->getTable(), function ($join) use ($modelClass, $modelSchedule) {
                $join->on($modelClass->qualifyColumn('id'), '=', $modelSchedule->qualifyColumn('class_id'));
                whereColumnDeleted($join, $modelClass->getTable());
            })
            ->join($modelClassStudent->getTable(), function ($join) use ($modelClass, $modelClassStudent, $idStudent) {
                $join->on($modelClassStudent->qualifyColumn('id'), '=', $modelClass->qualifyColumn('id'));
                $join->where($modelClass->qualifyColumn('student_id'), $idStudent);
                whereColumnDeleted($join, $modelClass->getTable());
            })
            ->whereRaw($querySearch)
            ->count();
    }
}
