<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorProgram;
use App\Models\TutorClassProgram;
use App\Models\TutorClass;
use App\Validations\TutorProgram as TutorProgramValidation;
use Illuminate\Support\Facades\DB;

class TutorProgramRepository extends BaseRepository
{
    protected $TutorClass;
    protected $tutorClassProgram;
    protected $tutorProgram;

    public function __construct(
        TutorProgram $tutorProgram,
        TutorProgramValidation $tutorProgramValidation,
        TutorClass $tutorClass,
        TutorClassProgram $tutorClassProgram
        )
    {
        $this->setModel($tutorProgram);
        $this->setValidate($tutorProgramValidation);
        $this->tutorClass = $tutorClass;
        $this->tutorClassProgram = $tutorClassProgram;
        $this->tutorProgram = $tutorProgram;
    }

    public function getList($query)
    {
        $perPage = isset($query['perPage']) ? $query['perPage'] : getConfig('per_page');

        $builder = $this
            ->getModel()
            ->where($this->tutorProgram->qualifyColumn('deleted'), getConfig('deleted_failed'))
            ->select(
                $this->tutorProgram->qualifyColumn('id'),
                $this->tutorProgram->qualifyColumn('name'),
                $this->tutorProgram->qualifyColumn('status'),
                $this->tutorProgram->qualifyColumn('update_date'),
                $this->tutorProgram->qualifyColumn('total_num_of_rate'),
                $this->tutorProgram->qualifyColumn('total_rate_point'),
                $this->tutorProgram->qualifyColumn('target_grade_id'),
                $this->tutorProgram->qualifyColumn('target_subject_id'),
                $this->tutorProgram->qualifyColumn('level')
            );

        $builder = (isset($query['target_grade_id']))
            ? $builder->where($this->tutorProgram->qualifyColumn('target_grade_id'), '=', $query['target_grade_id'])
            : $builder;
        $builder = (isset($query['level']))
            ? $builder->where($this->tutorProgram->qualifyColumn('level'), '=', $query['level'])
            : $builder;
        $builder = (isset($query['status']))
            ? $builder->where($this->tutorProgram->qualifyColumn('status'), '=', $query['status'])
            : $builder;
        $builder = (isset($query['target_subject_id']))
            ? $builder->where($this->tutorProgram->qualifyColumn('target_subject_id'), '=', $query['target_subject_id'])
            : $builder;

        if (isset($query['name'])) {
            $builder = $builder->whereRaw("(" . $this->tutorProgram->qualifyColumn('name') . " LIKE '%" . $query['name'] . "%' OR " . $this->tutorProgram->qualifyColumn('id') . " LIKE '%" . $query['name'] . "%' )");
        }

        if (isset($query['apply_status'])) {
            $builder = ($query['apply_status'] == getConfig('program_apply_statuses.is_using'))
                ? $builder->has('classProgram')
                : $builder->doesntHave('classProgram');
        }

        $listPrograms = $builder->orderBy('id', 'DESC')->paginate($perPage);

        $listData = [];
        $tutorProgram = $this->getModel();
        $tutorClass = $this->tutorClass;
        $tutorClassProgram = $this->tutorClassProgram;

        foreach ($listPrograms as $itemProgram) {
            $item = $itemProgram->toArray();
            $item['count_using'] = $tutorClass
                ->whereIn($tutorClass->qualifyColumn('status'), [ getConfig('tutor_class.status.await.id'), getConfig('tutor_class.status.running.id') ])
                ->join($tutorClassProgram->getTable(), $tutorClassProgram->qualifyColumn('class_id'), '=', $tutorClass->qualifyColumn('id'))
                ->join($tutorProgram->getTable(), $tutorProgram->qualifyColumn('id'), '=', $tutorClassProgram->qualifyColumn('program_id'))
                ->where($tutorProgram->qualifyColumn('id'), $itemProgram->id)
                ->where($tutorClass->qualifyColumn('deleted'), getConfig('deleted_failed'))
                ->count();
            $item['count_ended'] = $this->tutorClass
                ->where($tutorClass->qualifyColumn('status'), '=', getConfig('tutor_class.status.finish.id'))
                ->join($tutorClassProgram->getTable(), $tutorClassProgram->qualifyColumn('class_id'), '=', $tutorClass->qualifyColumn('id'))
                ->join($tutorProgram->getTable(), $tutorProgram->qualifyColumn('id'), '=', $tutorClassProgram->qualifyColumn('program_id'))
                ->where($tutorProgram->qualifyColumn('id'), $itemProgram->id)
                ->where($tutorClass->qualifyColumn('deleted'), getConfig('deleted_failed'))
                ->count();

            array_push($listData, $item);
        }
        return [
            'data' => $listData,
            'total' => $listPrograms->total()
        ];
    }

    public function searchByNameOrId($query)
    {
        return $this
            ->getModel()
            ->where($this->tutorProgram->qualifyColumn('deleted'), getConfig('deleted_failed'))
            ->whereRaw("(" . $this->tutorProgram->qualifyColumn('name') . " LIKE '%" . $query['name'] . "%' OR " . $this->tutorProgram->qualifyColumn('id') . " LIKE '%" . $query['name'] . "%' )")
            ->select(
                $this->tutorProgram->qualifyColumn('id'),
                $this->tutorProgram->qualifyColumn('name'),
                $this->tutorProgram->qualifyColumn('status'),
                $this->tutorProgram->qualifyColumn('update_date'),
                $this->tutorProgram->qualifyColumn('total_num_of_rate'),
                $this->tutorProgram->qualifyColumn('total_rate_point'),
                $this->tutorProgram->qualifyColumn('target_grade_id'),
                $this->tutorProgram->qualifyColumn('target_subject_id'),
                $this->tutorProgram->qualifyColumn('level')
            )->get();
    }

    public function getClassRunning($programId)
    {
        return $this->tutorClass
            ->select('tutor_class.id', 'tutor_class.title')
            ->whereRaw('tutor_class.id IN (SELECT class_id FROM tutor_class_program WHERE program_id=' . $programId . ')')
            ->where('deleted', 0)
            ->whereIn('status', [
                TutorProgram::STATUS_WILL_RUN,
                TutorProgram::STATUS_RUNNING
            ])->get();
    }
}
