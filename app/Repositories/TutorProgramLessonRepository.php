<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorProgramLesson;
use App\Validations\TutorProgramLesson as TutorProgramLessonValidation;

class TutorProgramLessonRepository extends BaseRepository
{
    public function __construct(
        TutorProgramLesson $tutorProgramLesson,
        TutorProgramLessonValidation $tutorProgramLessonValidation
        )
    {
        $this->setModel($tutorProgramLesson);
        $this->setValidate($tutorProgramLessonValidation);
    }

    public function getListByProgram($programId)
    {
        return $this->search([ 'tutor_program_lesson.program_id' => $programId ])
            ->with('tutorProgramLessonSections')
            ->select('tutor_program_lesson.title', 'tutor_program_lesson.program_id', 'tutor_program_lesson.position', 'tutor_program_lesson.desc', 'tutor_program_lesson.id')
            ->orderBy('tutor_program_lesson.position')
            ->get();
    }
}
