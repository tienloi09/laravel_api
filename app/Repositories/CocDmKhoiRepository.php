<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CocDmKhoi;
use App\Models\TutorClass;

class CocDmKhoiRepository extends BaseRepository
{
    protected $classModel;
    public function __construct(CocDmKhoi $cocDmKhoi, TutorClass $tutorClass)
    {
        $this->setModel($cocDmKhoi);
        $this->classModel = $tutorClass;
    }

    public function getListForFilter()
    {
        $tableName = $this->getModel()->getTable();
        return $this->getModel()
            ->select($tableName.'.id', $tableName.'.name')
            ->get();
    }
}
