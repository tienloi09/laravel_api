<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CoreConfig;

class TutorCoreConfigRepository extends BaseRepository
{
    public function __construct(CoreConfig $coreConfig)
    {
        $this->setModel($coreConfig);
    }

    public function getByKey($key = null)
    {
        return $this->getModel()
            ->select('value')
            ->where('key', $key)
            ->first();
    }
}
