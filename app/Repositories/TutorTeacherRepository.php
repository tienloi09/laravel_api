<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorTeacher;
use App\Models\TutorClass;
use App\Models\CocDmKhoi2Mon;
use App\Models\CocDmKhoi;
use App\Models\CocDmMonHoc;
use App\Models\TutorClassSchedule;
use App\Models\TutorRatingComment;
use App\Models\TutorStudent;
use App\Models\TutorClassLesson;
use App\Models\CoreUser;
use App\Validations\TutorTeacher as TutorTeacherValidation;

class TutorTeacherRepository extends BaseRepository
{
    protected $classModel;
    protected $CocDmKhoi2Mon;
    protected $CocDmKhoi;
    protected $TutorStudent;
    protected $TutorRatingComment;
    protected $TutorClassLesson;
    protected $CoreUser;
    protected $tutorClass;

    public function __construct(
        TutorTeacher $tutorTeacher,
        TutorClass $tutorClass,
        CocDmKhoi2Mon $CocDmKhoi2Mon,
        CocDmKhoi $CocDmKhoi,
        TutorStudent $TutorStudent,
        TutorRatingComment $TutorRatingComment,
        TutorClassLesson $TutorClassLesson,
        CoreUser $CoreUser,
        TutorTeacherValidation $TutorTeacherValidation
    )
    {
        $this->setModel($tutorTeacher);
        $this->classModel = $tutorClass;
        $this->CocDmKhoi2Mon = $CocDmKhoi2Mon;
        $this->CocDmKhoi = $CocDmKhoi;
        $this->TutorStudent = $TutorStudent;
        $this->TutorRatingComment = $TutorRatingComment;
        $this->TutorClassLesson = $TutorClassLesson;
        $this->CoreUser = $CoreUser;
        $this->setValidate($TutorTeacherValidation);
    }

    public function getListTeacher($query)
    {
        $currentDate = formatDateTime('', 'd-m-Y H:i');
        $keySearch = $query->input('keySearch');
        $subjectIds = $query->input('subjectIds');
        $gradeIds = $query->input('gradeIds');
        $levelId = $query->input('levelId');
        $statusId = $query->input('status');
        $inClass = $query->input('inClass');
        $page = $query->input('page', 1);
        $limit = $query->input('limit', 6);
        $start = ($page - 1) * $limit;
        $where = ' 1 = 1';
        if(!empty($keySearch)) {
            $where .= ' AND ( name like "%' . $keySearch . '%" or id = "' . $keySearch . '")';
        }
        if(!empty($levelId)) {
            $where .= ' AND  level = ' . $levelId;
        }
        if($statusId !== null) {
            $where .= ' AND  status = ' . $statusId;
        }
        if(!empty($subjectIds)) {
            $where .= ' AND (';
            $expStrSubject = explode(',',$subjectIds);
            for($i = 0; $i < count($expStrSubject); $i++){
                $where .= ' target_subject_ids like "%' . $expStrSubject[$i] . '%"';
                if ($i < count($expStrSubject) - 1) {
                    $where .= ' OR ';
                }
            }
            $where .= ')';
        }
        if(!empty($gradeIds)) {
            $where .= ' AND (';
            $expStrGrade = explode(',',$gradeIds);
            for($i = 0; $i < count($expStrGrade); $i++){
                $where .= ' target_grade_ids like "%' . $expStrGrade[$i] . '%"';
                if ($i < count($expStrGrade) - 1) {
                    $where .= ' OR ';
                }
            }
            $where .= ')';
        }
        if(!empty($inClass)) {
            // value 1: đang có lớp dạy - 2: chưa có lớp dạy
            $getTeacherInClass = $this->classModel
                ->select('tutor_class.teacher_id')
                ->whereRaw('DATE_FORMAT(end_date,\'%d-%m-%Y %H:%i\') >= \'' . $currentDate . '\' <= DATE_FORMAT(start_date,\'%d-%m-%Y %H:%i\')')
                ->distinct()
                ->get();
            $listTeacherInClass = [];
            foreach($getTeacherInClass as $v) {
                array_push($listTeacherInClass, $v->teacher_id);
            }
            $key = array('[', ']');
            $arrayTeacherId = str_replace($key, '', json_encode($listTeacherInClass));
            if ($inClass == 1) {
                $where .= ' AND  id in (' . $arrayTeacherId . ')';
            } else {
                $where .= ' AND  id not in (' . $arrayTeacherId . ')';
            }
        }

        $data = $this->search()
            ->select('tutor_teacher.*')
            ->whereRaw($where)
            ->orderBy('id', 'DESC')
            ->offset($start)
            ->limit($limit)
            ->get();

        if(count($data) > 0) {
            for($i = 0 ; $i < count($data); $i++) {
                $createDate = $data[$i]['create_date'];
                $getDateJoinVuihoc = date ('m', strtotime($currentDate) - strtotime($createDate));
                $data[$i]['month_join_vh'] = (int) $getDateJoinVuihoc;
                //get info class by teacher
                $teacherId = $data[$i]['id'];
                $data[$i]['total_inclass'] = $this->classModel
                    ->select('tutor_class.id')
                    ->whereRaw('teacher_id = ' . $teacherId . ' and DATE_FORMAT(end_date,\'%d-%m-%Y %H:%i\') >= \'' . $currentDate . '\' <= DATE_FORMAT(start_date,\'%d-%m-%Y %H:%i\')')
                    ->get()
                    ->count();
                $data[$i]['total_studied_class'] = $this->classModel
                    ->select('tutor_class.id')
                    ->whereRaw('teacher_id = ' . $teacherId . ' and DATE_FORMAT(end_date,\'%d-%m-%Y %H:%i\') < \'' . $currentDate . '\'')
                    ->get()
                    ->count();
            }
        }

        $total = $this->search()
            ->select('tutor_teacher.*')
            ->whereRaw($where)
            ->get()->count();

        return [
            'data' => $data,
            'total' => $total
        ];
    }

    public function getById($teacherId)
    {
        if (!empty($teacherId)) {
            $teacher = TutorTeacher::find($teacherId);
            if ($teacher && $teacher['id'])
            {
                $tableTutorStudent = $this->TutorStudent->getTable();
                $tableTutorRatingComment = $this->TutorRatingComment->getTable();
                $tableTutorClassLesson = $this->TutorClassLesson->getTable();
                $tableCoreUser = $this->CoreUser->getTable();
                $tableClass = $this->classModel->getTable();

                $currentDate = formatDateTime(now(),'Y-m-d');
                $teacher['total_inclass'] = 0;
                $teacher['total_studied_class'] = 0;
                $listComment = array();
                $inClass = array();
                $studiedClass = array();

                $getClassByTeacher = TutorClass::select('*')->where('teacher_id' ,'=' ,$teacherId)->get()->toarray();
                for ($i = 0; $i < count($getClassByTeacher); $i++) {
                    //check schedule
                    $classId = $getClassByTeacher[$i]['id'];
                    $getSchedule = TutorClassSchedule::select('*')->where('class_id' ,'=' ,$classId)->get()->toarray();
                    $schedule = [];
                    for ($j = 0; $j < count($getSchedule) ; $j ++) {
                        $item = [];
                        $startTime = explode(" ",$getSchedule[$j]['start_time']);
                        $endTime = explode(" ",$getSchedule[$j]['end_time']);
                        $item['start_time'] = $startTime[1];
                        $item['end_time'] = $endTime[1];
                        $item['day_number'] = $getSchedule[$j]['day_of_week'];
                        array_push($schedule, $item);
                    }
                    $getClassByTeacher[$i]['schedule'] = $schedule;
                    //check lop dang mo - chua mo
                    if ($getClassByTeacher[$i]['start_date'] < $currentDate && $currentDate  < $getClassByTeacher[$i]['end_date']) {
                        $teacher['total_inclass'] += 1;
                        array_push($inClass, $getClassByTeacher[$i]);
                    }
                    if ($currentDate  > $getClassByTeacher[$i]['end_date']) {
                        $teacher['total_studied_class'] += 1;
                        array_push($studiedClass, $getClassByTeacher[$i]);
                    }
                    //get list comment by class 
                    $comment =  $this->TutorRatingComment
                        ->selectRaw($tableTutorRatingComment . '.*, cu.full_name as user_name, tcl.position, tc.title as class_name')
                        ->join($tableTutorStudent.' as ts',function ($join) use($tableTutorRatingComment){
                            $join->on('ts.id','=',$tableTutorRatingComment.'.student_id');
                            whereColumnDeleted($join, 'ts');
                        })
                        ->join($tableClass.' as tc',function ($join) use($tableTutorRatingComment){
                            $join->on('tc.id','=',$tableTutorRatingComment.'.class_id');
                            whereColumnDeleted($join, 'tc');
                        })
                        ->join($tableCoreUser.' as cu',function ($join) use($tableTutorStudent){
                            $join->on('cu.id','=','ts.lms_core_user_id');
                            whereColumnDeleted($join, 'cu');
                        })
                        ->leftjoin($tableTutorClassLesson.' as tcl',function ($join) use($tableTutorRatingComment){
                            $join->on('tcl.id','=',$tableTutorRatingComment.'.lesson_id');
                            whereColumnDeleted($join, 'tcl');
                        })
                        ->whereRaw($tableTutorRatingComment . '.class_id = ' . $classId)
                        ->orderBy('create_date', 'desc')
                        ->get();
                    if ($comment && count($comment)) {
                        for($j = 0; $j < count($comment); $j++) {
                            array_push($listComment, $comment[$j]);
                        }
                    }
                }
                $teacher['list_class'] = array(
                    "in_class" => $inClass,
                    "studied_class" => $studiedClass
                );
                $teacher['list_comment'] = $listComment;
                return $teacher;
            }
        }
    }

    public function create($query = [])
    {
        $data = new TutorTeacher;
        $data->name = isset($query['name']) ? $query['name'] : '';
        $data->gender = isset($query['gender']) ? $query['gender'] : '';
        $data->phone = isset($query['phone']) ? $query['phone'] : '';
        $data->level = isset($query['level']) ? $query['level'] : ''; 
        $data->target_grade_ids = isset($query['gradeIds']) ? $query['gradeIds'] : '';
        $data->target_subject_ids = isset($query['subjectIds']) ? $query['subjectIds'] : '';
        $data->status = isset($query['status']) ? $query['status'] : '';
        $data->desc = isset($query['desc']) ? $query['desc'] : '';
        $data->image = isset($query['avatar_url']) ? $query['avatar_url'] : '';
        $data->years_of_exp = isset($query['yearsOfExp']) ? $query['yearsOfExp'] : '';
        $data->save();

        return 1;
    }

    public function getTeacherByPhone($phone)
    {
        if ($phone) {
            $tableName = $this->getModel()->getTable();
            $getTeacher = $this->search()
                ->select($tableName.'.*')
                ->whereRaw('phone = ' . $phone)
                ->get();
            return $getTeacher;
        } else {
            return [];
        }
    }

    public function update($id, $query) 
    {
        $data = array();
        $data['name'] = isset($query['name']) ? $query['name'] : '';
        $data['gender'] = isset($query['gender']) ? $query['gender'] : '';
        $data['phone'] = isset($query['phone']) ? $query['phone'] : '';
        $data['level'] = isset($query['level']) ? $query['level'] : '';
        $data['target_grade_ids'] = isset($query['gradeIds']) ? $query['gradeIds'] : '';
        $data['target_subject_ids'] = isset($query['subjectIds']) ? $query['subjectIds'] : '';
        $data['status'] = isset($query['status']) ? $query['status'] : '';
        $data['desc'] = isset($query['desc']) ? $query['desc'] : '';
        $data['image'] = isset($query['avatar_url']) ? $query['avatar_url'] : '';
        $data['years_of_exp'] = isset($query['yearsOfExp']) ? $query['yearsOfExp'] : '';
        $update = TutorTeacher::find($id);
        $update->update($data);
        return 1;
    }

    public function getListByOrQuery($query)
    {
        $builder = $this->search()
            ->select('name', 'id', 'phone', 'image');
        if (isset($query['name'])) {
            $builder = $builder->whereRaw("(name LIKE '%" . $query['name'] . "%' OR id LIKE '%" . $query['name'] . "%'  OR phone LIKE '%" . $query['name'] . "%')");
        }

        return $builder->where('status', getConfig('tutor_teacher.status.active.id'))
            ->get();
    }
}
