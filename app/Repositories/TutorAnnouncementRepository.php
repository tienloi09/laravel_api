<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorAnnouncement;

class TutorAnnouncementRepository extends BaseRepository
{
    public function __construct(TutorAnnouncement $tutorAnnouncement)
    {
        $this->setModel($tutorAnnouncement);
    }
}
