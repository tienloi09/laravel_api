<?php
namespace App\Repositories;

use App\Http\Controllers\Crm\CrmController;
use App\Models\TutorStudent;
use App\Services\BaseLogger;
use App\Models\CoreUser;
use App\Services\TutorClassStudentService;

class MessageQueue extends CrmController
{
    public function __construct()
    {
    }

    public function insertTutorStudent($query)
    {
        // check user id in LMS
        \Log::debug(json_encode($query));
        if(empty($query['data']) || count($query['data']) == 0) {
            \Log::debug('insertTutorStudent: Data null: ' . json_encode($query));
        } else {
            $signature = md5(json_encode($query['data'])) . env('SALT_STR');
            if ($signature == $query['signature']) {
                foreach($query['data'] as $dataItem) {
                    $userLms = CoreUser::find($dataItem['lms_core_user_id']);
                    if (empty($userLms)) {
                        \Log::debug('insertTutorStudent: Chưa có tài khoản ' . $dataItem['lms_core_user_id'] . ' trên LMS');
                    } else {
                        $modelStudent = new TutorStudent;
                        $tableStudent = $modelStudent->getTable();
                        $item = $modelStudent
                            ->selectRaw($tableStudent . '.*')
                            ->whereRaw($tableStudent.'.lms_core_user_id = ' . $dataItem['lms_core_user_id'])
                            ->get();
                        if (count($item) > 0) {
                            //update data tutor_student
                            \Log::debug('insertTutorStudent: Update học viên : LMS userId: ' . $dataItem['lms_core_user_id']);
                            $dataUpdate = array();
                            if (isset($dataItem['target_grade_ids']) && $dataItem['target_grade_ids'] !== "") {
                                $implodeGradeOld = explode(',', $item[0]['target_grade_ids']);
                                $implodeGradeNew = explode(',', $dataItem['target_grade_ids']);
                                $grades = array_merge($implodeGradeOld, $implodeGradeNew);
                                $dataUpdate['target_grade_ids'] = implode(",", array_unique($grades));
                            }
                            if (isset($dataItem['target_subject_ids']) && $dataItem['target_subject_ids'] !== "") {
                                $implodeSubjectOld = explode(',', $item[0]['target_subject_ids']);
                                $implodeSubjectNew = explode(',', $dataItem['target_subject_ids']);
                                $subjects = array_merge($implodeSubjectOld, $implodeSubjectNew);
                                $dataUpdate['target_subject_ids'] = implode(",", array_unique($subjects));
                            }
                            if (isset($dataItem['product_codes']) && $dataItem['product_codes'] !== "") {
                                if ($item[0]['products'] !== "") {
                                    $listProductOld = json_decode($item[0]['products']);
                                    $idsOld = [];
                                    foreach($listProductOld as $i) {
                                        array_push($idsOld, $i->product_code);
                                    }
                                    $implodeProductNew = explode(',', $dataItem['product_codes']);
                                    $service = new TutorClassStudentService;
                                    foreach($implodeProductNew as $idNew) {
                                        if (in_array($idNew, $idsOld)) {
                                            // cộng dồn tổng số buổi học
                                            for($i = 0; $i < count($listProductOld); $i++) {
                                                if ($listProductOld[$i]->product_code == $idNew) {
                                                    $numLesson = $service->getLessonNumberByProductCode($idNew);
                                                    $listProductOld[$i]->total_lesson = (int) $listProductOld[$i]->total_lesson + $numLesson['total_lesson'];
                                                }
                                            }
                                        } else {
                                            // thêm item vào mảng
                                            $totalLesson = $service->getLessonNumberByProductCode($idNew);
                                            $listProductOld[] = (object) [
                                                "product_code" => $idNew,
                                                "total_lesson" => $totalLesson['total_lesson']
                                            ];
                                        }
                                    }
                                } else {
                                    $service = new TutorClassStudentService;
                                    $listProductOld = [];
                                    $implodeProductNew = explode(',', $dataItem['product_codes']);
                                    foreach($implodeProductNew as $idNew) {
                                        // thêm item vào mảng
                                        $totalLesson = $service->getLessonNumberByProductCode($idNew);
                                        $listProductOld[] = (object) [
                                            "product_code" => $idNew,
                                            "total_lesson" => $totalLesson['total_lesson']
                                        ];
                                    }
                                }
                                $dataUpdate['products'] = json_encode($listProductOld);
                            }
                            $update = TutorStudent::find($item[0]['id']);
                            $update->update($dataUpdate);
                            \Log::debug('insertTutorStudent: Update học viên thành công : LMS userId: ' . $dataItem['lms_core_user_id']);
                            // write log
                            // $author = apiStudentId();
                            $log = [
                                    'student_id' => (int) $item[0]['id'],
                                    'action' => 'update',
                                    'type' => 1,
                                    'create_by' => 0,
                                    'content' => 'đã cập nhật thông tin học viên'
                            ];
                            (new BaseLogger)->writeLogStudent($log);

                        } else {
                            //insert data tutor_student
                            \Log::debug('insertTutorStudent: Thêm học viên : LMS userId: ' . $dataItem['lms_core_user_id']);
                            $data = new TutorStudent;
                            $data->lms_core_user_id = $dataItem['lms_core_user_id'];
                            $data->level = 1;
                            $data->target_grade_ids = isset($dataItem['target_grade_ids']) ? $dataItem['target_grade_ids'] : "";
                            $data->target_subject_ids = $dataItem['target_subject_ids'];
                            $data->products = "";
                            //get json product register by student
                            if (isset($dataItem['product_codes']) && $dataItem['product_codes'] !== "") {
                                $productArray = array();
                                $implodeProducts = explode(',', $dataItem['product_codes']);
                                foreach ($implodeProducts as $productCode) {
                                    $item = [];
                                    $totalLesson = (new TutorClassStudentService)->getLessonNumberByProductCode($productCode);
                                    $item['product_code'] = $productCode;
                                    $item['total_lesson'] = $totalLesson['total_lesson'];
                                    array_push($productArray, $item);
                                }
                                $data->products = json_encode($productArray);
                            }
                            $data->save();
                            \Log::debug('insertTutorStudent: Thêm học viên thành công: LMS userId: ' . $dataItem['lms_core_user_id']);
                            // write log
                            $new = TutorStudent::where('lms_core_user_id', '=', $dataItem['lms_core_user_id'])->get();
                            $studentId = $new[0]['id'];
                            $log = [
                                'student_id' => (int) $studentId,
                                'action' => 'create',
                                'type' => 1,
                                'create_by' => 0,
                                'content' => 'đã thêm học viên'
                            ];
                            (new BaseLogger)->writeLogStudent($log);
                        }
                    }
                }
            } else {
                \Log::debug('insertTutorStudent: Sai signature, vui lòng kiểm tra lại');
            }
        }  
    }
}
