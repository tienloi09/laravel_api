<?php

namespace App\Repositories;

use App\Models\BaseModel;
use App\Repositories\BaseRepository;
use App\Models\TutorClassLessonSection;
use App\Validations\TutorClassLessonSection as TutorClassLessonSectionValidation;
use Illuminate\Support\Facades\DB;
use App\Models\LmsPractice;
use App\Services\PixelService;
use App\Models\Mongo\Pixel;
use Carbon\Carbon;

class TutorClassLessonSectionRepository extends BaseRepository
{
    const TYPE_SECTION_LIVESTREAM = 0;
    const IS_REQUIRED = 1;

    protected $pixelService;

    public function __construct(
        TutorClassLessonSection $tutorClassLessonSection,
        TutorClassLessonSectionValidation $tutorClassLessonSectionValidation,
        PixelService $pixelService
        )
    {
        $this->setModel($tutorClassLessonSection);
        $this->setValidate($tutorClassLessonSectionValidation);
        $this->pixelService = $pixelService;
    }

    public function getListByLesson($lessonId)
    {
        return $this->search([ 'lesson_id' => $lessonId ])
            ->select('position', 'id')
            ->get();
    }

    

    /*
     Lay danh sach cac buoi live lien truoc cua cac lop ma hoc sinh dang ki hoc
    */
    public function getMissionInRange($studentId, $prevAndNextLessonIds)
    {
        $q = $this->getModel()
        ->selectRaw('DISTINCT(cls.id), cls.title, cls.lms_id, cls.lms_num_question, 
        s.name as subject, s.id as subject_id, type, cls.lesson_id, cl.class_id')
        ->from('tutor_class_lesson_section as cls')
        ->join('tutor_class_lesson as cl', 'cls.lesson_id', '=', 'cl.id')
        ->join('tutor_class_students as cs', 'cl.class_id', '=', 'cs.class_id')
        ->join('tutor_class as c', 'cl.class_id', '=', 'c.id')
        ->join('coc_dm_mon_hoc as s', 'c.target_subject_id', '=', 's.id')
        ->join('tutor_student as st', 'cs.student_id', '=', 'st.id')
        ->leftJoin('tutor_result as re', 'cls.id', '=', 're.lesson_section_id');

        $rows = $q->where('cs.student_id', $studentId)
                ->where('cls.is_required' , self::IS_REQUIRED)
                ->where('cls.deleted' , BaseModel::UNDELETED)
                ->where('cls.type', '<>', self::TYPE_SECTION_LIVESTREAM)
                ->whereIn('cls.lesson_id', $prevAndNextLessonIds)
                // ->groupBy('cls.id')
                ->orderBy('cls.lesson_id')
                ->orderBy('cls.position')
                ->get()->toArray();
        $ret = [];
        if(count($rows) > 0) {
            $lmsIds = [];
            foreach($rows as $row) {
                $lmsIds[] = $row['lms_id'];
            }

            $lmsPracticeIds = DB::table('lms_practice_result')
                    ->select(DB::raw("id, MAX(total_correct_question) AS mx"))
                    ->where('user_id' , apiLmsCoreUserId())
                    ->whereIn('practice_id' , $lmsIds)
                    ->groupBy('practice_id')->pluck('id');

            $result = DB::table('lms_practice_result as pp')
            ->selectRaw('pp.practice_id, pp.total_correct_question, pp.total_question')
            ->whereIn('id', $lmsPracticeIds)
            ->get()->toArray();

            $kv = [];
            if(count($result) > 0) {    
                foreach($result as $r) {
                    $kv[$r->practice_id] = (array)$r;
                }
            }

            foreach($rows as $row) {
                $numCorrect = $kv[$row['lms_id']]['total_correct_question'] ?? null;
                $isDone = $numCorrect ? 1 : 0;

                if ($row['type'] == TutorClassLessonSection::TYPE_VIDEO || $row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                    $missionType = Pixel::MISSION_TYPE_VIDEO;
                    if ($row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                        $missionType = Pixel::MISSION_TYPE_DOCUMENT;
                    }
                    $item = $this->pixelService->findByParams([
                        'mission_type' => $missionType,
                        'uid' => apiUserId() . '',
                        'lesson_id' => $row['lesson_id'] . '',
                        'id' => $row['id'] . '',
                    ]);
                    if ($item) {
                        $isDone = 1;
                    }
                }

                $arrNewProperties = [
                    'lms_num_question' => $kv[$row['lms_id']]['total_question'] ?? $row['lms_num_question'],
                    'num_correct' => $kv[$row['lms_id']]['total_correct_question'] ?? null,
                    'is_done' => $isDone,
                ];

                $ret[] = array_merge($row, $arrNewProperties);
            }
        }

        return $ret;
    }

    public function getOldMissions($studentId, $subjectId, $gradeId)
    {
        $now = Carbon::now();
        $timeBefore = Carbon::now()->subDays(7);
        $q = $this->getModel()
            ->selectRaw('DISTINCT(cls.id), cls.title, cls.lms_id, cls.lms_num_question,
            s.name as subject, s.id as subject_id, type, cls.lesson_id, cl.class_id, c.target_grade_id')
            ->from('tutor_class_lesson_section as cls')
            ->join('tutor_class_lesson as cl', 'cls.lesson_id', '=', 'cl.id')
            ->join('tutor_class_students as cs', 'cl.class_id', '=', 'cs.class_id')
            ->join('tutor_class as c', 'cl.class_id', '=', 'c.id')
            ->join('coc_dm_mon_hoc as s', 'c.target_subject_id', '=', 's.id')
            ->join('tutor_student as st', 'cs.student_id', '=', 'st.id')
            ->leftJoin('tutor_result as re', 'cls.id', '=', 're.lesson_section_id');

        $rows = $q->where('cs.student_id', $studentId)
                ->where('cls.is_required' , self::IS_REQUIRED)
                ->where('cl.lesson_end_time', '<', $now->toDateTimeString())
                ->where('cl.lesson_start_time', '>', $timeBefore->toDateTimeString())
                ->where('cls.deleted' , BaseModel::UNDELETED)
                ->where('cl.deleted' , BaseModel::UNDELETED)
                ->where('cs.deleted' , BaseModel::UNDELETED)
                ->where('cls.type', '<>', self::TYPE_SECTION_LIVESTREAM);
        if ($subjectId) {
            $rows = $rows->where('c.target_subject_id', $subjectId);
        }
        if ($gradeId) {
            $rows = $rows->where('c.target_grade_id', $gradeId);
        }
        $rows = $rows->orderBy('cls.lesson_id')
            ->orderBy('cls.position')
            ->get()->toArray();
        $ret = [];
        if(count($rows) > 0) {
            $lmsIds = [];
            foreach($rows as $row) {
                $lmsIds[] = $row['lms_id'];
            }

            $subquery = DB::table('lms_practice_result')
                    ->select(DB::raw("id, MAX(total_correct_question) AS mx"))
                    ->where('user_id' , apiLmsCoreUserId())
                    ->whereIn('practice_id' , $lmsIds)
                    ->groupBy('practice_id')->pluck('id');

            $result = DB::table('lms_practice_result as pp')
            ->selectRaw('pp.practice_id, pp.total_correct_question, pp.total_question')
            ->whereIn('id', $subquery)
            ->get()->toArray();

            $kv = [];
            if(count($result) > 0) {
                foreach($result as $r) {
                    $kv[$r->practice_id] = (array)$r;
                }
            }

            foreach($rows as $row) {
                $numCorrect = $kv[$row['lms_id']]['total_correct_question'] ?? null;
                $isDone = $numCorrect ? 1 : 0;

                if ($row['type'] == TutorClassLessonSection::TYPE_VIDEO || $row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                    $missionType = Pixel::MISSION_TYPE_VIDEO;
                    if ($row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                        $missionType = Pixel::MISSION_TYPE_DOCUMENT;
                    }
                    $item = $this->pixelService->findByParams([
                        'mission_type' => $missionType,
                        'uid' => apiUserId() . '',
                        'lesson_id' => $row['lesson_id'] . '',
                        'id' => $row['id'] . '',
                    ]);
                    if ($item) {
                        $isDone = 1;
                    }
                }

                $arrNewProperties = [
                    'lms_num_question' => $kv[$row['lms_id']]['total_question'] ?? $row['lms_num_question'],
                    'num_correct' => $kv[$row['lms_id']]['total_correct_question'] ?? null,
                    'is_done' => $isDone,
                ];

                $ret[] = array_merge($row, $arrNewProperties);
            }
        }

        return $ret;
    }

    public function getListReferrences($lessonId) {
        return $this->getModel()
        ->selectRaw('cls.id, cls.title, cls.lms_id, cls.type, cls.lms_duration as duration')
        ->from('tutor_class_lesson_section as cls')
        ->where('cls.lesson_id', '=', $lessonId)
        ->where('cls.type', '<>', self::TYPE_SECTION_LIVESTREAM)
        ->where('cls.is_required', '<>', self::IS_REQUIRED)
        ->get()->toArray();
    }

    public function getLivestreamNotUpdateNumOfPass($now) {
        $fiveMinBefore = $now->clone()->subMinutes(5);
        $rows = $this->getModel()
        ->selectRaw('id, lesson_id')
        ->from('tutor_class_lesson_section as cls')
        ->where('cls.type', '=', self::TYPE_SECTION_LIVESTREAM)
        ->whereBetween('cls.start_time', [$fiveMinBefore->toDatetimeString(), $now->toDatetimeString()])
        ->where('cls.had_update_num_of_pass_lesson', '=', 0)
        ->get()->toArray();
        return $rows;
    }

    public function markUpdatedNumOfPass($ids)
    {
        $this->getModel()
        ->whereIn('id', $ids)
        ->update([
            'had_update_num_of_pass_lesson' => DB::raw('had_update_num_of_pass_lesson + 1')
        ]);
    }

    public function getLivestreamNotUpdateNumOfJoin($now) {
        $fiveMinBefore = $now->clone()->subMinutes(5);
        $rows = $this->getModel()
        ->selectRaw('id, lesson_id')
        ->from('tutor_class_lesson_section as cls')
        ->where('cls.type', '=', self::TYPE_SECTION_LIVESTREAM)
        ->whereBetween('cls.start_time', [$fiveMinBefore->toDatetimeString(), $now->toDatetimeString()])
        ->where('cls.had_update_num_of_join_student', '=', 0)
        ->get()->toArray();
        return $rows;
    }

    public function markUpdatedNumOfJoin($ids)
    {
        $this->getModel()
        ->whereIn('id', $ids)
        ->update([
            'had_update_num_of_join_student' => DB::raw('had_update_num_of_join_student + 1')
        ]);
    }

    public function getPraceticeResult($lmsId, $userId)
    {
        return (new LmsPractice)->getPracticeResult($lmsId, $userId);
    }
}
