<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorMissionCompleted;

class TutorMissionCompletedRepository extends BaseRepository
{
    public function __construct(TutorMissionCompleted $tutorMissionCompleted)
    {
        $this->setModel($tutorMissionCompleted);
    }

    public function getListByStudent($query = [])
    {
        $query['student_id'] = apiStudentId();
        $data = $this->search($query)
            ->orderBy('class_id', 'DESC')
            ->get();
        return $data;
    }

    public function getListByClass($query = [])
    {
        if (empty($query)) return [];
        return $this->search($query);
    }
}
