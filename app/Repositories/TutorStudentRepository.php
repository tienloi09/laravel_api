<?php

namespace App\Repositories;

use App\Models\BaseModel;
use App\Repositories\BaseRepository;
use App\Models\TutorStudent;
use App\Models\TutorClass;
use App\Models\CocDmMonHoc;
use App\Models\CoreUser;
use App\Models\CoreUserProfile;
use App\Models\CoreFile;
use App\Models\TutorClassStudent;
use Illuminate\Support\Facades\DB;
use App\Validations\TutorStudent as TutorStudentValidation;
use App\Models\TutorClassSchedule;
use App\Services\BaseLogger;
use App\Repositories\TutorClassLessonRepository;
use App\Repositories\TutorResultRepository;
use App\Models\TutorClassProgram;
use App\Models\TutorProgram;

class TutorStudentRepository extends BaseRepository
{
    protected $modelSubject;
    protected $CoreUser;
    protected $tutorStudent;
    protected $tutorClassStudent;
    protected $tutorClass;
    protected $CoreUserProfile;
    protected $CoreFile;
    protected $baseLogger;
    protected $tutorClassLesson;
    protected $tutorResult;
    protected $tutorClassProgram;
    protected $tutorProgram;

    public function __construct(
        TutorStudent $tutorStudent,
        CocDmMonHoc $cocDmMonHoc,
        CoreUser $CoreUser,
        TutorClassStudent $TutorClassStudent,
        TutorClass $tutorClass,
        CoreUserProfile $CoreUserProfile,
        CoreFile $CoreFile,
        TutorStudentValidation $TutorStudentValidation,
        BaseLogger $baseLogger,
        TutorClassLessonRepository $tutorClassLesson,
        TutorResultRepository $tutorResult,
        TutorClassProgram $tutorClassProgram,
        TutorProgram $tutorProgram
    )
    {
        $this->setModel($tutorStudent);
        $this->modelSubject = $cocDmMonHoc;
        $this->CoreUser = $CoreUser;
        $this->TutorClassStudent = $TutorClassStudent;
        $this->TutorClass = $tutorClass;
        $this->CoreUserProfile = $CoreUserProfile;
        $this->CoreFile = $CoreFile;
        $this->setValidate($TutorStudentValidation);
        $this->baseLogger = $baseLogger;
        $this->tutorClassLesson = $tutorClassLesson;
        $this->tutorResult = $tutorResult;
        $this->tutorClassProgram = $tutorClassProgram;
        $this->tutorProgram = $tutorProgram;
    }

    public function validate(){
        return \App\Validations\TutorStudent::class;
    }

    public function getSubjectByUser()
    {
        $listSubject = array_values(getConfig('list_subject_id'));
        $data = $this->modelSubject->select('*')
            ->where('site_id',getConfig('site_id'))
            ->whereIn('id',$listSubject)
            ->get();
        return $data;
    }

    public function getListStudentByUser($idUser){
        $student = $this->search(['lms_core_user_id' => $idUser])
                ->first();
        if(!empty($student)){
            $student->target_grade_ids = explodeMapInt($student->target_grade_ids);
            $student->target_subject_ids = explodeMapInt($student->target_subject_ids);
        }
        return $student;
    }

    public function getListStudent($query)
    {
        $keySearch = $query->input('key_search');
        $classId = $query->input('class_id');
        $subjectId = $query->input('subject_id');
        $status = $query->input('status');
        $page = $query->input('page', 1);
        $limit = $query->input('limit', 6);
        $where = ' 1 = 1';

        if(!empty($keySearch)) {
            $where .= ' AND (tutor_student.id = "' . $keySearch . '" or cu.full_name like "%' . $keySearch . '%")';
        }
        if($classId) {
            $where .= ' AND (';
            $expStrClass = explode(',',$classId);
            for($i = 0; $i < count($expStrClass); $i++){
                $where .= ' tutor_class_students.class_id = ' . $expStrClass[$i];
                if ($i < count($expStrClass) - 1) {
                    $where .= ' OR ';
                }
            }
            $where .= ')';
        }
        if($subjectId) {
            $where .= ' AND (';
            $expStrSubject = explode(',',$subjectId);
            for($i = 0; $i < count($expStrSubject); $i++){
                $where .= ' target_subject_ids like "%' . $expStrSubject[$i] . '%"';
                if ($i < count($expStrSubject) - 1) {
                    $where .= ' OR ';
                }
            }
            $where .= ')';
        }

        $listStudent = $this->getModel()
            ->selectRaw('tutor_student.*, cu.full_name, cu.mobile, cf.file_url as avatar_url')
            ->join('core_user as cu',function ($join) {
                $join->on('cu.id','=', 'tutor_student.lms_core_user_id');
            })
            ->leftjoin('core_user_profile as cup',function ($join) {
                $join->on('cup.user_id','=','cu.id');
            })
            ->leftjoin('core_file as cf',function ($join) {
                $join->on('cf.id','=','cup.image_id');
            });
        $listStudent = $listStudent->leftjoin('tutor_class_students', 'tutor_class_students.student_id', '=', 'tutor_student.id');
        if ($status) {
            switch($status) {
                case getConfig('tutor_student.value.studying'):
                    $listStudent = $listStudent->join('tutor_class', 'tutor_class.id', '=', 'tutor_class_students.class_id')
                        ->where([
                            ['tutor_class.status', getConfig('tutor_class.status.running.id')],
                            ['tutor_class.deleted', getConfig('deleted_failed')]
                        ]);
                    break;
                case getConfig('tutor_student.value.await'):
                    $listStudent = $listStudent->join('tutor_class', 'tutor_class.id', '=', 'tutor_class_students.class_id')
                        ->where([
                            ['tutor_class.status', getConfig('tutor_class.status.await.id')],
                            ['tutor_class.deleted', getConfig('deleted_failed')]
                        ]);
                    break;
                case getConfig('tutor_student.value.finished'):
                    $listStudent = $listStudent->join('tutor_class', 'tutor_class.id', '=', 'tutor_class_students.class_id')
                        ->where([
                            ['tutor_class.status', getConfig('tutor_class.status.finish.id')],
                            ['tutor_class.deleted', getConfig('deleted_failed')]
                        ]);
                    break;
                case getConfig('tutor_student.value.no_class'):
                    $listStudent = $listStudent->where('tutor_class_students.student_id', 'IS NULL');
                    break;
                default: break;
            }
        }

        $listStudent = $listStudent
            ->whereRaw($where)
            ->orderBy('id','DESC')
            ->groupBy('lms_core_user_id')
            ->paginate($limit);
        $result = [];
        $result['total'] = $listStudent->total();
        if ($listStudent) {
            for ($i = 0; $i < count($listStudent); $i++) {
                $studentId = $listStudent[$i]['id'];
                $listStudent[$i]['total_class'] = 0;
                $listStudent[$i]['total_lesson'] = $this->getLessonRegisterByStudent($studentId);
                $listStudent[$i]['total_num_join_lesson'] = 0;

                $getClassInfoByStudent = $this->TutorClassStudent
                    ->selectRaw('tutor_class_students.class_id, tutor_class_students.total_num_join_lesson')
                    ->join('tutor_class_program as tcp', function ($join) {
                        $join->on('tcp.class_id', '=', 'tutor_class_students.class_id');
                    })
                    ->join('tutor_class as tc',function ($join) {
                        $join->on('tc.id', '=', 'tutor_class_students.class_id');
                    })
                    ->whereRaw('tutor_class_students.student_id = ' . $studentId . ' and tcp.program_id = tutor_class_students.program_id and tcp.apply_end_datetime is null and tc.deleted = '. TutorClass::UNDELETED .' and tutor_class_students.deleted = '. TutorClassStudent::UNDELETED .' and tc.status = ' . TutorClass::CLASS_RUNNING)
                    ->get()->toArray();
                $listStudent[$i]['total_class'] = 0;
                $listStudent[$i]['total_num_join_lesson'] = 0;

                if ($getClassInfoByStudent) {
                    $listStudent[$i]['total_class'] = count($getClassInfoByStudent);
                    foreach($getClassInfoByStudent as $item) {
                        $listStudent[$i]['total_num_join_lesson'] += $item['total_num_join_lesson'];
                    }
                }

                $listStudent[$i]['status'] = $this->TutorClassStudent
                    ->selectRaw('tutor_class_students.class_id, tutor_class_students.student_id, tc.start_date, tc.end_date, tc.title')
                    ->join('tutor_class as tc',function ($join) {
                        $join->on('tc.id', '=', 'tutor_class_students.class_id');
                        whereColumnDeleted($join, 'tc');
                    })
                    ->whereRaw('tutor_class_students.student_id = ' . $studentId . ' and tutor_class_students.deleted = ' . TutorClassStudent::UNDELETED)
                    ->select('tc.id', 'tc.title', 'tc.status')
                    ->distinct('tutor_class_students.class_id')
                    ->get()
                    ->toarray();
            }
        }
        $listData = array();
        foreach ($listStudent as $student) {
            $item = $student->toArray();
            array_push($listData, $item);
        }
        $result['data'] = $listData;
        return $result;
    }

    public function searchForAddToClass($query)
    {
        $builder = $this->getModel()
            ->join('core_user', 'core_user.id', '=', 'tutor_student.lms_core_user_id');
        if (isset($query['name'])) {
            $builder = $builder->whereRaw("( core_user.full_name LIKE '%" . $query['name'] . "%' OR core_user.id LIKE '%" . $query['name'] . "%'  OR core_user.mobile LIKE '%" . $query['name'] . "%' OR tutor_student.id LIKE '%" . $query['name'] . "%')");
        }

        return $builder->where([
                'core_user.deleted' => getConfig('deleted_failed'),
                'tutor_student.deleted' => getConfig('deleted_failed'),
            ])
            ->selectRaw('core_user.full_name, core_user.id as core_user_id, core_user.mobile, core_user.image_url, tutor_student.id')
            ->get();
    }

    public function getById($studentId)
    {
        if (!empty($studentId)) {
            $student = TutorStudent::find($studentId);
            if ($student && $student['lms_core_user_id'])
            {
                $tableCoreUser = $this->CoreUser->getTable();
                $tableCoreUserProfile = $this->CoreUserProfile->getTable();
                $tableCoreFile = $this->CoreFile->getTable();
                $tableClassStudent = $this->TutorClassStudent->getTable();
                $tableClass = $this->TutorClass->getTable();
                $tableClassProgram = $this->tutorClassProgram->getTable();
                $currentDate = formatDateTime(now(),'Y-m-d');
                $inClass = array();
                $studiedClass = array();

                //get info user lms
                $infoLmsUser = $this->CoreUser
                    ->selectRaw($tableCoreUser.'.full_name,' . $tableCoreUser.'.mobile, cf.file_url as avatar_url, cup.birthday')
                    ->leftjoin($tableCoreUserProfile.' as cup',function ($join) use($tableCoreUser){
                        $join->on('cup.user_id','=',$tableCoreUser .'.id');
                    })
                    ->leftjoin($tableCoreFile.' as cf',function ($join) use($tableCoreUserProfile){
                        $join->on('cf.id','=','cup.image_id');
                    })
                    ->whereRaw($tableCoreUser . '.id = ' . $student['lms_core_user_id'])
                    ->get();
                $student['full_name'] = $infoLmsUser[0]['full_name'];
                $student['mobile'] = $infoLmsUser[0]['mobile'];
                $student['avatar_url'] = $infoLmsUser[0]['avatar_url'];
                $student['birthday'] = $infoLmsUser[0]['birthday'];

                // get info class by student
                $getClassInfoByStudent = $this->TutorClassStudent
                    ->selectRaw('tc.*, ' . $tableClassStudent . '.total_num_join_lesson')
                    ->join($tableClassProgram.' as tcp',function ($join) use($tableClassStudent){
                        $join->on('tcp.class_id','=',$tableClassStudent .'.class_id');
                    })
                    ->join($tableClass.' as tc',function ($join) use($tableClassStudent){
                        $join->on('tc.id','=',$tableClassStudent .'.class_id');
                    })
                    ->whereRaw($tableClassStudent . '.student_id = ' . $studentId . ' and tcp.program_id = ' . $tableClassStudent . '.program_id and tcp.apply_end_datetime is null and tc.deleted = '. TutorClass::UNDELETED .' and ' . $tableClassStudent . '.deleted = ' . TutorClassStudent::UNDELETED)
                    ->get()->toArray();

                $student['total_class'] = 0;
                $student['total_num_join_lesson'] = 0;

                $student['total_lesson'] = $this->getLessonRegisterByStudent($studentId);
                if ($getClassInfoByStudent) {
                    $student['total_class'] = count($getClassInfoByStudent);
                    foreach ($getClassInfoByStudent as $item) {
                        $student['total_num_join_lesson'] +=  $item['total_num_join_lesson'];
                    }
                    
                }

                if ($getClassInfoByStudent) {
                    for ($j = 0; $j < count($getClassInfoByStudent) ; $j++) {
                        $classId = $getClassInfoByStudent[$j]['id'];
                        $getSchedule = TutorClassSchedule::select('*')->whereRaw('class_id = ' . $classId . ' and deleted = ' . TutorClassSchedule::UNDELETED)->get()->toarray();
                        $schedule = [];
                        for ($i = 0; $i < count($getSchedule) ; $i ++) {
                            $item = [];
                            $startTime = explode(" ",$getSchedule[$i]['start_time']);
                            $endTime = explode(" ",$getSchedule[$i]['end_time']);
                            $item['start_time'] = $startTime[1];
                            $item['end_time'] = $endTime[1];
                            $item['day_number'] = $getSchedule[$i]['day_of_week'];
                            array_push($schedule, $item);
                        }
                        $getClassInfoByStudent[$j]['schedule'] = $schedule;
                        $getClassInfoByStudent[$j]['total_lesson_student_pass'] = $getClassInfoByStudent[$j]['total_num_join_lesson'];
                        //check lop dang mo - chua mo
                        if ($getClassInfoByStudent[$j]['start_date'] <= $currentDate && $currentDate  <= $getClassInfoByStudent[$j]['end_date']) {
                            // Lấy ra tổng số buổi user đã mua
                            $getClassInfoByStudent[$j]['total_lesson_register'] = self::getLessonRegisterByClass($studentId,$classId);
                            array_push($inClass, $getClassInfoByStudent[$j]);
                        }
                        if ($currentDate  > $getClassInfoByStudent[$j]['end_date']) {
                            array_push($studiedClass, $getClassInfoByStudent[$j]);
                        }
                    }
                }
                //Lấy data nhiệm vụ chưa hoàn thành
                $student['misson_completed'] = [];
                if ($inClass) {
                    $listSection = [];
                    foreach ($inClass as $class) {
                        // Lấy danh sách các học liệu thành phần của lớp
                        $sections = $this->tutorClassLesson->getListLessonFinish($class['id']);
                        if (isset($sections[0]) && $sections[0]['sections']) {
                            foreach ($sections[0]['sections'] as $section) {
                                $sectionItem = array();
                                $sectionItem['section_id'] = $section['id'];
                                $sectionItem['section_name'] = $section['title'];
                                $sectionItem['section_type'] = $section['type'];
                                $sectionItem['class_id'] = $sections[0]['class']['id'];
                                $sectionItem['class_name'] = $sections[0]['class']['title'];
                                array_push($listSection, $sectionItem);
                            }
                            // Danh sách kết quả user đã làm
                            $sectionResult = $this->tutorResult->getSectionResultByUser($class['id'], $studentId, $sections[0]['id']);
                            if ($sectionResult && count($sectionResult) > 0) {
                                foreach ($sectionResult as $r) {
                                    for($i = 0; $i < count($listSection) ; $i++) {
                                        if ($r['id'] == $listSection[$i]['section_id']) {
                                            array_splice($listSection, $i, 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $student['misson_completed'] = $listSection;
                }
                $student['list_class'] = array(
                    "in_class" => $inClass,
                    "studied_class" => $studiedClass
                );

                return $student;
            }
        }

    }

    public function update($id, $query)
    {
        // update tutor student
        $data = array();
        $avatarUrl = isset($query['avatar_url']) ? $query['avatar_url'] : '';
        $data['target_grade_ids'] = isset($query['target_grade_ids']) ? $query['target_grade_ids'] : '';
        $data['target_subject_ids'] = isset($query['target_subject_ids']) ? $query['target_subject_ids'] : '';
        $update = TutorStudent::find($id);
        $update->update($data);
        return 1;
    }

    // public static function changeNumLessonByStudent($query)
    // {
    //     $classId = $query['class_id'];
    //     $studentId = $query['student_id'];
    //     $numLessonPass = $query['number_lesson_pass'];
    //     $totalLesson = $query['total_lesson'];

    //     $data = array();
    //     $data['total_num_join_lesson'] = $numLessonPass;
    //     $data['total_lesson_class'] = $totalLesson;

    //     $update = $this->TutorClassStudent
    //         ->selectRaw($tableClassStudent .'.*')
    //         ->whereRaw($tableClassStudent . '.student_id = ' . $studentId . ' and ' . $tableClassStudent . '.class_id = ' . $classId)
    //         ->get();
    //     if ($update && count($update) > 0) {
    //         $update[0]->update($data);
    //     }
    // }

    public function getStudentsInfo($studentIds)
    {
        $modelStudent = $this->getModel();
        $modelUser = $this->CoreUser;
        return $this->getModel()
            ->join($modelUser->getTable(), $modelUser->qualifyColumn('id'), '=', $modelStudent->qualifyColumn('lms_core_user_id'))
            ->where([
                $modelStudent->qualifyColumn(getConfig('deleted_name')) => getConfig('deleted_failed'),
                $modelUser->qualifyColumn(getConfig('deleted_name')) => getConfig('deleted_failed'),
            ])
            ->whereIn($modelStudent->qualifyColumn('id'),$studentIds)
            ->select($modelStudent->qualifyColumn('id'), $modelUser->qualifyColumn('image_url'), $modelUser->qualifyColumn('full_name'))
            ->limit(getConfig('per_page'))->get();
    }

    public function setLessonForStudent($data)
    {
        $tableClass = $this->TutorClass->getTable();
        $tableClassStudent = $this->TutorClassStudent->getTable();
        $tableClassProgram = $this->tutorClassProgram->getTable();
        $tableProgram = $this->tutorProgram->getTable();
        $classStudent = TutorClassStudent::join('tutor_class_program', 'tutor_class_program.class_id', '=', 'tutor_class_students.class_id')
            ->whereNull('tutor_class_program.apply_end_datetime')
            ->WhereRaw('tutor_class_students.program_id = tutor_class_program.program_id and tutor_class_students.class_id = ' . $data['class_id'] . ' and tutor_class_students.student_id = ' . $data['student_id'] . ' and tutor_class_students.deleted = ' . TutorClassStudent::UNDELETED)
            ->select('tutor_class_students.*')
            ->first();

        if (!$classStudent) {
            return false;
        }
        $isUpdate = false;
        //update số buổi học đã học
        $classStudent->update([
            'total_num_join_lesson' => $data['lesson_pass']
        ]);
        //update số buổi học đã mua
        $product = TutorStudent::find($data['student_id']);
        $listProduct = json_decode($product['products']);

        $productsByClass = $this->TutorClassStudent
            ->selectRaw('tp.product_code')
            ->whereRaw('tutor_class_students.student_id = ' . $data['student_id'] . ' and tutor_class_students.class_id = ' . $data['class_id'])
            ->join($tableClass.' as tc',function ($join) use($tableClassStudent){
                $join->on('tc.id','=',$tableClassStudent .'.class_id');
            })
            ->join($tableClassProgram.' as tcp',function ($join) use($tableClass){
                $join->on('tcp.class_id','=','tc.id');
            })
            ->join($tableProgram.' as tp',function ($join) use($tableClassProgram){
                $join->on('tp.id','=','tcp.program_id');
            })
            ->get();
        foreach ($productsByClass as $item) {
            for($i = 0; $i < count($listProduct); $i++) {
                if ($listProduct[$i]->product_code == $item['product_code']) {
                    $listProduct[$i]->total_lesson = $data['total_lesson'];
                    $isUpdate = true;
                }
            }
            $jsonProduct = json_encode($listProduct);
            $product->update(['products' => $jsonProduct]);
        }

        if ($isUpdate == false) {
            return false;
        }
        // ghi log
        $author = apiStudentId();
        $log = [
                'student_id' => (int) $data['student_id'],
                'action' => 'update',
                'type' => 1,
                'create_by' => $author,
                'content' => 'cập nhật số buổi học của học viên - ' . $data['comment']
        ];
        $this->baseLogger->writeLogStudent($log);

        return true;
    }

    public function getLessonRegisterByClass($studentId, $classId)
    {
        $tableClass = $this->TutorClass->getTable();
        $tableClassStudent = $this->TutorClassStudent->getTable();
        $tableClassProgram = $this->tutorClassProgram->getTable();
        $tableProgram = $this->tutorProgram->getTable();

        $product = TutorStudent::find($studentId);
        $listProduct = json_decode($product['products']);

        $getProductHavedBuy = $this->TutorClassStudent
            ->selectRaw('tp.product_code')
            ->whereRaw('tutor_class_students.student_id = ' . $studentId . ' and tutor_class_students.class_id = ' . $classId)
            ->join($tableClass.' as tc',function ($join) use($tableClassStudent){
                $join->on('tc.id','=',$tableClassStudent .'.class_id');
            })
            ->join($tableClassProgram.' as tcp',function ($join) use($tableClass){
                $join->on('tcp.class_id','=','tc.id');
            })
            ->join($tableProgram.' as tp',function ($join) use($tableClassProgram){
                $join->on('tp.id','=','tcp.program_id');
            })
            ->get();
        $result = 0;
        if (isset($getProductHavedBuy) && isset($getProductHavedBuy[0]) &&  $listProduct) {
            foreach ($listProduct as $p) {
                if ($p->product_code == $getProductHavedBuy[0]['product_code']) {
                    $result = $p->total_lesson;
                }
            }
        }
        return $result;
    }

    public function getLessonRegisterByStudent($studentId)
    {
        $totalLesson = 0;
        $product = TutorStudent::find($studentId);
        $listProduct = json_decode($product['products']);
        if ($listProduct !== null) {
            foreach ($listProduct as $p) {
                $totalLesson += (int) $p->total_lesson;
            }
        }
        return $totalLesson;
    }

    public function removeGradeAndSubject($studentId, $oldTargetGradeId, $oldTargetSubjectId)
    {
        $student = $this->findById($studentId);

        $tagetGradeIdsOfStudent = $student->target_grade_ids;
        $tagetSubjectIdsOfStudent = $student->target_subject_ids;
        $tagetGradeIdsOfStudent = explode(',', $tagetGradeIdsOfStudent);
        $tagetSubjectIdsOfStudent = explode(',', $tagetSubjectIdsOfStudent);

        $classes = $this->TutorClass
            ->select('tutor_class.target_subject_id', 'tutor_class.target_grade_id')
            ->join('tutor_class_students', 'tutor_class_students.class_id', '=', 'tutor_class.id')
            ->where([
                'tutor_class_students.deleted' => BaseModel::UNDELETED,
                'tutor_class_students.student_id' => $studentId,
                'tutor_class.deleted' => $studentId,
            ])->get()->toArray();

        $tagetGradeIds = '';
        $tagetSubjectIds = '';
        foreach ($classes as $class) {
            $tagetGradeIds .= ',' . $class['target_grade_id'];
            $tagetSubjectIds .= ',' . $class['target_subject_id'];
        }

        $tagetGradeIds = explode(',', $tagetGradeIds);
        $tagetSubjectIds = explode(',', $tagetSubjectIds);
        $tagetGradeIds = array_unique($tagetGradeIds);
        $tagetSubjectIds = array_unique($tagetSubjectIds);

        if (!in_array($oldTargetGradeId, $tagetGradeIds)) {
            if (($keyGrade = array_search($oldTargetGradeId, $tagetGradeIdsOfStudent)) !== false) {
                unset($tagetGradeIdsOfStudent[$keyGrade]);
            }
        }

        if (!in_array($oldTargetSubjectId, $tagetSubjectIds)) {
            if (($keySubject = array_search($oldTargetSubjectId, $tagetSubjectIdsOfStudent)) !== false) {
                unset($tagetSubjectIdsOfStudent[$keySubject]);
            }
        }
        $student->target_grade_ids = (count($tagetGradeIdsOfStudent)) ? implode(',', $tagetGradeIdsOfStudent) : null;
        $student->target_subject_ids = (count($tagetSubjectIdsOfStudent)) ? implode(',', $tagetSubjectIdsOfStudent) : null;

        $student->save();
    }
}
