<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorClassStudent;
use App\Models\TutorClass;
use App\Models\TutorStudent;
use App\Validations\TutorClassStudent as TutorClassStudentValidation;
use Illuminate\Support\Facades\DB;

class TutorClassStudentRepository extends BaseRepository
{
    protected $tutorStudent;
    protected $tutorClass;
    public function __construct(TutorClassStudent $tutorClassStudent, TutorStudent $tutorStudent, TutorClass $tutorClass, TutorClassStudentValidation $tutorClassStudentValidation)
    {
        $this->setModel($tutorClassStudent);
        $this->tutorStudent = $tutorStudent;
        $this->tutorClass = $tutorClass;
        $this->setValidate($tutorClassStudentValidation);
    }
    public function getStudentByUser($idUser){
        $modelStudent = $this->tutorStudent->getTable();
        $dataUser = $this->search(['lms_core_user_id' => $idUser])
            ->join($modelStudent, $modelStudent.'.id', '=', $this->getModel()->getTable().'.student_id')
            ->get();
        return $dataUser;
    }

    public function getClassBySubject($subjectId = null, $gradeId = null)
    {
        
        $className = $this->tutorClass->getTable();
        $dateNow = formatDateTime(now(),'Y-m-d');
        $data = $this->search(['student_id' => apiStudentId()])
            ->join($className,function ($join) use ($className, $dateNow, $subjectId, $gradeId){
                $join->on($className.'.id','=',$this->getModel()->getTable().'.class_id');
                if(!empty($subjectId)) $join->where("target_subject_id", "=",$subjectId);
                if(!empty($gradeId)) $join->where("target_grade_id", "=", $gradeId);
            })
            ->pluck('class_id')
            ->toArray();
            
        return $data;
    }

    public function insert($data = [])
    {
        return TutorClassStudent::insert($data);
    }

    public function deleteByClassStudent($idClass, $student)
    {
        $entities = $this->search([
            'class_id' => $idClass,
            'student_id' => $student,
            'deleted' => 0
        ])->get();
        // $entity->update_date = formatDateTime('','Y-m-d H:i:s');
        // $entity->deleted = 1;
        if(count($entities) > 0) {
            foreach($entities as $entity) {
                $entity->update([
                    'update_date' =>  formatDateTime('','Y-m-d H:i:s'),
                    'deleted' => 1
                ]);
            }
        }
    }

    public function incNumOfLessonJoin($lessonIds)
    {
        $this->getModel()
        ->from('tutor_class_students as cs')
        ->join('tutor_class as c', 'cs.class_id', '=', 'c.id')
        ->join('tutor_class_lesson as cl', 'c.id', '=', 'cl.class_id')
        ->join('tutor_class_program as cp', 'c.id', '=', 'cp.class_id')
        ->whereIn('cl.id', $lessonIds)
        ->whereNull('apply_end_datetime')
        ->whereRaw('cs.program_id = cp.program_id')
        ->update([
            'total_num_join_lesson' => DB::raw('total_num_join_lesson + 1')
        ]);
    }

    public function updateProgramIdWhenUpdateStudents($studentIds, $classId, $program_id)
    {
        $this->getModel()
        ->from('tutor_class_students as cs')
        ->where(function ($q) {
            $q->whereNull('program_id')->orWhere('program_id', 0);
        })
        ->where('class_id', $classId)
        ->whereIn('student_id', $studentIds)
        ->update([
            'program_id' => $program_id
        ]);
    }

    public function getClassesOfStudent($studentId, $gradeId = null)
    {
        $data = $this->search(['student_id' => $studentId])
            ->join('tutor_class',function ($join) use ($gradeId){
                $join->on('tutor_class.id','=','tutor_class_students.class_id');
                if(!empty($gradeId)) $join->where("target_grade_id", "=", $gradeId);
            })
            ->pluck('class_id')
            ->toArray();
            
        return $data;
    }

}
