<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CoreUser;

class CoreUserRepository extends BaseRepository
{
    public function __construct(CoreUser $coreUser)
    {
        $this->setModel($coreUser);
    }
}
