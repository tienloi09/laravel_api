<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CocDmMonHoc;

class CocDmMonHocRepository extends BaseRepository
{
    public function __construct(CocDmMonHoc $cocDmMonHoc)
    {
        $this->setModel($cocDmMonHoc);
    }

    public function getListForFilter()
    {
        $model = $this->getModel();
        return $this->getModel()
            ->select($model->qualifyColumn('id'), $model->qualifyColumn('name'))
            ->whereIn($model->qualifyColumn('id'), array_values(getConfig('list_subject_id')))
            ->get();
    }
}
