<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\TutorPrioBox;
use App\Validations\TutorPrioBox as TutorPrioBoxValidation;
use App\Models\TutorClass;
use App\Models\TutorClassStudent;
use App\Repositories\CocDmMonHocRepository;
use App\Repositories\CocDmKhoiRepository;
use Carbon\Carbon;

class TutorPrioBoxRepository extends BaseRepository
{
    protected $tutorPrioBox;
    protected $tutorClass;
    protected $tutorSubject;
    protected $tutorGrade;

    public function __construct(
        TutorPrioBox $tutorPrioBox,
        TutorPrioBoxValidation $TutorPrioBoxValidation,
        TutorClass $tutorClass,
        TutorClassStudent $tutorClassStudent,
        CocDmMonHocRepository $CocDmMonHocRepository,
        CocDmKhoiRepository $CocDmKhoiRepository
    )
    {
        $this->setModel($tutorPrioBox);
        $this->setValidate($TutorPrioBoxValidation);
        $this->tutorClass = $tutorClass;
        $this->tutorClassStudent = $tutorClassStudent;
        $this->tutorSubject = $CocDmMonHocRepository;
        $this->tutorGrade = $CocDmKhoiRepository;
    }

    public function getList($query = [])
    {
        $statusId = isset($query['status']) ? $query['status'] : '';
        $startDate = isset($query['start_date']) ? $query['start_date'] : '';
        $endDate = isset($query['end_date']) ? $query['end_date'] : '';
        $page = isset($query['page']) ? $query['page'] : 1 ;
        $limit = isset($query['limit']) ? $query['limit'] : 6 ;
        $searchName = isset($query['search_name']) ? $query['search_name'] : '';

        $where = ' 1 = 1';
        if($searchName !== '') {
            $where .= ' AND  title like "%' . $searchName . '%"';
        }
        if($statusId !== '') {
            $where .= ' AND  status = ' . $statusId;
        }
        if(!empty($startDate) && !empty($endDate)) {
            $where .= ' AND  start_date >= "' . $startDate . '" and end_date  <= "' . $endDate . '"';
        }
        $tableName = $this->getModel()->getTable();

        $data = $this->getModel()
            ->select($tableName.'.*')
            ->whereRaw($where)
            ->orderBy('id','DESC')
            ->paginate($limit);

        $listData = array();
        foreach ($data as $i) {
            $item = $i->toArray();
            array_push($listData, $item);
        }
        $total = $this->search()
            ->select($tableName.'.*')
            ->whereRaw($where)
            ->get()->count();
        
        $result = [];
        $result['data'] = $listData;
        $result['total'] = $total;
        return $result;
    }

    public function getById($id)
    {
        if (!empty($id)) {
            $prioBox = TutorPrioBox::find($id);
            //get subject
            $listSubject = $this->tutorSubject->getListForFilter();
            //get gradle
            $listGrade = $this->tutorGrade->getListForFilter();
            $subject = [];
            $grade = [];
            $arrSubject = explode(",",$prioBox['target_subject_id']);
            $arrGrade = explode(",",$prioBox['target_grade_id']);
            foreach ($arrSubject as $s) {
                foreach ($listSubject as $i) {
                    if ($s == $i['id']) {
                        array_push($subject, $i);
                    }
                }
            }
            foreach ($arrGrade as $g) {
                foreach ($listGrade as $l) {
                    if ($g == $l['id']) {
                        array_push($grade, $l);
                    }
                }
            }
            $prioBox['list_grade'] = $grade;
            $prioBox['list_subject'] = $subject;
            return $prioBox;
        }
    }

    public function create($query)
    {
        $target_user_id = $this->getListUserSendNoti($query);
        $new = [
            'title' => $query['notify_name'] ?? '',
            'type' => $query['notify_type'] ?? '',
            'status' => $query['notify_status'] ?? '',
            'level' => $query['level'] ?? '',
            'start_date' => $query['start_time'] ?? '',
            'end_date' => $query['end_time'] ?? '',
            'target_grade_id' => $query['target_grade_id_used'] ?? '',
            'target_subject_id' => $query['target_subject_id_used'] ?? '',
            'target_class_id' => $query['target_class_id_used'] ?? '',
            'teacher_id' => $query['teacher_id'] ?? '',
            'content' => $query['content'] ?? '',
            'avatar_url' => $query['avatar_url'] ?? '',
            'target_user_id' => $target_user_id,
        ];

        $data = new TutorPrioBox;
        return $data->create($new);
    }

    public function update($id, $query) 
    {
        $listUser = $this->getListUserSendNoti($query);
        $data = [
            'title' => $query['notify_name'] ?? '',
            'type' => $query['notify_type'] ?? '',
            'status' => $query['notify_status'] ?? '',
            'level' => $query['level'] ?? '',
            'start_date' => $query['start_time'] ?? '',
            'end_date' => $query['end_time'] ?? '',
            'target_grade_id' => $query['target_grade_id_used'] ?? '',
            'target_subject_id' => $query['target_subject_id_used'] ?? '',
            'target_class_id' => $query['target_class_id_used'] ?? '',
            'teacher_id' => $query['teacher_id'] ?? '',
            'content' => $query['content'] ?? '',
            'avatar_url' => $query['avatar_url'] ?? '',
            'target_user_id' => $listUser
        ];
        
        $update = TutorPrioBox::find($id);
        $currentTime = date('Y/m/d H:i:s', time());
        $startDate = Carbon::parse($update['start_date'])->format('Y/m/d H:i:s');
        if ($currentTime <= $startDate) {
            $update->update($data);
        }
        return 1;
    }

    public function getNotifyByUser($userId)
    {
        $currentTime =formatDateTime(now(),'Y-m-d H:i:s');
        return $this->getModel()
            ->with('teacher:id,name,gender,image')
            ->whereRaw('target_user_id  like "%' . $userId . '%" ')
            ->where('start_date', '<', $currentTime)
            ->where('end_date', '>', $currentTime)
            ->where('status', TutorPrioBox::ACTICE)
            ->orderby('start_date', 'DESC')
            ->limit(3)
            ->get()
            ->toArray();
    }

    public function getListUserSendNoti($query) {
        $gradeIds = isset($query['target_grade_id_used']) ? $query['target_grade_id_used'] : '';
        $subjectIds = isset($query['target_subject_id_used']) ? $query['target_subject_id_used'] : '';
        $classIds = isset($query['target_class_id_used']) ? $query['target_class_id_used'] : '';
        $userIds = isset($query['user_id_used']) ? $query['user_id_used'] : '';
        $where = '';
        if ($gradeIds) {
            $arrGrade = explode(",",$gradeIds);
            $where .= ' (';
            foreach ($arrGrade as $k => $grade) {
                $where .= 'target_grade_id = ' . $grade;
                if ($k+1 !== count($arrGrade)) $where .= ' OR ';
            }
            $where .= ') AND';
        }

        if ($subjectIds) {
            $arrSubject = explode(",",$subjectIds);
            $where .= ' (';
            foreach ($arrSubject as $k => $subject) {
                $where .= 'target_subject_id = ' . $subject;
                if ($k+1 !== count($arrSubject)) $where .= ' OR ';
            }
            $where .= ') AND';
        }

        if ($classIds) {
            $arrClass = explode(",",$classIds);
            $where .= ' (';
            foreach ($arrClass as $k => $class) {
                $where .= 'id = ' . $class;
                if ($k+1 !== count($arrClass)) $where .= ' OR ';
            }
            $where .= ') AND';
        }

        $where .= ' deleted = 0';

        $class = $this->tutorClass
            ->selectRaw('id')
            ->whereRaw($where)
            ->get()
            ->toArray();
        $listStudent = array();
        $result = array();
        if ($class) {
            foreach($class as $item) {
                $listUserForClass = $this->tutorClassStudent
                    ->select('student_id')
                    ->where('class_id', '=', $item)
                    ->distinct()
                    ->get()
                    ->toArray();
                if ($listUserForClass) {
                    foreach($listUserForClass as $s) {
                        array_push($listStudent, $s['student_id']);
                    }
                }
            }
        }
        // Lọc trùng userId
        $listStudent = array_unique($listStudent);

        if ($userIds) {
            $arrUser = explode(",",$userIds);
            foreach($arrUser as $u) {
                foreach($listStudent as $ls) {
                    // Kiểm tra mảng userId truyền lên có tồn tại trong mảng userId filter ra hay không ? nếu tồn tại thì lấy 
                    if ($u == $ls) {
                        array_push($result, $u);
                    }
                }
            }
        } else {
            $result = $listStudent;
        }
        $result = implode(",",$result);
        return $result;
    }

    public function getNotifyByClass($classId)
    {
        $currentTime =formatDateTime(now(),'Y-m-d H:i:s');
        return $this->getModel()
            ->with('teacher:id,name,gender,image')
            ->whereRaw('target_class_id  like "%' . $classId . '%" ')
            ->where('start_date', '<', $currentTime)
            ->where('end_date', '>', $currentTime)
            ->where('status', TutorPrioBox::ACTICE)
            ->orderby('start_date', 'DESC')
            ->limit(3)
            ->get()
            ->toArray();
    }

    public function getNotifyByGrade($gradeId)
    {
        $currentTime =formatDateTime(now(),'Y-m-d H:i:s');
        return $this->getModel()
            ->with('teacher:id,name,gender,image')
            ->whereRaw('target_grade_id  like "%' . $gradeId . '%" ')
            ->where('start_date', '<', $currentTime)
            ->where('end_date', '>', $currentTime)
            ->where('status', TutorPrioBox::ACTICE)
            ->orderby('start_date', 'DESC')
            ->limit(3)
            ->get()
            ->toArray();
    }
}
