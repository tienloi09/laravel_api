<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CoreUserDevice;
use App\Models\TutorStudent;

class CoreUserDeviceRepository extends BaseRepository
{
    protected $tutorStudent;
    public function __construct(CoreUserDevice $coreUserDevice, TutorStudent $tutorStudent)
    {
        $this->setModel($coreUserDevice);
        $this->tutorStudent = $tutorStudent;
    }

    public function getUserAndDevice($objData)
    {
        return $this->getModel()
                ->select(['*', 'tutor_student.id as student_id'])
                ->join('tutor_student' , function ($join) {
                    $join->on('tutor_student.lms_core_user_id', '=', 'core_user_device.user_id');
                    whereColumnDeleted($join, 'tutor_student');
                })
                ->where('core_user_device.user_id', $objData->id)
                ->where('device_id', $objData->deviceId)->first();

    }
}
