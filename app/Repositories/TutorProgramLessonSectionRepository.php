<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorProgramLessonSection;
use App\Models\TutorStudent;
use App\Models\TutorClassStudent;
use App\Models\TutorClassProgram;
use App\Models\TutorClassLesson;
use App\Validations\TutorProgramLessonSection as TutorProgramLessonSectionValidation;

class TutorProgramLessonSectionRepository extends BaseRepository
{
    protected $tutorProgramLessonSectionValidation;
    protected $tutorStudent;
    protected $tutorClassStudent;
    protected $tutorClassProgram;
    protected $tutorClassLesson;

    public function __construct(
        TutorProgramLessonSection $tutorProgramLessonSection,
        TutorStudent $tutorStudent,
        TutorClassStudent $tutorClassStudent,
        TutorClassProgram $tutorClassProgram,
        TutorClassLesson $tutorClassLesson,
        TutorProgramLessonSectionValidation $tutorProgramLessonSectionValidation
        )
    {
        $this->setModel($tutorProgramLessonSection);
        $this->setValidate($tutorProgramLessonSectionValidation);
        $this->tutorStudent = $tutorStudent;
        $this->tutorClassStudent = $tutorClassStudent;
        $this->tutorClassProgram = $tutorClassProgram;
        $this->tutorClassLesson = $tutorClassLesson;
    }
    public function findListProgramLesson($idStudent)
    {
        $tableName = $this->getModel()->getTable();
        $currentDate = formatDateTime('','Y-m-d H:i');

        $lstData = $this->search()
            ->select($tableName.'.*')
            ->join('tutor_student as std', function ($join) use ($idStudent) {
                $join->where('std.id', $idStudent);
                whereColumnDeleted($join, 'std');
            })->join('tutor_class_program as tcp', function ($join) use ($tableName) {
                $join->on('tcp.program_id', $tableName . '.program_id');
                whereColumnDeleted($join, 'tcp');
            })->join('tutor_class_lesson as tcl', function ($join) {
                $join->on('tcl.class_id', 'tcp.class_id');
                whereColumnDeleted($join, 'tcl');
            })->join('tutor_class_students as tcs', function ($join) {
                $join->on('tcs.class_id', 'tcp.class_id');
                $join->on('tcs.student_id', 'tcs.id');
                whereColumnDeleted($join, 'tcl');
            })
            ->whereRaw('DATE_FORMAT(end_time,\'%Y-%m-%d %H:%i\') < \''.$currentDate.'\'')
            ->orderBy('end_time','DESC')
            ->get();

        return $lstData;
    }

    public function getListByProgramLesson($lessonId)
    {
        return $this->search([ 'lesson_id' => $lessonId ])
            ->select('position', 'id')
            ->get();
    }
}
