<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\TutorResult;
use App\Models\LmsPractice;
use Illuminate\Support\Facades\DB;

class TutorResultRepository extends BaseRepository
{
    public function __construct(TutorResult $tutorResult)
    {
        $this->setModel($tutorResult);
    }

    public function getListByClass($query = [])
    {
        if (empty($query)) return [];
        $query['student_id'] = apiStudentId();

        $rows = $this->search($query)->get();
        
        $lmsIds = [];
        foreach($rows as $row) {
            $lmsIds[] = $row['lms_id'];
        }
        $lmsIds = array_unique($lmsIds);
        $practice = app(LmsPractice::class);
        
        $subquery = DB::table('lms_practice_result')
                    ->select(DB::raw("id, MAX(total_correct_question) AS mx"))
                    ->where('user_id' , apiLmsCoreUserId())
                    ->whereIn('practice_id' , $lmsIds)
                    ->groupBy('practice_id')->pluck('id');

        $result = DB::table('lms_practice_result as pp')
        ->selectRaw('pp.practice_id, pp.total_correct_question, pp.total_question')
        ->whereIn('id', $subquery)
        ->get()->toArray();
        
        $kv = [];
        if(count($result) > 0) {    
            foreach($result as $r) {
                $kv[$r->practice_id] = (array)$r;
            }
        }

        return $kv;
    }

    public function getSectionResultByUser($classId, $studentId, $lessonId)
    {
        $tutorResultTable = $this->getModel()->getTable();
        return $listSection = $this->getModel()
            ->selectRaw($tutorResultTable . '.id')
            ->whereRaw('tutor_result.class_id = ' . $classId . ' AND tutor_result.lesson_id = '. $lessonId . ' AND tutor_result.student_id = ' . $studentId)
            ->get()->toArray();
    }
}
