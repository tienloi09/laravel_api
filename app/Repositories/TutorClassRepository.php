<?php

namespace App\Repositories;

use App\Models\BaseModel;
use App\Repositories\BaseRepository;
use App\Models\TutorClass;
use App\Models\TutorStudent;
use App\Models\TutorTeacher;
use App\Models\TutorClassStudent;
use App\Models\TutorClassProgram;
use App\Models\TutorClassLesson;
use App\Models\TutorClassLessonSection;
use App\Models\TutorProgramLesson;
use App\Models\TutorClassSchedule;
use Illuminate\Support\Facades\DB;
use App\Validations\TutorClass as TutorClassValidations;
use Carbon\Carbon;
use App\Models\Mongo\Pixel;

class TutorClassRepository extends BaseRepository
{
    protected $studentModel;
    protected $classStudentModel;
    protected $classProgramModel;
    protected $tutorClassLessonModel;
    protected $tutorProgramLessonModel;
    protected $tutorClassLessonSectionModel;

    public function __construct(
        TutorClass $tutorClass,
        TutorStudent $tutorStudent,
        TutorClassStudent $classStudentModel,
        TutorClassProgram $classProgramModel,
        TutorClassLesson $tutorClassLessonModel,
        TutorProgramLesson $tutorProgramLessonModel,
        TutorClassLessonSection $tutorClassLessonSectionModel,
        TutorClassValidations $tutorClassValidation
        )
    {
        $this->setModel($tutorClass);
        $this->studentModel = $tutorStudent;
        $this->classStudentModel = $classStudentModel;
        $this->classProgramModel = $classProgramModel;
        $this->tutorProgramLessonModel = $tutorProgramLessonModel;
        $this->tutorClassLessonModel = $tutorClassLessonModel;
        $this->tutorClassLessonSectionModel = $tutorClassLessonSectionModel;
        $this->setValidate($tutorClassValidation);
    }

    public function getListSubjectWorking()
    {
        $dateNow = formatDateTime('','Y-m-d');
        return $this->search()
            ->selectRaw("DATE_FORMAT(start_date, '%d-%m-%Y') as start_date, DATE_FORMAT(end_date, '%d-%m-%Y') as end_date")
            ->whereRaw("DATE_FORMAT(start_date, '%Y-%m-%d') < '".$dateNow."' AND '".$dateNow."' <= DATE_FORMAT(end_date, '%Y-%m-%d')")
            ->get();
    }

    public function getListClass($query = [])
    {
        $perPage = isset($query['perPage']) ? $query['perPage'] : getConfig('per_page');
        $builder = $this->getModel()
            ->selectRaw('tutor_class.*, DATE_FORMAT(start_date, "%d-%m-%Y") as start_date, DATE_FORMAT(end_date, "%d-%m-%Y") as end_date')
            ->where('tutor_class.deleted', getConfig('deleted_failed'));

        if (isset($query['target_subject_id'])) {
            $builder = $builder
                ->where('tutor_class.target_subject_id', $query['target_subject_id']);
        }
        if (isset($query['target_grade_id'])) {
            $builder = $builder
                ->where('tutor_class.target_grade_id', $query['target_grade_id']);
        }
        if (isset($query['level'])) {
            $builder = $builder
                ->where('tutor_class.level', $query['level']);
        }
        if (isset($query['status'])) {
            $builder = $builder
                ->where('tutor_class.status', $query['status']);
        }
        if (isset($query['teacher_id'])) {
            $builder = $builder
                ->where('tutor_class.teacher_id', $query['teacher_id']);
        }
        if (isset($query['start_date'])) {
            $builder = $builder
                ->where('tutor_class.start_date', '>', Carbon::parse($query['start_date']));
        }
        if (isset($query['end_date'])) {
            $builder = $builder
                ->where('tutor_class.end_date', '<', Carbon::parse($query['end_date']));
        }
        if (isset($query['name_search'])) {
            $builder = $builder->whereRaw("(tutor_class.title LIKE '%" . $query['name_search'] . "%' OR tutor_class.id LIKE '%" . $query['name_search'] . "%' )");
        }
        if (isset($query['program_id'])) {
            $builder = $builder
                ->join('tutor_class_program', 'tutor_class_program.class_id', '=', 'tutor_class.id')
                ->where('tutor_class_program.program_id', $query['program_id'])
                ->whereNull('tutor_class_program.apply_end_datetime');
        }
        $data = $builder->orderBy('id', 'DESC')->paginate($perPage);
        $listDataClass = [];

        $teacherIds = $data->pluck('teacher_id');
        $teachers = (new TutorTeacher)::whereIn('id', $teacherIds)->get()->keyBy('id');

        $classIds = $data->pluck('id');
        $lessons = $this->tutorClassLessonModel
            ->select('id', 'lesson_start_time', 'lesson_end_time', 'class_id')
            ->whereIn('class_id', $classIds)
            ->where('deleted', BaseModel::UNDELETED)->get()->mapToGroups(function ($item, $key) {
                return [$item['class_id'] => [
                    'id' => $item['id'],
                    'lesson_start_time' => $item['lesson_start_time'],
                    'lesson_end_time' => $item['lesson_end_time']
                ]];
            })->all();
        $schedules = (new TutorClassSchedule)::selectRaw("id, class_id, day_of_week, DATE_FORMAT(start_time,'%d-%m-%Y %H:%i') as start_time, DATE_FORMAT(end_time,'%d-%m-%Y %H:%i') as end_time")
            ->whereIn('class_id', $classIds)
            ->where('deleted', BaseModel::UNDELETED)->get()->mapToGroups(function ($item, $key) {
                return [$item['class_id'] => [
                    'id' => $item['id'],
                    'day_of_week' => $item['day_of_week'],
                    'start_time' => $item['start_time'],
                    'end_time' => $item['end_time'],
                ]];
            })->all();
        $students = $this->classStudentModel
            ->select('class_id', 'student_id')
            ->whereIn('class_id', $classIds)
            ->where([
            'deleted' => getConfig('deleted_failed'),
        ])->get()->mapToGroups(function ($item, $key) {
            return [$item['class_id'] =>  $item['student_id']];
        })->all();

        foreach ($data as $item) {
            $itemClass = $item->toArray();
            // $teacher = $item->hasOneTeacher;
            $itemClass['teacher'] = [
                'name' => $teachers[$item->teacher_id]->name,
                'image' => $teachers[$item->teacher_id]->image,
            ];

            $itemClass['lesson'] = [];

            if ($item->status == getConfig('tutor_class.status.running.id') && isset($lessons[$item->id])) {
                $itemClass['lesson'] = $lessons[$item->id];
            }

            $itemClass['count_student'] = 0;
            if (isset($students[$item->id])) {
                $students = array_unique($students[$item->id]->toArray());
                $itemClass['count_student'] = count($students);
            }

            $itemClass['schedule'] = convertSchedule($schedules[$item->id]);

            array_push($listDataClass, $itemClass);
        }

        return [
            'data' => $listDataClass,
            'total' => $data->total()
        ];
    }

    public function addProgramToClass($classId, $programId, $startLessonId = null)
    {
        try {
            $programLessons = $this->tutorProgramLessonModel
                ->with('tutorProgramLessonSections')
                ->where([
                    ['program_id', $programId],
                    ['deleted', getConfig('deleted_failed')],
                ])
                ->orderBy('tutor_program_lesson.position')
                ->get()
                ->toArray();
            $class = $this->getModel()->find($classId);

            if ($class->status == getConfig('tutor_class.status.finish.id')) {
                return;
            }

            // Get listDates Map from start_date, end_date in class with schedule
            $dates = $this->genDates($classId);

            DB::BeginTransaction();
            $lessonType = getConfig('class_lesson_type.inActive');
            $lastestClassProgram = $this->classProgramModel->where([
                'class_id' => $classId,
                'apply_end_datetime' => null
            ])->first();
            if ($lastestClassProgram) {
                $lastestClassProgram->apply_end_datetime = Carbon::now();
                $lastestClassProgram->save();
            }

            $tutorClassProgram = $this->classProgramModel->create([
                'class_id' => $classId,
                'program_id' => $programId,
                'apply_start_datetime' => Carbon::now(),
            ]);

            $countClassLessonsEnded = $this->tutorClassLessonModel
                ->where([
                    [getConfig('deleted_name'), getConfig('deleted_failed')],
                    ['class_id', $classId],
                    ['lesson_type', getConfig('class_lesson_type.active')],
                ])
                ->where('lesson_end_time', '<', Carbon::now())
                ->count();
            $oldLessonNotRun = $this->tutorClassLessonModel
                ->where([
                    [getConfig('deleted_name'), getConfig('deleted_failed')],
                    ['class_id', $classId],
                    ['lesson_type', getConfig('class_lesson_type.active')],
                ])
                ->where('lesson_start_time', '>', Carbon::now())
                ->get();

            foreach ($oldLessonNotRun as $lesson) {
                $lesson->deleted = getConfig('deleted_active');
                $lesson->save();
            }

            if ($class->status == getConfig('tutor_class.status.running.id') || $class->status == getConfig('tutor_class.status.await.id')) {
                $timeSearch = formatDateTime('', 'Y-m-d H:i');
                $indexTime = -1;
                foreach ($dates as $key => $time) {
                    if (strtotime($timeSearch) <= strtotime($time['st'])) {
                        $indexTime = $key;
                        break;
                    }
                }

                foreach ($programLessons as $key => $programLesson) {
                    if ($programLesson['id'] == $startLessonId) {
                        $lessonType = getConfig('class_lesson_type.active');
                        $tutorClassProgram->start_lesson_position = $programLesson['position'];
                        $tutorClassProgram->save();
                    }
                    if (!isset($dates[$indexTime])) {
                        break;
                    }
                    if ($countClassLessonsEnded && $lessonType == getConfig('class_lesson_type.inActive')) {
                        continue;
                    }

                    $newClassLesson = ($lessonType) ? [
                        'class_id' => $classId,
                        'title' => $programLesson['title'],
                        'desc' => $programLesson['desc'],
                        'position' => $programLesson['position'],
                        'lesson_type' => $lessonType,
                        'lesson_start_time' => $dates[$indexTime]['st'],
                        'lesson_end_time' => $dates[$indexTime]['et'],
                        'tutor_program_lesson_id' => $programLesson['id'],
                    ]
                    : [
                        'class_id' => $classId,
                        'title' => $programLesson['title'],
                        'desc' => $programLesson['desc'],
                        'position' => $programLesson['position'],
                        'lesson_type' => $lessonType,
                        'tutor_program_lesson_id' => $programLesson['id'],
                    ];

                    if ($lessonType) {
                        $indexTime++;
                    }

                    $classLesson = $this->tutorClassLessonModel->create($newClassLesson);

                    $programLessonSections = $programLesson['tutor_program_lesson_sections'];
                    $newClassLessonSections = [];
                    foreach ($programLessonSections as $programLessonSection) {
                        $item = [
                            'lesson_id' => $classLesson->id,
                            'type' => $programLessonSection['type'],
                            'title' => $programLessonSection['title'],
                            'image' => $programLessonSection['image'],
                            'position' => $programLessonSection['position'],
                            'software_used' => $programLessonSection['software_used'],
                            'desc' => $programLessonSection['desc'],
                            'lms_id' => $programLessonSection['lms_id'],
                            'is_required' => $programLessonSection['is_required'],
                            'link_study' => $programLessonSection['link_study'],
                            'lms_duration' => $programLessonSection['lms_duration'],
                            'lms_num_question' => $programLessonSection['lms_num_question'],
                            'create_date' => Carbon::now(),
                            'update_date' => Carbon::now(),
                        ];
                        array_push($newClassLessonSections, $item);
                    }
                    $this->tutorClassLessonSectionModel->insert($newClassLessonSections);
                }
                $class->num_of_lesson = $this->tutorClassLessonModel->where([
                    ['deleted', getConfig('deleted_failed')],
                    ['class_id', $class->id],
                    ['lesson_type', TutorClassLesson::LESSON_TYPE_ACTIVE],
                ])->count();
                $class->save();
                DB::commit();

                return $tutorClassProgram;
            }
        } catch (\Exception $exception) {
            DB::rollback();
            logError($exception);
            return $exception;
        }
    }

    public function updateWhenChangeProgram($classId, $programId, $startLessonId = null)
    {
        try {
            $programLessons = $this->tutorProgramLessonModel
                ->with('tutorProgramLessonSections')
                ->where([
                    ['program_id', $programId],
                    ['deleted', getConfig('deleted_failed')],
                ])
                ->orderBy('tutor_program_lesson.position')
                ->get()
                ->toArray();
            $class = $this->getModel()->find($classId);

            if ($class->status == getConfig('tutor_class.status.finish.id')) {
                return;
            }

            // Get listDates Map from start_date, end_date in class with schedule
            $dates = $this->genDates($classId);

            DB::BeginTransaction();
            $lessonType = getConfig('class_lesson_type.inActive');

            $tutorClassProgram = $this->classProgramModel->where([
                'class_id' => $classId,
                'program_id' => $programId,
            ])->first();

            $classLessonsEnded = $this->tutorClassLessonModel
                ->where([
                    [getConfig('deleted_name'), getConfig('deleted_failed')],
                    ['class_id', $classId],
                    ['lesson_type', getConfig('class_lesson_type.active')],
                ])
                ->where('lesson_end_time', '<', Carbon::now())
                ->pluck('tutor_program_lesson_id')->toArray();

            if ($class->status == getConfig('tutor_class.status.running.id') || $class->status == getConfig('tutor_class.status.await.id')) {
                $timeSearch = formatDateTime('', 'Y-m-d H:i');
                $indexTime = -1;
                foreach ($dates as $key => $time) {
                    if (strtotime($timeSearch) <= strtotime($time['st'])) {
                        $indexTime = $key;
                        break;
                    }
                }
                foreach ($programLessons as $key => $programLesson) {
                    if (count($classLessonsEnded)) {
                        $lessonType = getConfig('class_lesson_type.active');
                        if ($key + 1 <= $tutorClassProgram->start_lesson_position) {
                            continue;
                        }
                    }
                    if (!count($classLessonsEnded) && $key + 1 == $tutorClassProgram->start_lesson_position) {
                        $lessonType = getConfig('class_lesson_type.active');
                    }

                    if (!isset($dates[$indexTime])) {
                        break;
                    }

                    if ($lessonType == getConfig('class_lesson_type.inActive') && count($classLessonsEnded)) {
                        continue;
                    }

                    if (in_array($programLesson['id'], $classLessonsEnded)) {
                        continue;
                    }

                    $newClassLesson = [
                        'class_id' => $classId,
                        'title' => $programLesson['title'],
                        'desc' => $programLesson['desc'],
                        'position' => $programLesson['position'],
                        'lesson_type' => $lessonType,
                        'lesson_start_time' => $dates[$indexTime]['st'],
                        'lesson_end_time' => $dates[$indexTime]['et'],
                        'tutor_program_lesson_id' => $programLesson['id'],
                    ];

                    if ($lessonType) {
                        $indexTime++;
                    }

                    $classLesson = $this->tutorClassLessonModel->create($newClassLesson);

                    $programLessonSections = $programLesson['tutor_program_lesson_sections'];
                    $newClassLessonSections = [];
                    foreach ($programLessonSections as $programLessonSection) {
                        $item = [
                            'lesson_id' => $classLesson->id,
                            'type' => $programLessonSection['type'],
                            'title' => $programLessonSection['title'],
                            'image' => $programLessonSection['image'],
                            'position' => $programLessonSection['position'],
                            'software_used' => $programLessonSection['software_used'],
                            'desc' => $programLessonSection['desc'],
                            'lms_id' => $programLessonSection['lms_id'],
                            'is_required' => $programLessonSection['is_required'],
                            'link_study' => $programLessonSection['link_study'],
                            'lms_duration' => $programLessonSection['lms_duration'],
                            'lms_num_question' => $programLessonSection['lms_num_question'],
                            'create_date' => Carbon::now(),
                            'update_date' => Carbon::now(),
                        ];
                        array_push($newClassLessonSections, $item);
                    }
                    $this->tutorClassLessonSectionModel->insert($newClassLessonSections);
                }
                $class->num_of_lesson = $this->tutorClassLessonModel->where([
                    ['deleted', getConfig('deleted_failed')],
                    ['class_id', $class->id],
                    ['lesson_type', TutorClassLesson::LESSON_TYPE_ACTIVE],
                ])->count();
                $class->save();
                DB::commit();

                return $tutorClassProgram;
            }
        } catch (\Exception $exception) {
            DB::rollback();
            logError($exception);
            return $exception;
        }
    }

    public function findById($id)
    {
        return $this->search(['id' => $id])
            ->first();
    }

    public function getListClassInfoByIds($lstId)
    {
        return $this->search()
            ->with('hasOneGradle')
            ->with('hasOneTeacher')
            ->with('hasManySchedule')
            ->whereIn('id', $lstId)->get();
    }

    public function getListClassOfStudent($lstId)
    {
        return $this->search()
            ->with('classLessons')
            ->with('hasManySchedule')
            ->whereIn('id', $lstId)->get();
    }

    public function getStudents($classId)
    {
        return $this->classStudentModel->where([
            'class_id' => $classId,
            'deleted' => getConfig('deleted_failed'),
        ])->select('student_id')->get()->pluck('student_id');
    }

    public function genDates($classId = null, $startDate = null, $endDate = null, $dayOfWeeks = [])
    {
        $arrDays = [];

        if ($classId) {
            $class = $this->getModel()->with('hasManySchedule')->find($classId);
            foreach ($class->hasManySchedule as $day) {
                $arrDays[] = [
                    'd' => $day->day_of_week,
                    'st' => $day->start_time,
                    'et' => $day->end_time,
                ];
            }

            // Get the date range
            $fromDate = new Carbon($class->start_date);
            $toDate = new Carbon($class->end_date);
        } else {
            foreach ($dayOfWeeks as $day) {
                $arrDays[] = [
                    'd' => $day['day_of_week'],
                    'st' => $day['start_time'],
                    'et' => $day['end_time'],
                ];
            }

            // Get the date range
            $fromDate = new Carbon($startDate);
            $toDate = new Carbon($endDate);
        }

        // Map with DayofWeek Carbon
        $weekMaps = getConstant('week_maps');

        $date = [];

        foreach ($arrDays as $day) {
            if ($fromDate->dayOfWeek == $day['d']) {
                $dateItem = (new Carbon($fromDate));
                $date[] = [
                    'key' => $day['d'],
                    'd' => $dateItem,
                    'st' => $day['st'],
                    'et' => $day['et'],
                    'strTime' => strtotime(date($dateItem->toDateString() . ' ' . date('H:i:s', strtotime($day['st'])))),
                ];
            } else {
                $dateItem = (new Carbon($fromDate))->copy()->modify('next ' . $weekMaps[$day['d']]['full']);
                $date[] = [
                    'key' => $day['d'],
                    'd' => $dateItem,
                    'st' => $day['st'],
                    'et' => $day['et'],
                    'strTime' => strtotime(date($dateItem->toDateString() . ' ' . date('H:i:s', strtotime($day['st'])))),
                ];
            }
        }

        usort($date, function($a, $b) {
            $retval = $a['strTime'] <=> $b['strTime'];
            return $retval;
        });

        $dates = [];
        // Iterate until you have reached the end date adding a week each time
        while ($this->isLessThanDay($date, $toDate)) {
            foreach($date as $k => $d) {
                if ($date[$k]['d']->lte($toDate)) {
                    $dates[] = [
                        'dow' => $date[$k]['key'],
                        'd' => $date[$k]['d']->toDateString(),
                        'st' => date($date[$k]['d']->toDateString() . ' ' . date('H:i:s', strtotime($date[$k]['st']))),
                        'et' => date($date[$k]['d']->toDateString() . ' ' . date('H:i:s', strtotime($date[$k]['et']))),
                    ];
                    $date[$k]['d']->addWeek();
                }
            }
        }

        return $dates;
    }

    public function getListClassStudent()
    {
        $classStudentName = $this->classStudentModel->getTable();
        $className = $this->getModel()->getTable();

        $data = $this->search()
        ->join($classStudentName, $classStudentName . '.class_id', '=', $className.'.id')
        ->groupby('tutor_class.id')
        ->get()->toarray();

        return $data;
    }

    public function getAllProgramAdded($classId)
    {
        $res = [];

        $classPrograms = $this->classProgramModel
            ->with('hasOneProgram')
            ->where([
                ['tutor_class_program.class_id', $classId],
                ['tutor_class_program.deleted', getConfig('deleted_failed')],
            ])->get();

        $studentsOfClass = $this->classStudentModel
            ->with('hasOneStudent:id,lms_core_user_id')
            ->where('class_id', $classId)
            ->where([
                'deleted' => getConfig('deleted_failed'),
            ])->pluck('student_id')->toArray();
        $studentsOfClass = array_unique($studentsOfClass);
        $students = $this->studentModel
            ->select('id', 'lms_core_user_id')
            ->where('deleted', BaseModel::UNDELETED)
            ->whereIn('id', $studentsOfClass)
            ->get()
            ->toArray();
        $timeNow = Carbon::now();

        foreach($classPrograms as $classProgram) {
            $itemArr = $classProgram->toArray();
            $programLessonIds = $this->tutorProgramLessonModel->where('program_id', $classProgram->program_id)->pluck('id')->toArray();

            $lessons = $this->tutorClassLessonModel
                ->with('hasManySection')
                ->where('class_id', $classProgram->class_id)
                ->where('deleted', getConfig('deleted_failed'));

            if (!$classProgram->apply_end_datetime) {
                if (count($programLessonIds)) {
                    $idsStr = implode(',', $programLessonIds);

                    $lessons = $lessons->whereRaw('(tutor_program_lesson_id IN (' . $idsStr . ') OR tutor_program_lesson_id IS NULL)');
                } else {
                    $lessons = $lessons->whereNull('tutor_program_lesson_id');
                }
            }
            if ($classProgram->apply_end_datetime) {
                $lessons = $lessons->whereIn('tutor_program_lesson_id', $programLessonIds);
            }
            $lessons = $lessons->orderBy('position')
                ->get()
                ->toArray();

            // Modify data for response
            $lessonsResponse = [];
            foreach ($lessons as $lessonItem) {
                $startTime = Carbon::parse($lessonItem['lesson_start_time']);
                $endTime = Carbon::parse($lessonItem['lesson_end_time']);
                $sectionsResponse = [];
                foreach ($lessonItem['has_many_section'] as $sectionItem) {
                    // Count user pass all mission of section
                    // Only for running section
                    $countStudentNotDoneMission = 0;
                    if ($startTime->lt($timeNow) && $endTime->gt($timeNow)) {
                        if ($sectionItem['type'] != TutorClassLessonSection::TYPE_LIVESTREAM) {
                            foreach ($students as $student) {
                                $lmsPracticeIds = DB::table('lms_practice_result')
                                    ->select(DB::raw("id, MAX(total_correct_question) AS mx"))
                                    ->where('user_id' , $student['lms_core_user_id'])
                                    ->where('practice_id' , $sectionItem['lms_id'])
                                    ->groupBy('practice_id')->pluck('id');

                                $result = DB::table('lms_practice_result as pp')
                                    ->selectRaw('pp.practice_id, pp.total_correct_question, pp.total_question')
                                    ->whereIn('id', $lmsPracticeIds)
                                    ->get()->toArray();

                                $kv = [];
                                if(count($result) > 0) {
                                    foreach($result as $r) {
                                        $kv[$r->practice_id] = (array)$r;
                                    }
                                }

                                $numCorrect = $kv[$sectionItem['lms_id']]['total_correct_question'] ?? null;
                                $isDone = $numCorrect ? 1 : 0;

                                if ($sectionItem['type'] == TutorClassLessonSection::TYPE_VIDEO || $sectionItem['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                                    $missionType = Pixel::MISSION_TYPE_VIDEO;
                                    if ($sectionItem['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                                        $missionType = Pixel::MISSION_TYPE_DOCUMENT;
                                    }
                                    $item = $this->pixelService->findByParams([
                                        'mission_type' => $missionType,
                                        'uid' => apiUserId() . '',
                                        'lesson_id' => $sectionItem['lesson_id'] . '',
                                        'id' => $sectionItem['id'] . '',
                                    ]);
                                    if ($item) {
                                        $isDone = 1;
                                    }
                                }

                                if (!$isDone) {
                                    $countStudentNotDoneMission++;
                                }
                            }
                        }
                    }

                    $sectionItem['count_student_not_done_mission'] = $countStudentNotDoneMission;
                    $sectionsResponse[] = $sectionItem;
                }

                $lessonItem['has_many_section'] = $sectionsResponse;
                $lessonsResponse[] = $lessonItem;
            }

            $res[] = [
                'id' => $classProgram->program_id,
                'program_title' => $itemArr['has_one_program']['name'],
                'apply_start_datetime' => $classProgram->apply_start_datetime,
                'apply_end_datetime' => $classProgram->apply_end_datetime,
                'lessons' => $lessonsResponse,
            ];
        }

        return $res;
    }

    private function isLessThanDay($arrDate, $toDate)
    {
        $flag = false;
        foreach ($arrDate as $date) {
            if ($date['d']->lte($toDate)) {
                $flag = true;
                break;
            }
        }

        return $flag;
    }

    public function incNumOfLessonPassed($lessonIds)
    {
        $this->getModel()
        ->from('tutor_class as c')
        ->join('tutor_class_lesson as cl', 'c.id', '=', 'cl.class_id')
        ->whereIn('cl.id', $lessonIds)
        ->update([
            'num_of_lesson_passed' => DB::raw('num_of_lesson_passed + 1')
        ]);
    }

    public function checkTimeInFuture($ids = [], $type = 'teacher', $paramRequest, $classId = null)
    {
        $newStartDate = Carbon::parse($paramRequest['start_date']);
        $newEndDate = Carbon::parse($paramRequest['end_date']);
        $classWithSchedules = [];
        if ($type == 'teacher') {
            $classWithSchedules = $this->search()
                ->with('hasManySchedule')
                ->whereIn('teacher_id', $ids)
                ->whereNotIn('status', [ TutorClass::CLASS_FINISHED ]);
            if ($classId) {
                $classWithSchedules = $classWithSchedules->whereNotIn('id', [$classId]);
            }
            $classWithSchedules = $classWithSchedules->get()->toArray();
        }

        if ($type == 'student') {
            $classWithSchedules = $this->search()
                ->with('hasManySchedule')
                ->join('tutor_class_students', 'tutor_class_students.class_id', '=', 'tutor_class.id')
                ->where('tutor_class_students.deleted', getConfig('deleted_failed'))
                ->whereIn('tutor_class_students.student_id', $ids)
                ->whereNotIn('status', [ TutorClass::CLASS_FINISHED ])
                ->get()->toArray();
        }

        foreach ($classWithSchedules as $class) {
            $startDate = Carbon::parse($class['start_date']);
            $endDate = Carbon::parse($class['end_date']);
            $isNestedRange = $this->checkTwoRangeTime($newStartDate, $newEndDate, $startDate, $endDate);
            if ($isNestedRange) {
                $schedules = $class['has_many_schedule'];
                foreach ($schedules as $scheduleOfClass) {
                    $startTimeOfSchedule = Carbon::parse(formatDateTime($scheduleOfClass['start_time'], 'H:i'));
                    $endTimeOfSchedule = Carbon::parse(formatDateTime($scheduleOfClass['end_time'], 'H:i'));
                    foreach ($paramRequest['day_of_weeks'] as $newSchedule) {
                        $newStartTime = Carbon::parse(formatDateTime($newSchedule['start_time'], 'H:i'));
                        $newEndTime = Carbon::parse(formatDateTime($newSchedule['end_time'], 'H:i'));
                        if ($scheduleOfClass['day_of_week'] == $newSchedule['day_of_week']) {
                            $isNestedRangeTime = $this->checkTwoRangeTime($startTimeOfSchedule, $endTimeOfSchedule, $newStartTime, $newEndTime);
                            if ($isNestedRangeTime) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function checkTwoRangeTime($startTime1, $endTime1, $startTime2, $endTime2)
    {
        if (
            ($startTime1->gte($startTime2) && $startTime1->lte($endTime2)) ||
            ($endTime1->gte($startTime2) && $endTime1->lte($endTime2)) ||
            ($startTime1->lte($startTime2) && $endTime1->gte($endTime2))
            ) {
                return true;
        }

        return false;
    }
}
