<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\TutorClassRepository;
use App\Models\TutorClassLesson;
use App\Models\TutorClassLessonSection;
use App\Models\TutorClass;
use App\Models\TutorClassProgram;
use App\Models\TutorRatingComment;
use App\Repositories\TutorProgramLessonRepository;
use App\Repositories\TutorClassScheduleRepository;
use App\Repositories\TutorClassLessonSectionRepository;
use App\Models\TutorMissionCompleted;
use App\Models\TutorClassStudent;
use App\Validations\TutorClassLesson as TutorClassLessonValidation;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\LmsPractice;
use App\Services\PixelService;
use App\Models\Mongo\Pixel;

class TutorClassLessonRepository extends BaseRepository
{
    protected $tutorRatingComment;
    protected $tutorMissionCompleted;
    protected $tutorClassStudent;
    protected $tutorClass;
    protected $tutorClassProgram;
    protected $tutorClassRepository;
    protected $tutorClassScheduleRepository;
    protected $tutorProgramLessonRepository;
    protected $tutorClassLessonSectionRepository;
    protected $pixelService;

    public function __construct(
        TutorClassLesson $tutorClassLesson,
        TutorMissionCompleted $tutorMissionCompleted,
        TutorRatingComment $tutorRatingComment,
        TutorClassStudent $tutorClassStudent,
        TutorClassLessonValidation $tutorClassLessonValidation,
        TutorClass $tutorClass,
        TutorClassProgram $tutorClassProgram,
        TutorClassScheduleRepository $tutorClassScheduleRepository,
        TutorClassRepository $tutorClassRepository,
        TutorProgramLessonRepository $tutorProgramLessonRepository,
        TutorClassLessonSectionRepository $tutorClassLessonSectionRepository,
        PixelService $pixelService
        )
    {
        $this->setModel($tutorClassLesson);
        $this->setValidate($tutorClassLessonValidation);
        $this->tutorRatingComment = $tutorRatingComment;
        $this->tutorMissionCompleted = $tutorMissionCompleted;
        $this->tutorClassStudent = $tutorClassStudent;
        $this->tutorClass = $tutorClass;
        $this->tutorClassProgram = $tutorClassProgram;
        $this->tutorClassScheduleRepository = $tutorClassScheduleRepository;
        $this->tutorClassRepository = $tutorClassRepository;
        $this->tutorProgramLessonRepository = $tutorProgramLessonRepository;
        $this->tutorClassLessonSectionRepository = $tutorClassLessonSectionRepository;
        $this->pixelService = $pixelService;
    }

    public function getListLessonFinish()
    {
        $classId = request()->class_id ?? null;
        $gradeId = request()->grade_id ?? null;
        $currentDate = formatDateTime('', 'Y-m-d H:i');

        $listData = $this->getModel()
            ->with('hasOneClass')
            ->with('hasOneSectionVideoStudy:lesson_id,title,is_required,lms_id,image,link_study,software_used,link_video')
            ->with('hasOneSectionTypeVideo')
            ->with('hasOneRatingByUser:comment,teacher_comment,score,lesson_id')
            ->with('hasManySection')
            ->selectRaw('DISTINCT(tutor_class_lesson.id), lesson_end_time as raw_lesson_end_time, 
            DATE_FORMAT(lesson_end_time,\'%d-%m-%Y %H:%i\') as lesson_end_time, 
            DATE_FORMAT(lesson_start_time,\'%d-%m-%Y %H:%i\') as lesson_start_time, 
            tutor_class_lesson.class_id, 
            tutor_class_lesson.title')
            ->join('tutor_class_students', function ($join) {
                $join->on('tutor_class_students.class_id', '=', 'tutor_class_lesson.class_id');
                $join->where('tutor_class_students.student_id', '=', apiStudentId());
                whereColumnDeleted($join,'tutor_class_students');
            })
            ->join('tutor_class', 'tutor_class.id', '=', 'tutor_class_lesson.class_id')
            ->whereRaw('DATE_FORMAT(lesson_end_time,\'%Y-%m-%d %H:%i\') < \'' . $currentDate . '\'')
            ->where('tutor_class.deleted', 0)
            ->where('tutor_class_lesson.deleted', 0) 
            ->whereRaw('tutor_class_lesson.lesson_type > 0');

        if ($classId) {
            $listData = $listData->where('tutor_class_lesson.class_id', $classId);
        }
        if ($gradeId) {
            $listData = $listData->where('tutor_class.target_grade_id', $gradeId);
        }

        $listData = $listData->orderBy('raw_lesson_end_time', 'DESC')->limit(4)->get();

        $data = [];
        foreach($listData as $itemDetail){
            $item = $itemDetail->toArray();
            $item['class'] = $itemDetail->hasOneClass->toArray();
            $item['study'] = $itemDetail->hasOneSectionVideoStudy;
            $video = $itemDetail->hasOneSectionTypeVideo;
            if(!empty($video)){
                $pixelLog = $this->pixelService->findByParams([
                    'mission_type' => Pixel::MISSION_TYPE_VIDEO,
                    'uid' => apiUserId() . '',
                    'lesson_id' => $video ->lesson_id . '',
                    'id' => $video ->id . '',
                ]);
                $video->is_done = 0;
                if ($pixelLog) {
                    $video->is_done = 1;
                }
                $video->class_id = $item['class']['id'];
                $item['video'] = $video;
            } else $item['video'] = null;
            // trạng thái làm bài tập về nhà
            $mission = $itemDetail->hasOneSectionTypePractice2();
            if(!empty($mission)){
                $mission->class_id = $item['class']['id'];
            }
            $item['mission'] = $mission;
            // nhận xét giáo viên
            // đánh giá buổi học
            $item['rating'] = $itemDetail->hasOneRatingByUser;
            $item['count_program_required'] = $itemDetail->hasManyTotalSectionRequired();
            $item['count_mission_completed'] = $itemDetail->hasManyTotalMissCompleted();
            //get section by lesson
            $item['sections'] = $itemDetail->hasManySection;

            unset($item['has_one_class']);
            unset($item['has_many_section']);
            unset($item['has_one_rating_by_user']);
            unset($item['has_one_section_type_video']);
            unset($item['has_one_section_video_study']);

            array_push($data,$item);
        }
        return $data;
    }

    public function getListLessonNearDay($query = []){
        $dateNextWeek = Carbon::parse('')->add(7,'day')->format('Y-m-d H:i');
        $currentDate = formatDateTime('','Y-m-d H:i');
        
        $listData = $this->search($query)
            ->select(\DB::raw('DISTINCT(tutor_class_lesson.id), tutor_class_lesson.class_id, tutor_class_lesson.title, 
            tutor_class_lesson.lesson_type, tutor_class_lesson.position, tutor_class_lesson.desc, tutor_class_lesson.tutor_program_lesson_id,
            tutor_class_lesson.is_custom,
            lesson_start_time, lesson_end_time, DATE_FORMAT(lesson_start_time,\'%d-%m-%Y %H:%i\') as disp_lesson_start_time, DATE_FORMAT(lesson_end_time,\'%d-%m-%Y %H:%i\') as disp_lesson_end_time, total_rate_point, total_num_of_rate'))
            ->with('hasOneClass')
            ->with('hasOneSectionVideoStudy:lesson_id,title,is_required,lms_id,image,link_study,software_used,link_video')
            ->join('tutor_class_students',function ($join) {
                $join->on('tutor_class_students.class_id', '=', 'tutor_class_lesson.class_id');
                $join->where('tutor_class_students.student_id', '=', apiStudentId());
                whereColumnDeleted($join, 'tutor_class_students');
            })
            ->join('tutor_class',function($join) {
                $join->on('tutor_class.id', '=', 'tutor_class_lesson.class_id');
                whereColumnDeleted($join, 'tutor_class');
            })
            ->whereRaw('lesson_end_time >= \'' . $currentDate . '\'')
            ->whereRaw('lesson_end_time <= \'' . $dateNextWeek . '\'')
            ->orderBy('lesson_end_time', 'ASC')
            ->get();
        
        return $listData;
    }
    public function findClassLessonById($query = []){
        $dataClassLesson = $this->search($query)
            ->selectRaw('title,class_id, DATE_FORMAT(lesson_start_time,\'%d-%m-%Y %H:%i\') as lesson_start_time, DATE_FORMAT(lesson_end_time,\'%d-%m-%Y %H:%i\') as lesson_end_time, total_rate_point, total_num_of_rate')
            ->first();
        return $dataClassLesson;
    }

    public function getListByClass($query = [])
    {
        $querySearch = [];
        foreach ($query as $key => $value) {
            $querySearch['tutor_class_lesson.' . $key] = $value;
        }
        
        $dataClassLesson = $this->search($querySearch)
            ->with('hasOneSectionVideoStudy:lesson_id,title,is_required,lms_id,image,link_study,software_used,link_video')
            ->select(\DB::raw('DISTINCT(tutor_class_lesson.id), title, tutor_class_students.class_id,
            lesson_start_time as raw_lesson_start_time,
            DATE_FORMAT(lesson_start_time,\'%d-%m-%Y %H:%i\') as lesson_start_time, 
            DATE_FORMAT(lesson_end_time,\'%d-%m-%Y %H:%i\') as lesson_end_time, total_rate_point, 
            total_num_of_rate, position'))
            ->join('tutor_class_students', function ($join) {
                $join->on('tutor_class_students.class_id', '=', 'tutor_class_lesson.class_id');
                $join->where('tutor_class_students.student_id', '=', apiStudentId());
                whereColumnDeleted($join, 'tutor_class_students');
            })
            ->orderBy('raw_lesson_start_time')
            ->get();
        
        if(count($dataClassLesson) > 0) {
            $k = 1;
            foreach($dataClassLesson as $r) {
                $r->position = $k++;
            }
        }
        
        return $dataClassLesson;
    }
    public function getListLessonByClass($query = [])
    {
        return $this->search($query)
            ->selectRaw('title,class_id, DATE_FORMAT(lesson_start_time,\'%d-%m-%Y %H:%i\') as lesson_start_time, DATE_FORMAT(lesson_end_time,\'%d-%m-%Y %H:%i\') as lesson_end_time, total_rate_point, total_num_of_rate')
            ->get();
    }

    public function updateWhenEditProgram($programId = null, $isChangePosition = null)
    {
        $classes = $this->tutorClass
            ->join('tutor_class_program', 'tutor_class_program.class_id', '=', 'tutor_class.id')
            ->where([
                'tutor_class_program.program_id'=> $programId,
                'tutor_class_program.deleted' => getConfig('deleted_failed'),
                'tutor_class.deleted' => getConfig('deleted_failed')
            ])
            ->whereNull('tutor_class_program.apply_end_datetime')
            ->whereIn('tutor_class.status', [ getConfig('tutor_class.status.running.id'), getConfig('tutor_class.status.await.id') ])
            ->selectRaw('tutor_class.*, program_id')
            ->get();
        $classEdited = [];

        foreach ($classes as $class) {
            // Class is await
            if ($class->status == getConfig('tutor_class.status.await.id')) {
                $classLessons = $this->search()
                    ->with('hasManySection')
                    ->where([
                        'tutor_class_lesson.class_id' => $class->id,
                    ])
                    ->where('tutor_class_lesson.lesson_start_time', '>', Carbon::now())
                    ->get();
                foreach($classLessons as $classLesson) {
                    $sections = $classLesson->hasManySection()->get();
                    foreach ($sections as $section) {
                        $section->{getConfig('deleted_name')} = getConfig('deleted_active');
                        $section->save();
                    }
                    $classLesson->{getConfig('deleted_name')} = getConfig('deleted_active');
                    $classLesson->save();
                }
                $this->tutorClassRepository->updateWhenChangeProgram($class->id, $programId);
            }
            // Class is running
            if ($class->status == getConfig('tutor_class.status.running.id')) {
                $classLessons = $this->search()
                    ->with('hasManySection')
                    ->where([
                        'tutor_class_lesson.class_id' => $class->id,
                    ])
                    ->where('tutor_class_lesson.lesson_start_time', '>', Carbon::now())
                    ->orderBy('position')
                    ->get();
                DB::BeginTransaction();
                foreach($classLessons as $classLesson) {
                    $sections = $classLesson->hasManySection()->get();
                    foreach ($sections as $section) {
                        $section->{getConfig('deleted_name')} = getConfig('deleted_active');
                        $section->save();
                    }
                    $classLesson->{getConfig('deleted_name')} = getConfig('deleted_active');
                    $classLesson->save();
                }
                DB::commit();
                $this->tutorClassRepository->updateWhenChangeProgram($class->id, $programId);
            }
            $classEdited[] = $class->title;
        }

        return $classEdited;
    }

    public function getScheduleByTeacher($teacherId, $queryRaw)
    {
        $modelClass = $this->tutorClass;

        $dataSchedule = $this->search()
            ->join($modelClass->getTable(), function ($join) use ($modelClass, $teacherId) {
                $join->on($modelClass->qualifyColumn('id'), '=', $this->getModel()->qualifyColumn('class_id'));
                $join->where($modelClass->qualifyColumn('teacher_id'), '=', $teacherId);
                whereColumnDeleted($join, $modelClass->getTable());
            })
            ->whereRaw($queryRaw)
            ->count();
        return $dataSchedule;
    }

    public function getScheduleByStudent($studentId, $queryRaw)
    {
        $modelClass = $this->tutorClass;
        $modelClassStudent = $this->tutorClassStudent;
        $dataSchedule = $this->search()
            ->join($modelClass->getTable(), function ($join) use ($modelClass) {
                $join->on($modelClass->qualifyColumn('id'), '=', $this->getModel()->qualifyColumn('class_id'));
                whereColumnDeleted($join, $modelClass->getTable());
            })
            ->join($modelClassStudent->getTable(), function ($join) use ($modelClassStudent, $modelClass, $studentId) {
                $join->on($modelClassStudent->qualifyColumn('class_id'), '=', $modelClass->qualifyColumn('id'));
                $join->where($modelClassStudent->qualifyColumn('student_id'), $studentId);
                whereColumnDeleted($join, $modelClassStudent->getTable());
            })
            ->whereRaw($queryRaw)
            ->count();
        return $dataSchedule;
    }

    public function getListByClassAndTime($idClass, $timeSearch)
    {
        return $this->search(['class_id' => $idClass])
            ->whereRaw("DATE_FORMAT(lesson_start_time,'%Y-%m-%d %H:%i') >= '" . $timeSearch . "' ")
            ->orderBy('lesson_start_time')->get();
    }

    public function updateInfo($idLesson, $dataUpdate)
    {
        $entity = $this->findById($idLesson);
        return $entity->update($dataUpdate);
    }

    public function getNextTimeFreeOfClass($class)
    {
        $schedules = $this->tutorClassScheduleRepository
            ->search([ 'class_id' => $class->id ])
            ->orderBy('day_of_week')->get();
        $lastLessonCurrent = $this->getModel()
            ->join('tutor_class', 'tutor_class.id', '=', 'tutor_class_lesson.class_id')
            ->join('tutor_class_program', 'tutor_class_program.class_id', '=', 'tutor_class.id')
            ->whereNull('tutor_class_program.apply_end_datetime')
            ->select('tutor_class_lesson.id', 'tutor_class_lesson.lesson_start_time', 'tutor_class_lesson.lesson_end_time', 'tutor_class_lesson.class_id')
            ->where('tutor_class_lesson.class_id', $class->id)
            ->where('tutor_class_lesson.deleted', getConfig('deleted_failed'))
            ->orderBy('tutor_class_lesson.lesson_start_time', 'desc')->first();

        $currentTime = Carbon::now();
        $fromTime = Carbon::now();
        $toDate = Carbon::parse($class->end_date);
        $schedulesMod = [];
        foreach ($schedules as $schedule) {
            $schedulesMod[$schedule->day_of_week][] = $schedule;
        }
        foreach ($schedulesMod as $key => $schedule) {
            if (count($schedule) > 1) {
                usort($schedule, function($a, $b) {
                    $time1 = strtotime(date($a->start_time));
                    $time2 = strtotime(date($b->start_time));
                    $retval = $time1 <=> $time2;
                    return $retval;
                });
                $schedulesMod[$key] = $schedule;
            }
        }
        $schedules = [];
        foreach ($schedulesMod as $item) {
            $schedules = array_merge($schedules, $item);
        }
        $toDateTime = date('Y-m-d', strtotime($toDate)) . ' ' . date('H:i:s', strtotime($schedules[0]->start_time));

        if (isset($lastLessonCurrent)) {
            $lastTime = Carbon::parse($lastLessonCurrent->lesson_end_time);
            if ($lastTime->gte($currentTime)) {
                $fromTime = $lastTime;
            }
        }

        $date = $this->getNextTime($fromTime, $schedules);
        if (Carbon::parse($toDateTime)->lt($date['st'])) {
            return;
        }

        return $date;
    }

    public function getNextTime($time, $schedules)
    {
        $dow = $time->dayOfWeek;
        $weekMaps = getConstant('week_maps');

        for ($i = $dow; $i <= 6; $i++) {
            foreach ($schedules as $schedule) {
                $startTime = new Carbon(date('H:i:s', strtotime($time)));
                $startTimeOfSchedule = new Carbon(date('H:i:s', strtotime($schedule->start_time)));
                if ($i == $schedule->day_of_week) {
                    if ($schedule->day_of_week == $dow && $startTimeOfSchedule->lt($startTime)) {
                        continue;
                    }
                    if ($schedule->day_of_week == $dow && $startTimeOfSchedule->gt($startTime)) {
                        return [
                            'dow' => $i,
                            'd' => $time->toDateString(),
                            'st' => date($time->toDateString() . ' ' . date('H:i:s', strtotime($schedule->start_time))),
                            'et' => date($time->toDateString() . ' ' . date('H:i:s', strtotime($schedule->end_time))),
                        ];
                    }

                    $date = $time->copy()->modify('next ' . $weekMaps[$i]['full']);
                    return [
                        'dow' => $i,
                        'd' => $date->toDateString(),
                        'st' => date($date->toDateString() . ' ' . date('H:i:s', strtotime($schedule->start_time))),
                        'et' => date($date->toDateString() . ' ' . date('H:i:s', strtotime($schedule->end_time))),
                    ];
                }
            }
        }
        foreach ($schedules as $schedule) {
            $date = $time->copy()->modify('next ' . $weekMaps[$schedule->day_of_week]['full']);
            return [
                'dow' => $schedule->day_of_week,
                'd' => $date->toDateString(),
                'st' => date($date->toDateString() . ' ' . date('H:i:s', strtotime($schedule->start_time))),
                'et' => date($date->toDateString() . ' ' . date('H:i:s', strtotime($schedule->end_time))),
            ];
        }
    }

    public function getPrevNextLessons($studentId, $gradeId, $now, $classId = null) {
        $prev = $this->getPrevLesson($studentId, $gradeId, $now, $classId);
        $next = $this->getNextLesson($studentId, $gradeId, $now, $classId);
        
        return array_merge($prev, $next);
    }

    public function suspend($lessonId, $params)
    {
        $lesson = $this->findById($lessonId);
        $lessonStartTime = Carbon::parse($params['lesson_start_time']);
        $lessonEndTime = Carbon::parse($params['lesson_end_time']);
        $currentStartTime = Carbon::parse($lesson->lesson_start_time);
        $listLesson = $this->search([ 'class_id' => $params['class_id'] ])
            ->where('lesson_start_time', '>', $currentStartTime)->get();
        if ($lessonStartTime->lt($currentStartTime)) {
            return;
        }

        $lesson->lesson_start_time = $lessonStartTime;
        $lesson->lesson_end_time = $lessonEndTime;
        $lesson->save();
        $section = $this->tutorClassLessonSectionRepository->search([ 'lesson_id' => $lesson->id, 'type' => TutorClassLessonSection::TYPE_LIVESTREAM ])->first();
        if ($section) {
            $section->start_time = $params['lesson_start_time'];
            $section->end_time = $params['lesson_end_time'];
            $section->save();
        }
        if ($lessonStartTime->gt($currentStartTime)) {
            $dates = $this->tutorClassRepository->genDates($params['class_id']);
            $indexTime = -1;
            foreach ($dates as $key => $time) {
                if (strtotime($currentStartTime) > strtotime($time['st'])) {
                    $indexTime = $key;
                    break;
                }
            }

            if (isset($dates[$indexTime]) && strtotime($lessonStartTime) <= strtotime($dates[$indexTime]['st'])) {
                return $lesson;
            } else {
                $indexTime2 = -1;
                foreach ($dates as $key => $time) {
                    if (strtotime($lessonStartTime) < strtotime($time['st'])) {
                        $indexTime2 = $key;
                        break;
                    }
                }
                foreach($listLesson as $item) {
                    if (!isset($dates[$indexTime2])) {
                        return;
                    }
                    $item->lesson_start_time = $dates[$indexTime2]['st'];
                    $item->lesson_end_time = $dates[$indexTime2]['et'];
                    $item->save();
                    $indexTime2++;
                }

                return $lesson;
            }
        }
    }

    public function checkChangeTime($params)
    {
        $class = $this->tutorClassRepository->findById($params['class_id']);
        $dateChange = formatDateTime($params['lesson_start_time'], 'Y-m-d');
        $dateChange = Carbon::parse($dateChange);
        $startTimeChange = formatDateTime($params['lesson_start_time'], 'H:i');
        $endTimeChange = formatDateTime($params['lesson_end_time'], 'H:i');
        $startTimeChange = Carbon::parse($startTimeChange);
        $endTimeChange = Carbon::parse($endTimeChange);
        $studentIds = $this->tutorClassStudent
            ->where([
                'class_id' => $class->id,
                'deleted' => getConfig('deleted_failed'),
            ])->get()->pluck('student_id');
        $sqlRaw = '(teacher_id = ' . $class->teacher_id;
        if (count($studentIds)) {
            $classIdsOfStudents = $this->tutorClassStudent
                ->where('deleted', getConfig('deleted_failed'))
                ->whereIn('student_id', $studentIds)
                ->get()
                ->pluck('class_id')->toArray();
            $classIds = implode(',', $classIdsOfStudents);
            $sqlRaw .= ' OR ' . 'id in (' . $classIds . ')';
        }
        $sqlRaw .= ')';

        $classWithSchedules = $this->tutorClassRepository
            ->search()
            ->with('hasManySchedule')
            ->whereNotIn('id', [ $class->id ])
            ->whereNotIn('status', [ TutorClass::CLASS_FINISHED ])
            ->whereRaw($sqlRaw)
            ->get()->toArray();
        foreach ($classWithSchedules as $class) {
            $startDateOfClass = Carbon::parse($class['start_date']);
            $endDateOfClass = Carbon::parse($class['end_date']);
            $schedulesOfClass = $class['has_many_schedule'];
            if ($dateChange->gte($startDateOfClass) && $dateChange->lte($endDateOfClass)) {
                foreach ($schedulesOfClass as $schedule) {
                    if ($schedule['day_of_week'] == $dateChange->dayOfWeek) {
                        $startTimeSchedule = formatDateTime($schedule['start_time'], 'H:i');
                        $endTimeSchedule = formatDateTime($schedule['end_time'], 'H:i');
                        $startTimeSchedule = Carbon::parse($startTimeSchedule);
                        $endTimeSchedule = Carbon::parse($endTimeSchedule);
                        $check = $this->tutorClassRepository->checkTwoRangeTime($startTimeSchedule, $endTimeSchedule, $startTimeChange, $endTimeChange);
                        if ($check) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /*
    Lay danh sach cac buoi hoc lien truoc
    */
    protected function getPrevLesson($studentId, $gradeId, $now, $classId = null)
    {
        
        $prev = $this->getModel()::from('tutor_class_lesson as cl')
        ->select('cl.id')
        ->join('tutor_class_students as cs', 'cl.class_id', '=', 'cs.class_id')
        ->join('tutor_class as c', 'cl.class_id', '=', 'c.id')
        ->where('cl.lesson_end_time', '<', $now->toDateTimeString())
        ->where([
            'cs.student_id' => $studentId,
            'c.target_grade_id' => $gradeId,
            'c.status' => TutorClass::CLASS_RUNNING,
        ]);
        if($classId != null) {
            $prev = $prev->where('cl.class_id', (int) $classId);
        }
        $prev = $prev->orderBy('lesson_end_time', 'DESC')
        ->first();
        
        return $prev ? [$prev->id] : [];
    }

    /*
    Lay danh sach cac buoi hoc lien truoc
    */
    protected function getNextLesson($studentId, $gradeId, $now, $classId = null)
    {
        $next = $this->getModel()::from('tutor_class_lesson as cl')
        ->select('cl.id')
        ->join('tutor_class_students as cs', 'cl.class_id', '=', 'cs.class_id')
        ->join('tutor_class as c', 'cl.class_id', '=', 'c.id')
        ->where(function ($query) use ($now) {
            $query->where(function ($query)  use ($now){
                $query->where('cl.lesson_start_time', '>', $now->toDateTimeString())
                      ->where('cl.lesson_end_time', '>', $now->toDateTimeString());
            })->orWhere(function ($query)  use ($now) {
                $query->where('cl.lesson_start_time', '<=', $now->toDateTimeString())
                      ->where('cl.lesson_end_time', '>', $now->toDateTimeString());
            });
        })
        //->where('cl.lesson_start_time', '>', $now->toDateTimeString())
        ->where([
            'cs.student_id' => $studentId,
            'c.target_grade_id' => $gradeId,
            'c.status' => TutorClass::CLASS_RUNNING,
        ]);
        if($classId != null) {
            $next = $next->where('cl.class_id', (int) $classId);
        }
        $next = $next->orderBy('lesson_start_time', 'ASC')
        ->first();

        return $next ? [$next->id] : [];
    }

    public function getOldLessons($studentId, $gradeId, $now, $classId = null)
    {
        return $this->getModel()::from('tutor_class_lesson as cl')
            ->select('cl.id')
            ->join('tutor_class_students as cs', 'cl.class_id', '=', 'cs.class_id')
            ->join('tutor_class as c', 'cl.class_id', '=', 'c.id')
            ->where('cl.lesson_end_time', '<', $now->toDateTimeString())
            ->where([
                'cs.student_id' => $studentId,
                'c.target_grade_id' => $gradeId,
                'c.status' => TutorClass::CLASS_RUNNING,
                'cl.class_id' => (int) $classId,
            ])->orderBy('lesson_end_time', 'DESC')
            ->get();
    }
}
