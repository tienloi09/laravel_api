<?php

namespace App\Services;

use App\Models\Mongo\MongoTutorStudent;
use App\Models\Mongo\MongoTutorTeacher;

class MongoLogger
{
    public static function setTeacher($data) {
        MongoTutorTeacher::create($data);
        return 1;
    }

    public static function setStudent($data) {
        MongoTutorStudent::create($data);
        return 1;
    }

    public static function getStudent($id, $page, $limit)
    {
        $data = MongoTutorStudent::where('student_id', $id)
                ->orderBy('created_at', 'DESC')
                ->paginate($limit);
        $result = [];
        $result['totalItem'] = $data->total();
        $listData = array();
        foreach ($data as $student) {
            $item = $student->toArray();
            array_push($listData, $item);
        }
        $result['data'] = $listData;
        return $result;
    }

    public static function getTeacher($id, $page, $limit)
    {
        $data = MongoTutorTeacher::where('teacher_id', $id)
                ->orderBy('created_at', 'DESC')
                ->paginate($limit);
        $result = [];
        $result['totalItem'] = $data->total();
        $listData = array();
        foreach ($data as $student) {
            $item = $student->toArray();
            array_push($listData, $item);
        }
        $result['data'] = $listData;
        return $result;
    }
}
