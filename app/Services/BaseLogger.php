<?php

namespace App\Services;

use App\Models\Mongo\MongoTutorStudent;
use App\Services\MongoLogger;
use App\Services\FileLogger;

class BaseLogger
{
    public $methodLogger;

    public function __construct()
    {
        if (env('DEFAULT_LOG_METHOD') == 'file') {
            $this->methodLogger = (new FileLogger);
        }
        $this->methodLogger = (new MongoLogger);
    }

    public function writeLogStudent($data)
    {
        $this->methodLogger::setStudent($data);
        return 1;
    }

    public function writeLogTeacher($data)
    {
        $this->methodLogger::setTeacher($data);
        return 1;
    }

    public function readLogStudent($id, $page, $limit)
    {
        return $this->methodLogger::getStudent($id, $page, $limit);
    }

    public function readLogTeacher($id, $page, $limit)
    {
        return $this->methodLogger::getTeacher($id, $page, $limit);
    }
}
