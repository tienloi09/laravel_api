<?php

namespace App\Services;

use App\Models\Mongo\Pixel;

class PixelService
{
    protected $model;

    public function __construct()
    {
    }

    public function findByParams($params = [])
    {
        return Pixel::where($params)->first();
    }

    public function getList($params = [])
    {
        $data = Pixel::where($params)
            ->orderBy('created_at', 'DESC')
            ->paginate(6);

        $listData = [];
        foreach ($data as $val) {
            $item = $val->toArray();
            array_push($listData, $item);
        }

        return [
            'totalItem' => $data->total(),
            'data' => $listData,
        ];
    }
}
