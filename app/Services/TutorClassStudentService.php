<?php

namespace App\Services;

use App\Models\TutorClassStudent;
use App\Models\TutorProduct;

class TutorClassStudentService
{
    public function __construct()
    {
    }

    public function getLessonNumberByClass($classId)
    {
    }

    public function getLessonNumberByProduct($productId)
    {
        $model = new TutorProduct;
        return $totalLesson = $model
            ->selectRaw('total_lesson')
            ->whereRaw('product_id = ' . $productId)
            ->first();
    }

    public function getLessonNumberByProductCode($productCode)
    {
        $model = new TutorProduct;
        return $totalLesson = $model
            ->selectRaw('total_lesson')
            ->whereRaw('product_code like "%' . $productCode . '%"')
            ->first();
    }

    public function getGradeByProduct($productId)
    {
        $model = new TutorProduct;
        return $totalLesson = $model
            ->selectRaw('grade_id')
            ->whereRaw('product_id = ' . $productId)
            ->first();
    }
}
