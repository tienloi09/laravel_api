<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Validations\CustomValidation;
use Illuminate\Support\Facades\Validator;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::resolver(function ($translator, $data, $rules,
                                      $messages = array(), $customAttributes = array()) {
            return new CustomValidation($translator, $data, $rules,
                $messages, $customAttributes);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
