<?php

namespace App\Models;

use App\Models\BaseModel;

class TutorTeacher extends BaseModel
{
    protected $table = 'tutor_teacher';
    //
    protected $fillable = [
        'name',
        'level',
        'target_grade_ids',
        'target_subject_ids',
        'status',
        'desc',
        'total_rate_point',
        'total_rate_time',
        'image',
        'total_num_of_lesson',
        'phone',
        'years_of_exp',
        'gender'
    ];
    protected $hidden = [
        // 'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];
}
