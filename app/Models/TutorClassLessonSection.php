<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\LmsDocument;
use App\Models\LmsRevise;
use App\Models\LmsPractice;

class TutorClassLessonSection extends BaseModel
{
    protected $table = 'tutor_class_lesson_section';
    //
    protected $fillable = [
        'program_lesson_section_id',
        'lesson_id',
        'type',
        'title',
        'image',
        'position',
        'software_used',
        'desc',
        'lms_id',
        'is_required',
        'link_study',
        'start_time',
        'end_time',
        'create_date',
        'update_date',
        'lms_duration',
        'link_video',
        'lms_num_question'
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];

    // Map with mission type in Pixel
    const TYPE_LIVESTREAM = 0;
    const TYPE_VIDEO = 1;
    const TYPE_DOCUMENT = 3;
    const TYPE_PRACTICE = 4;

    public function hasOnePractice()
    {
        return $this->hasOne(LmsPractice::class, 'id', 'lms_id')->where('site_id',getConfig('site_id'));
    }

    public function hasOneRevise()
    {
        return $this->hasOne(LmsRevise::class, 'id', 'lms_id')->where('site_id',getConfig('site_id'))
            ->select('title','content');
    }
}
