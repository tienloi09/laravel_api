<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorTeacher;

class TutorPrioBox extends BaseModel
{
    protected $table = 'tutor_prio_box';
    const ACTICE = 1;
    const INACTICE = 0;
    //
    protected $fillable = [
        'title',
        'type',
        'status',
        'start_date',
        'end_date',
        'target_grade_id',
        'target_subject_id',
        'target_class_id',
        'target_user_id',
        'teacher_id',
        'content',
        'avatar_url'
    ];

    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];

    public function teacher()
    {
        return $this->hasOne(TutorTeacher::class, 'id', 'teacher_id');
    }
}
