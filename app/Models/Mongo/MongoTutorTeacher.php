<?php

namespace App\Models\Mongo;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoTutorTeacher extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'tutor_teacher_log';
    //
    protected $fillable = [
        'teacher_id',
        'action',
        'type',
        'create_by',
        'content',
        'updated_at',
        'created_at'
    ];
    protected $hidden = [
    //
    ];
}
