<?php

namespace App\Models\Mongo;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Pixel extends Eloquent
{
    // Map with type in TutorClassLessonSection
    const MISSION_TYPE_LIVESTREAM = 'livestream';
    const MISSION_TYPE_VIDEO = 'video';
    const MISSION_TYPE_DOCUMENT = 'document';
    const MISSION_TYPE_PRACTICE = 'practice';

    protected $connection = 'mongodb_pixel';
    protected $collection = 'pixel';

    protected $fillable = [
        'class_id',
        'domain',
        'env',
        'id',
        'lesson_id',
        'lms_id',
        'mission_type',
        'ts',
        'uid',
        'utm_source',
    ];
    protected $hidden = [
    //
    ];
}
