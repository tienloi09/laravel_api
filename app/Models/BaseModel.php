<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';
    const UNDELETED = 0;
    protected $guarded = ['*'];

    /**
     * @param array $options
     * @return mixed
     */

    public function save(array $options = [])
    {
        $options[getConfig('deleted_name')] = getConfig('deleted_failed');
        return parent::save($options); // TODO: Change the autogenerated stub
    }

    /**
     * @param array $options
     * @return mixed
     */

    public function create(array $options = [])
    {
        $options[getConfig('deleted_name')] = getConfig('deleted_failed');
        return parent::create($options);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id, $flagDelete = true)
    {
        if($flagDelete){
            $this->where(getConfig('deleted_name'), getConfig('deleted_failed'))->find($id);
        }
        return $this->find($id);
    }

    /**
     * @param array $query
     * @return mixed
     */
    public function getListRow($builder)
    {
        whereColumnDeleted($builder);
        $listData = $builder->get()->toArray();
        return $listData;
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}

