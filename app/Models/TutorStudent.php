<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class TutorStudent extends BaseModel
{
    protected $table = 'tutor_student';
    //
    protected $fillable = [
        'lms_core_user_id',
        'level',
        'target_grade_ids',
        'target_subject_ids',
        'products',
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];
    //
    public function hasManyClassStudent(){
        return $this->hasOne(TutorClassStudent::class, 'student_id','id')
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'))
            ->get();
    }

    public function findTotalClassByGrade($gradeId)
    {
        return $this->hasMany(TutorClassStudent::class, 'student_id', 'id')
            ->join('tutor_class', function ($join) use ($gradeId) {
                $join->on('tutor_class.id', '=', 'tutor_class_students.class_id');
                $join->where('target_grade_id', $gradeId);
                $join->where('tutor_class_students.student_id', apiStudentId());
                whereColumnDeleted($join, 'tutor_class');
            })
            ->groupBy('tutor_class.id')
            ->count();
    }
}
