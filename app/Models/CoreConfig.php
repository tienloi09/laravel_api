<?php

namespace App\Models;

use App\Models\BaseModel;

class CoreConfig extends BaseModel
{

    protected $table = 'core_config';
    //
    protected $fillable = [];
}
