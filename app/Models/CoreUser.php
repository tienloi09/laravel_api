<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class CoreUser extends Authenticatable
{
    protected $table = 'core_user';
    //
    protected $fillable = [
        'sso_type',
        'sso_id',
        'user_name',
        'full_name',
        'password',
        'password_rule',
        'email',
        'account_type',
        'mobile',
        'vnedu_id',
        'tinh_id',
        'huyen_id',
        'xa_id',
        'address',
        'truong_id',
        'khoi_id',
        'ten_lop',
        'active',
        'online_time',
        'last_visited'
    ];
}
