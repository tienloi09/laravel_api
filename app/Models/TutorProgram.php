<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorClassProgram;
use App\Models\TutorProgramLesson;

class TutorProgram extends BaseModel
{

    const STATUS_RUNNING = 1;
    const STATUS_WILL_RUN = 0;

    protected $table = 'tutor_program';
    //
    protected $fillable = [
        'name',
        'target_grade_id',
        'target_subject_id',
        'total_num_of_rate',
        'total_rate_point',
        'level',
        'status',
        'product_code'
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];
    public function classProgram()
    {
        return $this->hasMany(TutorClassProgram::class, 'program_id');
    }

    public function programLessons()
    {
        return $this->hasMany(TutorProgramLesson::class, 'program_id')
            ->where('deleted', getConfig('deleted_failed'))
            ->orderBy('position')
            ->select('title', 'id', 'position', 'program_id');
    }
}
