<?php

namespace App\Models;

use App\Models\BaseModel;

class TutorRatingComment extends BaseModel
{
    protected $table = 'tutor_rating_comment';
    //
    protected $fillable = [
        'class_id',
        'lesson_id',
        'student_id',
        'score',
        'comment',
        'status'
    ];
}
