<?php

namespace App\Models;

use App\Models\BaseModel;

class LmsPractice extends BaseModel
{

    protected $table = 'lms_practice';
    //
    protected $fillable = [];

    public function countLmsPracticeQuestion($practiceId)
    {
        $count = \DB::table('lms_practice_2_question')->where('practice_id', $practiceId)->count();
        return $count;
    }

    public function getPracticeResult($practiceId, $userId)
    {
        return \DB::table('lms_practice_result')->where([
            'practice_id' => $practiceId,
            'user_id' => $userId
        ])->get();
    }
}
