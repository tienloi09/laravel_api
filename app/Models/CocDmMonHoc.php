<?php
namespace App\Models;

use App\Models\TutorClass;
use App\Models\TutorClassStudent;

class CocDmMonHoc extends BaseModel{

    protected $table = 'coc_dm_mon_hoc';
    //
    protected $fillable = [];

    public function hasManyClassByStudent($gradeId = null)
    {
        $data = $this->hasMany(TutorClass::class,'target_subject_id','id')
            ->selectRaw('DISTINCT(tutor_class.id), title, target_grade_id, target_subject_id, level, status, teacher_id, logo, logo_thumb, DATE_FORMAT(`start_date`,"%d-%m-%Y") as start_date,DATE_FORMAT(`end_date`,"%d-%m-%Y") as end_date')
            ->join('tutor_class_students',function ($join) {
                $join->on('tutor_class_students.class_id', '=', 'tutor_class.id');
                $join->where('tutor_class_students.student_id', '=', apiStudentId());
                whereColumnDeleted($join, 'tutor_class_students');
            })
            ->where('tutor_class.deleted', BaseModel::UNDELETED);
        if(!empty($gradeId)) $data->where('tutor_class.target_grade_id', $gradeId);

        return $data->get();
    }
}
