<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorClass;

class TutorClassSchedule extends BaseModel
{
    protected $table = 'tutor_class_schedule';
    //
    protected $fillable = [
        'class_id',
        'day_of_week',
        'start_time',
        'end_time',
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];

    public function hasOneClassSchedule()
    {
        return $this->hasOne(TutorClass::class, 'id','class_id')
            ->select(['id', 'title', 'target_grade_id', 'target_subject_id', 'level', 'status', 'teacher_id','logo','logo_thumb'])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

}
