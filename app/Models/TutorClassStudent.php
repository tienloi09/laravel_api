<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorClass;
use App\Models\TutorStudent;

class TutorClassStudent extends BaseModel
{
    protected $table = 'tutor_class_students';
    //
    protected $fillable = [
        'class_id',
        'student_id',
        'total_num_join_lesson',
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];
    public function hasOneClass()
    {
        return $this->hasOne(TutorClass::class, 'id','class_id')
            ->select(['id', 'title', 'target_grade_id', 'target_subject_id', 'level', 'status', 'teacher_id','logo','logo_thumb'])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function hasOneStudent()
    {
        return $this->hasOne(TutorStudent::class, 'id','student_id')
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }
}
