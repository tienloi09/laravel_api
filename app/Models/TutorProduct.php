<?php

namespace App\Models;

use App\Models\BaseModel;

class TutorProduct extends BaseModel
{


    protected $table = 'tutor_product';
    //
    protected $fillable = [
        'product_id',
        'total_lesson',
        'grade_id',
        'product_code'
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];
}
