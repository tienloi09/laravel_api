<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorClass;
use App\Models\TutorProgramLessonSection;
use App\Models\TutorMissionCompleted;
use App\Models\TutorRatingComment;
use App\Models\TutorClassLessonSection;
use Illuminate\Support\Facades\DB;
use App\Models\LmsPractice;

class TutorClassLesson extends BaseModel
{
    const LESSON_TYPE_ACTIVE = 1;
    const LESSON_TYPE_INACTIVE = 0;

    protected $table = 'tutor_class_lesson';
    //
    protected $fillable = [
        'class_id',
        'title',
        'lesson_type',
        'lesson_start_time',
        'lesson_end_time',
        'total_rate_point',
        'position',
        'total_num_of_rate',
        'tutor_program_lesson_id',
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted',
        'desc',
        'is_custom',
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted',
    ];

    public function hasOneClass()
    {
        return $this->hasOne(TutorClass::class, 'id','class_id')
            ->select(['id', 'title', 'target_grade_id', 'target_subject_id', 'level', 'status', 'teacher_id','logo','logo_thumb'])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function class()
    {
        return $this->hasOne(TutorClass::class, 'id','class_id')
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function hasManyProgram()
    {
        return $this->hasMany(TutorProgramLessonSection::class, 'lesson_id', 'id')
            ->select(['title', 'type', 'position','is_required'])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }
    public function hasManyTotalProgramRequired()
    {
        return $this->hasMany(TutorProgramLessonSection::class, 'lesson_id', 'id')
            ->select(['title', 'type', 'position','is_required'])
            ->where('is_required', getConfig('program_lesson_section.is_required.active'))
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'))
            ->count();
    }
    public function hasManyTotalMissCompleted(){
        $modelClassSection = new TutorClassLessonSection;
        $modelMissionCompleted = new TutorMissionCompleted;
        return $this->hasMany(TutorMissionCompleted::class, 'lesson_id', 'id')
            ->join($modelClassSection->getTable(), function ($join) use ($modelClassSection, $modelMissionCompleted) {
                $join->on($modelClassSection->qualifyColumn('lesson_id'), '=', $modelMissionCompleted->qualifyColumn('lesson_section_id'));
                $join->where($modelClassSection->qualifyColumn('type'), '=', getConfig('tutor_program_section.type.revise.id'));
                whereColumnDeleted($join, $modelClassSection->getTable());
            })
//            ->select(['id','status'])
            ->where('student_id',apiStudentId())
            ->where('status',getConfig('mission_completed_status.complete'))
            ->where($modelMissionCompleted->qualifyColumn(getConfig('deleted_name')), getConfig('deleted_failed'))
            ->count();
    }
    public function hasManySection()
    {
        return $this->hasMany(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->selectRaw('tutor_class_lesson_section.*, DATE_FORMAT(tutor_class_lesson_section.start_time, "%H:%i:%S") as start_time_mod, DATE_FORMAT(tutor_class_lesson_section.end_time, "%H:%i:%S") as end_time_mod, DATE_FORMAT(tutor_class_lesson_section.start_time, "%d-%m-%Y") as date_time_mod')
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'))
            ->orderBy('position');
    }

    public function hasOneSectionVideoStudy()
    {
        return $this->hasOne(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->where('type', getConfig('tutor_program_section.type.video_online'))
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }
    public function hasOneSectionTypeVideo()
    {
        return $this->hasOne(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->where('type', getConfig('tutor_program_section.type.video'))
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }
    public function hasOneSectionTypePractice()
    {
        return $this->hasOne(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->select(['title', 'type', 'position', 'is_required', 'lms_id', 'image'])
            ->where('type', getConfig('tutor_program_section.type.practice'))
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function hasOneSectionTypeRevise()
    {
        return $this->hasOne(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->select(['title', 'type', 'position', 'is_required', 'lms_id', 'image'])
            ->where([
                'type' => getConfig('tutor_program_section.type.revise'),
                'is_required' => getConfig('program_lesson_section.is_required.active')
            ])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'))
            ->first();
    }

    public function hasOneRatingByUser()
    {
        return $this->hasOne(TutorRatingComment::class, 'lesson_id', 'id')
            ->select('comment','teacher_comment','score')
            ->where('student_id',apiStudentId())
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function hasManyTotalSectionRequired()
    {
        return $this->hasMany(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->where([
                getConfig('deleted_name') => getConfig('deleted_failed'),
                'type' => getConfig('tutor_program_section.type.revise.id'),
                'is_required' => getConfig('program_lesson_section.is_required.active'),
            ])
            ->whereIn('type', [ getConfig('tutor_program_section.type.revise.id'), getConfig('tutor_program_section.type.video.id'), getConfig('tutor_program_section.type.document.id')])
            ->count();
    }

    public function hasManySectionTypeRevise()
    {
        return $this->hasMany(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->select(['title', 'type', 'position', 'is_required', 'lms_id', 'image'])
            ->where([
                'type' => getConfig('tutor_program_section.type.revise'),
                'is_required' => getConfig('program_lesson_section.is_required.active')
            ])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function hasOneSectionTypePractice2()
    {
        $section = $this->hasOne(TutorClassLessonSection::class, 'lesson_id', 'id')
            ->select(['title', 'type', 'position', 'is_required', 'lms_id', 'image', 'id', 'lesson_id'])
            ->where([
                'type' => TutorClassLessonSection::TYPE_PRACTICE,
                'is_required' => getConfig('program_lesson_section.is_required.active')
            ])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'))
            ->orderBy('position')
            ->first();
        
        if($section) {
            $lmsIds = [$section->lms_id];
            $subquery = DB::table('lms_practice_result')
                    ->select(DB::raw("id, MAX(total_correct_question) AS mx"))
                    ->where('user_id' , apiLmsCoreUserId())
                    ->whereIn('practice_id' , $lmsIds)
                    ->groupBy('practice_id')->pluck('id');

            $result = DB::table('lms_practice_result as pp')
            ->selectRaw('pp.practice_id, pp.total_correct_question, pp.total_question')
            ->whereIn('id', $subquery)
            ->get()->toArray();
            
            $kv = [];
            if(count($result) > 0) {    
                foreach($result as $r) {
                    $kv[$r->practice_id] = (array)$r;
                }
            }
            $section->lms_num_question = $kv[$section->lms_id]['total_question'] ?? null;
            $section->num_correct = $kv[$section->lms_id]['total_correct_question'] ?? null;
            $section->is_done = ($section->num_correct) ? 1 : 0;
        }
        return $section;
    }
}
