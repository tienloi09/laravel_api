<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\LmsDocument;
use App\Models\LmsRevise;
use App\Models\LmsPractice;
use App\Models\TutorProgramLesson;
use App\Models\TutorClassLessonSection;

class TutorProgramLessonSection extends BaseModel
{
    protected $table = 'tutor_program_lesson_section';
    //
    protected $fillable = [
        'program_id',
        'lesson_id',
        'type',
        'title',
        'image',
        'position',
        'software_used',
        'desc',
        'lms_id',
        'is_required',
        'link_study',
        'start_time',
        'end_time',
        'lms_duration',
        'lms_num_question',
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];

    const TYPE_LIVESTREAM = 0;
    const TYPE_VIDEO = 1;
    const TYPE_DOCUMENT = 3;
    const TYPE_PRACTICE = 4;

    public function hasOnePractice()
    {
        return $this->hasOne(LmsPractice::class, 'id', 'lms_id')->where('site_id',getConfig('site_id'));
//            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function hasOneRevise()
    {
        return $this->hasOne(LmsRevise::class, 'id', 'lms_id')->where('site_id',getConfig('site_id'))
                ->select('title','content');
//            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }
    public function hasManyMissionCompleted($studentId){
            $data = $this->hasMany(TutorMissionCompleted::class,'lesson_section_id','id')
                ->select('lesson_section_id','status')
                ->where('student_id',$studentId)
                ->where(getConfig('deleted_name'), getConfig('deleted_failed'))
                ->first();
            return $data;
    }

    public function tutorProgramLesson()
    {
        return $this->hasOne(TutorProgramLesson::class, 'id', 'lesson_id');
    }
    public function hasManyClassLessonSection(){
        $data = $this->hasOne(TutorClassLessonSection::class,'program_lesson_section_id','id')
            ->get();
        return $data;
    }
}
