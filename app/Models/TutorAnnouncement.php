<?php

namespace App\Models;

use App\Models\BaseModel;

class TutorAnnouncement extends BaseModel
{
    protected $table = 'tutor_announcement';
    //
    protected $fillable = [
        'title',
        'type',
        'status',
        'start_date',
        'end_date',
        'target_grade_ids',
        'target_subject_ids',
        'target_class_ids',
        'target_level_ids',
        'target_user_ids',
        'teacher_id',
        'content',
        'image'
    ];
    //
}
