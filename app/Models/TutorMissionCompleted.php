<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorClass;
use App\Models\TutorClassLesson;

class TutorMissionCompleted extends BaseModel
{
    protected $table = 'tutor_mission_completed';
    //
    protected $fillable = [
        'class_id',
        'lesson_id',
        'status',
        'student_id'
    ];

    public function hasOneClassMission()
    {
        return $this->hasOne(TutorClass::class, 'id', 'class_id')
            ->select(['id', 'title', 'target_grade_id', 'target_subject_id', 'level', 'status', 'teacher_id', 'logo', 'logo_thumb'])
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }

    public function hasOneClassLesson()
    {
        return $this->hasOne(TutorClassLesson::class, 'id', 'lesson_id')
            ->selectRaw('id, class_id, title, lesson_type, DATE_FORMAT(lesson_start_time,\'%d-%m-%Y %H:%i\') as lesson_start_time, DATE_FORMAT(lesson_end_time,\'%d-%m-%Y %H:%i\') as lesson_end_time, total_rate_point, total_num_of_rate')
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }
}
