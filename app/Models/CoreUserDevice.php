<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class CoreUserDevice extends Authenticatable
{
    protected $table = 'core_user_device';
    //
    protected $fillable = [];

    public function hasManyStudent()
    {
        return $this->hasMany(TutorStudent::class, 'lms_core_user_id', 'user_id')
            ->select('level', 'target_grade_ids', 'target_subject_ids')
            ->where(getConfig('deleted_name'), getConfig('deleted_failed'));
    }
}
