<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorClass;
use App\Models\TutorProgram;

class TutorClassProgram extends BaseModel
{
    protected $table = 'tutor_class_program';
    //
    protected $fillable = [
        'class_id',
        'program_id',
        'apply_start_datetime',
        'apply_end_datetime',
        'lesson_start_at'
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];

    public function tutorClass()
    {
        return $this->hasOne(TutorClass::class, 'id', 'class_id');
    }

    public function hasOneProgram()
    {
        return $this->hasOne(TutorProgram::class, 'id', 'program_id');
    }
}
