<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TutorProgramLessonSection;
use App\Models\TutorProgram;

class TutorProgramLesson extends BaseModel
{
    protected $table = 'tutor_program_lesson';
    //
    protected $fillable = [
        'program_id',
        'title',
        'desc',
        'position'
    ];

    public function tutorProgramLessonSections()
    {
        return $this->hasMany(TutorProgramLessonSection::class, 'lesson_id', 'id')
            ->where('deleted', getConfig('deleted_failed'))
            ->orderBy('position');
    }

    public function tutorProgram()
    {
        return $this->hasOne(TutorProgram::class);
    }
}
