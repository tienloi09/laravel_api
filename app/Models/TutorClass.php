<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\CocDmMonHoc;
use App\Models\CocDmKhoi;
use App\Models\TutorTeacher;
use App\Models\TutorClassSchedule;
use App\Models\TutorClassProgram;
use App\Models\TutorClassLesson;
use App\Models\TutorClassStudent;

class TutorClass extends BaseModel
{
    protected $table = 'tutor_class';
    //
    protected $fillable = [
        'title',
        'target_grade_id',
        'target_subject_id',
        'level',
        'status',
        'teacher_id',
        'slot',
        'start_date',
        'end_date',
        'num_of_lesson',
        'num_of_lesson_passed',
        'start_lesson_position',
        'logo',
        'logo_thumb',
    ];
    protected $hidden = [
        'create_date',
        'update_date',
        'create_user',
        'update_user',
        'deleted'
    ];

    const CLASS_RUNNING = 1;

    const CLASS_FINISHED = 2;

    public function hasOneSubject()
    {
        return $this->hasOne(CocDmMonHoc::class, 'id', 'target_subject_id')
            ->select(['id', 'site_id', 'name', 'alias', 'no', 'description']);
    }

    public function hasOneGradle()
    {
        return $this->hasOne(CocDmKhoi::class, 'id', 'target_grade_id')
            ->select(['id', 'name', 'alias', 'short_name',   'no', 'site_id', 'description', 'cap']);
    }

    public function hasOneTeacher($listColumn = [])
    {
        if(empty($listColumn)) $listColumn = ['id', 'name', 'level', 'target_grade_ids', 'target_subject_ids', 'status', 'desc', 'total_rate_point', 'total_rate_time', 'image', 'total_num_of_lesson', 'phone', 'gender'];
        return $this->hasOne(TutorTeacher::class, 'id', 'teacher_id')
            ->select($listColumn)
            ->where([
                getConfig('deleted_name') => getConfig('deleted_failed')
            ]);
    }

    public function hasManySchedule()
    {
        return $this->hasMany(TutorClassSchedule::class, 'class_id', 'id')
            ->selectRaw('id, class_id, day_of_week, DATE_FORMAT(start_time,\'%d-%m-%Y %H:%i\') as start_time, DATE_FORMAT(end_time,\'%d-%m-%Y %H:%i\') as end_time')
            ->where([
                getConfig('deleted_name') => getConfig('deleted_failed')
            ]);
    }

    public function hasManyTutorClassProgram()
    {
        return $this->hasMany(TutorClassProgram::class, 'class_id', 'id');
    }

    public function hasOneClassProgram()
    {
        return $this->hasOne(TutorClassProgram::class, 'class_id', 'id')->whereNull('apply_end_datetime');
    }

    public function classLessons()
    {
        return $this->hasMany(TutorClassLesson::class, 'class_id')
            ->where([
                getConfig('deleted_name') => getConfig('deleted_failed')
            ]);
    }

    public function hasOneLessonNextTime()
    {
        $currentTime = formatDateTime('','Y-m-d H:i');
        return $this->hasOne(TutorClassLesson::class, 'class_id', 'id')
            ->selectRaw("title, DATE_FORMAT(lesson_start_time,'%d-%m-%Y %H:%i') as lesson_start_time, DATE_FORMAT(lesson_end_time,'%d-%m-%Y %H:%i') as lesson_end_time ")
            ->whereRaw("DATE_FORMAT(lesson_end_time,'%Y-%m-%d %H:%i') >= '" . $currentTime . "'")
            ->orderBy('lesson_start_time')
            ->first();
    }

    public function classStudents()
    {
        return $this->hasMany(TutorClassStudent::class, 'class_id', 'id')->where([
            getConfig('deleted_name') => getConfig('deleted_failed')
        ]);
    }
}
