<?php

namespace App\Models;

use App\Models\BaseModel;

class TutorResult extends BaseModel
{
    protected $table = 'tutor_result';
    //
    protected $fillable = [
        'class_id',
        'lesson_id',
        'student_id',
        'score',
        'is_correct',
        'lms_id'
    ];
}
