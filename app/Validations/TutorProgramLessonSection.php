<?php
namespace App\Validations;

use App\Validations\BaseValidation;
use Illuminate\Validation\Rule;
use App\Models\LmsLessonVideo;
use App\Models\LmsHomeWork;
use App\Models\LmsPractice;
use App\Models\TutorProgramLessonSection as TutorProgramLessonSectionModel;

class TutorProgramLessonSection extends BaseValidation
{
    protected $storeRule = [
    ];

    protected $updateRule = [
    ];

    protected $messages = [
        'title.required' => 'Thiếu tên buổi học!',
        'program_id.required' => 'Thiếu mã khung chương trình!',
        'lesson_id.required' => 'Thiếu mã bài học!',
        'image.required' => 'Thiếu avatar!',
        'type.required' => 'Thiếu loại thành phần!',
        'desc.required' => 'Thiếu nội dung buổi học!',
        'lms_id.lms_id' => 'Không tồn tại bài giảng!',
        'lms_id.integer' => 'Sai định dạng mã bài giảng!',
    ];

    public function storeValidate($request)
    {
        $storeRule = [
            'program_id' => 'required',
            'lesson_id' => 'required',
            'image' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('tutor_program_section.type.video_online.id')),
            'type' => 'required',
            'title' => 'required',
            'desc' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('tutor_program_section.type.video_online.id')),
            'lms_id' => function ($attribute, $value, $fail) {
                $type = request('type');
                $lmsId = request('lms_id', null);
                if($lmsId === null) {
                    return true;
                }
                if($type == TutorProgramLessonSectionModel::TYPE_VIDEO) {
                    $model = new LmsLessonVideo();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại video có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorProgramLessonSectionModel::TYPE_DOCUMENT) {
                    $model = new LmsHomeWork();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại tài liệu có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorProgramLessonSectionModel::TYPE_PRACTICE) {
                    $model = new LmsPractice();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại bài luyện tập có id '.$lmsId.' trên hệ thống');
                    }
                }
            },
        ];

        return $this->validate($request, $storeRule, $this->messages);
    }

    public function updateValidate($request)
    {
        $updateRule = [
            'program_id' => 'required',
            'lesson_id' => 'required',
            'type' => 'required',
            'title' => 'required',
            'desc' => Rule::requiredIf($request['type'] == getConfig('tutor_program_section.type.video_online.id')),
            'lms_id' => function ($attribute, $value, $fail) {
                $type = request('type');
                $lmsId = request('lms_id', null);
                if($lmsId === null) {
                    return true;
                }
                if($type == TutorProgramLessonSectionModel::TYPE_VIDEO) {
                    $model = new LmsLessonVideo();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại video có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorProgramLessonSectionModel::TYPE_DOCUMENT) {
                    $model = new LmsHomeWork();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại tài liệu có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorProgramLessonSectionModel::TYPE_PRACTICE) {
                    $model = new LmsPractice();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại bài luyện tập có id '.$lmsId.' trên hệ thống');
                    }
                }
            },
        ];

        return $this->validate($request, $updateRule, $this->messages);
    }
}


