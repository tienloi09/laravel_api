<?php
namespace App\Validations;

// use Illuminate\Validation\Validator;
use App\Validations\BaseValidation;

class TutorStudent extends BaseValidation
{
    protected function _rules(){
        $rules = [
            'level' => 'require'
        ];
        return $rules;
    }

    protected $updateRule = [
        // 'name' => 'required|max:100',
        // 'mobile' => 'required',
        // 'age' => 'required',
        'target_grade_ids' => 'required',
        'target_subject_ids' => 'required',
    ];

    protected $storeRule = [
        'lms_core_user_id' => 'required',
        'target_grade_ids' => 'required',
        'target_subject_ids' => 'required',
    ];

    protected $messages = [
        'lms_core_user_id.required' => 'Mã học viên LMS không được để trống',
        // 'name.required' => 'Tên học viên không được để trống',
        // 'name.max' => 'Tên học viên không được quá 100 kí tự',
        // 'mobile.required' => 'Số điện thoại học viên không được đê trống',
        // 'age.required' => 'Tuổi học viên không được để trống',
        'target_grade_ids.required' => 'Thiếu lớp đăng ký',
        'target_subject_ids.required' => 'Thiếu môn học đăng ký',
    ];

    protected $setLessonRule = [
        'student_id' => 'required',
        'class_id' => 'required',
        'lesson_pass' => 'required',
        'total_lesson' => 'required',
        'comment' => 'required',
    ];

    protected $messagesSetLesson = [
        'student_id.required' => 'Mã học viên không tồn tại',
        'class_id.required' => 'Mã lớp không tồn tại',
        'lesson_pass.required' => 'Số buổi đã học không tồn tại',
        'total_lesson.required' => 'Tổng số buổi học không tồn tại',
        'comment.required' => 'Vui lòng nhập lý do thay đổi',
    ];
    public function storeValidate($request)
    {
        return $this->validate($request, $this->storeRule, $this->messages);
    }

    public function updateValidate($request)
    {
        return $this->validate($request, $this->updateRule, $this->messages);
    }

    public function setLessonValidate($request)
    {
        return $this->validate($request, $this->setLessonRule, $this->messagesSetLesson);
    }
}
