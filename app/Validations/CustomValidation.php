<?php

namespace App\Validations;

use App\Models\TutorClass;
use App\Models\TutorClassLesson;
use App\Models\TutorClassStudent;
use App\Models\TutorClassSchedule;
use App\Models\TutorStudent;
use App\Models\LmsPractice;

/**
 * Class CustomValidation
 * @package App\Validations
 */class CustomValidation extends \Illuminate\Validation\Validator
{
    public function validateFindClassStudent($attribute, $value, $parameter)
    {
        $classStudentTable = (new TutorClassStudent);
        $classTable = (new TutorClass);
        $builder = TutorClass::select()
            ->join($classStudentTable->getTable(), function ($join) use ($classStudentTable, $classTable) {
                $join->on($classStudentTable->qualifyColumn('class_id'), '=', $classTable->qualifyColumn('id'));
                $join->where($classStudentTable->qualifyColumn(getConfig('deleted_name')) , getConfig('deleted_failed'));
            })
            ->where([
                $classTable->qualifyColumn('id') => $value,
                $classTable->qualifyColumn(getConfig('deleted_name')) => getConfig('deleted_failed'),
            ])
            ->get()->toArray();
        if (empty($builder)) return false;
        return true;
    }

    public function validateFindClassLessonStudent($attribute, $value, $parameter)
    {
        $classLessonTable = (new TutorClassLesson);
        $builder = $classLessonTable->select('id')
            ->where([
                $classLessonTable->qualifyColumn('id') => $value,
                $classLessonTable->qualifyColumn(getConfig('deleted_name')) => getConfig('deleted_failed')
            ])
            ->get()->toArray();
        if (empty($builder)) return false;
        return true;
    }

    public function validateDateTime($attribute, $value, $parameter)
    {
        //regex format date time "Y-m-d H:i";
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[1-9]|2[0-3]):([0-5][0-9])$/", $value)) {
            return true;
        } else {
            return false;
        }
    }

    public function validateDateMonthYear($attribute, $value, $parameter)
    {
        //regex format date time "d-m-Y";
        if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/", $value)) {
            return true;
        } else {
            return false;
        }
    }
    public function validateStudentActive($attribute, $value, $parameter)
    {
        $modelStudent = new TutorStudent;
        if(!is_array($value)) return false;
        $totalStudent = $modelStudent->select()->whereIn($modelStudent->qualifyColumn('id'),$value)
            ->where([
                $modelStudent->qualifyColumn(getConfig('deleted_name')) => getConfig('deleted_failed'),
            ])->count();
        if($totalStudent == count($value))
            return true;
        else return false;
    }
    public function validateDateSchedule($attribute, $value, $parameter)
    {
        /**
         * validate schedule date time
         */
        $listTime = [];
        foreach ($value as $time){
            $startTime = str_replace(":","",$time['start_time']);
            $endTime = str_replace(":","",$time['end_time']);
            $dayOfWeek = $time['day_of_week'];
            if($startTime > $endTime) return false;
            if (isset($listTime[$dayOfWeek])) {
                foreach ($listTime[$dayOfWeek] as $timeDetail){
                    if(
                        ($timeDetail['start_time'] >= $startTime && $timeDetail['start_time'] <= $endTime) ||
                        ($timeDetail['end_time'] >= $startTime && $timeDetail['end_time'] <= $endTime) ||
                        ($timeDetail['start_time'] <= $startTime && $timeDetail['end_time'] >= $endTime)
                    ){
                        return false;
                    }else{
                        array_push($listTime[$dayOfWeek], [
                            'start_time' => $startTime,
                            'end_time' => $endTime
                        ]);
                    }
                }
            } else $listTime[$dayOfWeek][] = [
                'start_time' => $startTime,
                'end_time' => $endTime
            ];
        }
        return true;
    }

    public function validateLmsId($attribute, $value, $parameter)
    {
        /**
         * validate lms_id
         */
        $lmsPractice = (new LmsPractice);
        $practice = $lmsPractice->where('active', getConfig('deleted_active'))->find($value);
        if ($value && !$practice) {
            return false;
        }

        return true;
    }

    public function validateClassHasBeenFinished($attribute, $value, $parameter)
    {
        $classModel = (new TutorClass);
        $class = $classModel->find($value);
        if ($class->status == getConfig('tutor_class.status.finish.id')) {
            return false;
        }

        return true;
    }
}
