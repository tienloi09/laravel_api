<?php
namespace App\Validations;

use App\Validations\BaseValidation;

class TutorProgram extends BaseValidation
{
    protected $storeRule = [
        'name' => 'required|max:74',
        'target_grade_id' => 'required',
        'level' => 'required',
        'status' => 'required',
        'target_subject_id' => 'required',
    ];

    protected $updateRule = [
        'name' => 'required|max:74',
        'target_grade_id' => 'required',
        'level' => 'required',
        'status' => 'required',
        'target_subject_id' => 'required',
    ];

    protected $messages = [
        'name.required' => 'Thiếu tên khung chương trình!',
        'name.max' => 'Tên khung chương trình không được quá 74 kí tự!',
        'target_grade_id.required' => 'Thiếu lớp!',
        'level.required' => 'Thiếu trình độ!',
        'status.required' => 'Thiếu trạng thái!',
        'target_subject_id.required' => 'Thiếu môn học!',
    ];

    public function storeValidate($request)
    {
        return $this->validate($request, $this->storeRule, $this->messages);
    }

    public function updateValidate($request)
    {
        return $this->validate($request, $this->updateRule, $this->messages);
    }
}


