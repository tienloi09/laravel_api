<?php
namespace App\Validations;

use App\Validations\BaseValidation;

class TutorClassLesson extends BaseValidation
{
    protected $storeRule = [
        'title' => 'required',
        'class_id' => 'class_has_been_finished|required',
        'desc' => 'required',
    ];

    protected $updateRule = [
        'title' => 'required',
        'class_id' => 'class_has_been_finished|required',
        'desc' => 'required',
    ];

    protected $messages = [
        'title.required' => 'Thiếu tên buổi học!',
        'class_id.required' => 'Thiếu mã lớp!',
        'desc.required' => 'Thiếu nội dung buổi học!'
    ];

    public function searchByClassProgramValid($data)
    {
        $rule = [
            'class_id' => 'required|integer',
            'program_id' => 'required|integer',
        ];
        $message = [
            'class_id.required' => getValidation('tutor_program.class_id_missing'),
            'class_id.integer' => getValidation('integer'),
            'program_id.required' => getValidation('tutor_program.program_id_missing'),
            'program_id.integer' => getValidation('integer'),
        ];
        return $this->validate($data,$rule,$message);
    }

    public function storeValidate($request)
    {
        $messages = [
            'title.required' => 'Thiếu tên buổi học!',
            'class_id.required' => 'Thiếu mã lớp!',
            'desc.required' => 'Thiếu nội dung buổi học!',
            'class_id.class_has_been_finished' => getValidation('tutor_class.class_has_been_finished'),
        ];

        return $this->validate($request, $this->storeRule, $messages);
    }

    public function updateValidate($request)
    {
        $messages = [
            'title.required' => 'Thiếu tên buổi học!',
            'class_id.required' => 'Thiếu mã lớp!',
            'desc.required' => 'Thiếu nội dung buổi học!',
            'class_id.class_has_been_finished' => getValidation('tutor_class.class_has_been_finished')
        ];

        return $this->validate($request, $this->updateRule,$messages);
    }

    public function suspendValidate($request)
    {
        $suspendRule = [
            'class_id' => 'required',
            'lesson_start_time' => 'required',
            'lesson_end_time' => 'required',
        ];
        $messages = [
            'class_id.required' => 'Thiếu mã lớp!',
            'lesson_start_time.required' => 'Thiếu thời gian bắt đầu!',
            'lesson_end_time.required' => 'Thiếu thời gian kết thúc!',
        ];

        return $this->validate($request, $suspendRule, $messages);
    }
}


