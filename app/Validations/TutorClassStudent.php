<?php
namespace App\Validations;

use App\Validations\BaseValidation;

class TutorClassStudent extends BaseValidation
{
    public function validateListNearDay($data)
    {
        $rule = [
            'grade_id' => 'required|integer',
        ];
        $message = [
            'grade_id.required' => getValidation('required',['']),
            'grade_id.integer' => getValidation('integer'),
        ];
        return $this->validate($data, $rule, $message);
    }
}


