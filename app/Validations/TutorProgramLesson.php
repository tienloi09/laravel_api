<?php
namespace App\Validations;

use App\Validations\BaseValidation;

class TutorProgramLesson extends BaseValidation
{
    protected $storeRule = [
        'title' => 'required',
        'program_id' => 'required',
        'desc' => 'required',
    ];

    protected $updateRule = [
        'title' => 'required',
        'program_id' => 'required',
        'desc' => 'required',
    ];

    protected $messages = [
        'title.required' => 'Thiếu tên tên buổi học!',
        'program_id.required' => 'Thiếu mã khung chương trình!',
        'desc.required' => 'Thiếu nội dung buổi học!',
        'position.required' => 'Thiêu Thứ tự buổi học!',
    ];

    public function storeValidate($request)
    {
        return $this->validate($request, $this->storeRule, $this->messages);
    }

    public function updateValidate($request)
    {
        return $this->validate($request, $this->updateRule, $this->messages);
    }
}

