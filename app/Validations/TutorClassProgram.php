<?php
namespace App\Validations;

use App\Validations\BaseValidation;

class TutorClassProgram extends BaseValidation
{
    public function searchByClassValid($data)
    {
        $rule = [
            'class_id' => 'required|integer'
        ];
        $message = [
            'class_id.required' => getValidation('tutor_class_program.class_id_not_found'),
            'class_id.integer' => getValidation('integer'),
        ];
        return $this->validate($data,$rule,$message);
    }
}

