<?php
namespace App\Validations;

// use Illuminate\Validation\Validator;
use App\Validations\BaseValidation;

class TutorTeacher extends BaseValidation
{
    protected $updateRule = [
        'name' => 'required|max:100',
        'phone' => 'required| numeric | digits:10',
        'avatar_url' => 'required',
        'gender' => 'required',
        'level' => 'required',
        'yearsOfExp' => 'required',
        'gradeIds' => 'required',
        'subjectIds' => 'required',
        'status' => 'required',
    ];

    protected $storeRule = [
        'name' => 'required|max:100',
        'phone' => 'required| numeric | digits:10',
        'avatar_url' => 'required',
        'gender' => 'required',
        'level' => 'required',
        'yearsOfExp' => 'required',
        'gradeIds' => 'required',
        'subjectIds' => 'required',
        'status' => 'required',
    ];

    protected $messages = [
        'name.required' => 'Tên giảng viên không được để trống',
        'name.max' => 'Tên giảng viên không được quá 100 kí tự',
        'avatar_url.required' => 'Ảnh giảng viên không được để trống',
        'gender.required' => 'Giới tính không được để trống',
        'level.required' => 'Trình độ giảng viên không được để trống',
        'yearsOfExp.required' => 'Kinh nghiệm giảng viên không được để trống',
        'gradeIds.required' => 'Khối đăng ký không được để trống',
        'subjectIds.required' => 'Môn đăng ký không được để trống',
        'status.required' => 'Trạng thái giảng viên không được để trống',
        'phone.required' => 'Số điện thoại giảng viên không được để trống',
        'phone.numeric' => 'Sai định dạng số điện thoại',
        'phone.digits' => 'Sai định dạng số điện thoại',
    ];
    public function storeValidate($request)
    {
        return $this->validate($request, $this->storeRule, $this->messages);
    }

    public function updateValidate($request)
    {
        return $this->validate($request, $this->updateRule, $this->messages);
    }
}
