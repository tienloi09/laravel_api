<?php
namespace App\Validations;

use App\Validations\BaseValidation;
use Illuminate\Validation\Rule;
use App\Models\LmsLessonVideo;
use App\Models\LmsHomeWork;
use App\Models\LmsPractice;
use App\Models\TutorClassLessonSection as TutorClassLessonSectionModel;

class TutorClassLessonSection extends BaseValidation
{
    protected $storeRule = [
    ];

    protected $updateRule = [
    ];

    protected $messages = [
        'title.required' => 'Thiếu tên buổi học!',
        'lesson_id.required' => 'Thiếu mã bài học!',
        'image.required' => 'Thiếu avatar!',
        'type.required' => 'Thiếu loại thành phần!',
        'desc.required' => 'Thiếu nội dung buổi học!',
        'start_time.required' => 'Thiếu thời gian bắt đầu!',
        'end_time.required' => 'Thiếu thời gian kết thúc!',
        'software_used.required' => 'Thiếu phần mềm!',
        'link_study.required' => 'Thiếu link vào lớp!',
    ];

    public function storeValidate($request)
    {
        $storeRule = [
            'lesson_id' => 'required',
            'image' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'type' => 'required',
            'title' => 'required',
            'desc' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'start_time' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'end_time' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'software_used' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'link_study' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'lms_id' => function ($attribute, $value, $fail) {
                $type = request('type');
                $lmsId = request('lms_id', null);
                if($lmsId === null) {
                    return true;
                }
                if($type == TutorClassLessonSectionModel::TYPE_VIDEO) {
                    $model = new LmsLessonVideo();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại video có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorClassLessonSectionModel::TYPE_DOCUMENT) {
                    $model = new LmsHomeWork();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại tài liệu có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorClassLessonSectionModel::TYPE_PRACTICE) {
                    $model = new LmsPractice();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại bài luyện tập có id '.$lmsId.' trên hệ thống');
                    }
                }
            }
        ];

        return $this->validate($request, $storeRule, $this->messages);
    }

    public function updateValidate($request)
    {
        $updateRule = [
            'lesson_id' => 'required',
            'type' => 'required',
            'title' => 'required',
            'desc' => Rule::requiredIf($request['type'] == getConfig('program_lesson_section_type.video_online')),
            'start_time' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'end_time' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'software_used' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'link_study' => Rule::requiredIf(isset($request['type']) && $request['type'] == getConfig('program_lesson_section_type.video_online')),
            'lms_id' => function ($attribute, $value, $fail) {
                $type = request('type');
                $lmsId = request('lms_id', null);
                if($lmsId === null) {
                    return true;
                }
                if($type == TutorClassLessonSectionModel::TYPE_VIDEO) {
                    $model = new LmsLessonVideo();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại video có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorClassLessonSectionModel::TYPE_DOCUMENT) {
                    $model = new LmsHomeWork();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại tài liệu có id '.$lmsId.' trên hệ thống');
                    }
                }
                if($type == TutorClassLessonSectionModel::TYPE_PRACTICE) {
                    $model = new LmsPractice();
                    $item = $model->find($lmsId);
                    if(!$item) {
                        return $fail('Không tồn tại bài luyện tập có id '.$lmsId.' trên hệ thống');
                    }
                }
            }
        ];

        return $this->validate($request, $updateRule, $this->messages);
    }
}

?>
