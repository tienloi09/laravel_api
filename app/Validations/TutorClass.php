<?php
namespace App\Validations;

use App\Validations\BaseValidation;

class TutorClass extends BaseValidation
{
    public function storeValidate($data = [])
    {
        $rule = [
            'title' => 'required',
            'target_subject_id' => 'required|integer',
            'target_grade_id' => 'required|integer',
            'level' => 'required|integer',
            'status' => 'required|integer',
            'teacher_id' => 'required|integer',
            'student_ids' => 'student_active',
            'start_date' => 'required|date_month_year',
            'end_date' => 'required|date_month_year|after_or_equal:start_date',
            'day_of_weeks' => 'required|date_schedule',
        ];
        $message = [
            'student_ids.student_active' => 'Số học sinh được chọn không thoả mãn',
            'day_of_weeks.date_schedule' => 'Thời gian bị trùng lặp'
        ];
        if(isset($data['day_of_weeks'])){
            foreach ($data['day_of_weeks'] as $key => $day){
                $rule['day_of_weeks.'.$key.'.day_of_week'] = 'required|integer';
                $rule['day_of_weeks.'.$key.'.start_time'] = 'required|date_format:H:i';
                $rule['day_of_weeks.'.$key.'.end_time'] = 'required|date_format:H:i';
                $message['day_of_weeks.'.$key.'.start_time.date_format'] = 'Không đúng định dạng';
                $message['day_of_weeks.'.$key.'.end_time.date_format'] = 'Không đúng định dạng';
            }
        }
        return $this->validate($data, $rule, $message);
    }
    public function addProgramToClassValid($data)
    {
        $rule = [
            'program_id' => 'required|integer',
            'start_lesson_id' => 'required',
        ];
        $message = [
            'class_id.required' => getValidation('tutor_program.class_id_missing'),
            'start_lesson_id.required' => getValidation('tutor_program.start_lesson_id_not_found'),
        ];
        return $this->validate($data,$rule,$message);
    }

    public function searchByClassProgramValid($data)
    {
        $rule = [
            'class_id' => 'required|integer',
            'program_id' => 'required|integer',
        ];
        $message = [
            'class_id.required' => getValidation('tutor_program.class_id_missing'),
            'class_id.integer' => getValidation('integer'),
            'program_id.required' => getValidation('tutor_program.program_id_missing'),
            'program_id.integer' => getValidation('integer'),
        ];
        return $this->validate($data,$rule,$message);
    }

    public function validateById($data)
    {
        $rule = [
            'id' => 'required|integer|min:1',
        ];
        $message = [
            'id.required' => getValidation('required'),
            'id.integer' => getValidation('integer'),
            'id.min' => 'Số lượng yêu cầu không thoả mãn',
        ];
        return $this->validate($data, $rule, $message);
    }

    public function updateValidate($data = [])
    {
        $rule = [
            'title' => 'required',
            'target_subject_id' => 'required|integer',
            'target_grade_id' => 'required|integer',
            'level' => 'required|integer',
            'status' => 'required|integer',
            'teacher_id' => 'integer',
            'student_ids' => 'student_active',
            'start_date' => 'required|date_month_year',
            'end_date' => 'required|date_month_year',
            'day_of_weeks' => 'required|date_schedule',
        ];
        $message = [
            'student_ids.student_active' => 'Số học sinh được chọn không thoả mãn',
            'day_of_weeks.date_schedule' => 'Thời gian bị trùng lặp'
        ];
        if(isset($data['day_of_weeks'])){
            foreach ($data['day_of_weeks'] as $key => $day){
                $rule['day_of_weeks.'.$key.'.day_of_week'] = 'required|integer';
                $rule['day_of_weeks.'.$key.'.start_time'] = 'required|date_format:H:i';
                $rule['day_of_weeks.'.$key.'.end_time'] = 'required|date_format:H:i';
                $message['day_of_weeks.'.$key.'.start_time.date_format'] = 'Không đúng định dạng';
                $message['day_of_weeks.'.$key.'.end_time.date_format'] = 'Không đúng định dạng';
            }
        }
        return $this->validate($data, $rule, $message);
    }

}


