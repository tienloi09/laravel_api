<?php
namespace App\Validations;

use App\Validations\BaseValidation;

class TutorRatingComment extends BaseValidation
{
    protected function _rules(){
        $rules = [
            'score' => 'required',
            'comment' => 'required',
        ];
        return $rules;
    }
    protected function _messageDefault(){
        $messages = [
            'score.required' => getValidation('required'),
            'comment.required' => getValidation('required')
        ];
        return $messages;
    }
    public function storeValidate($data)
    {
        $rules = [
            'class_id' => 'required|find_class_student:'.apiStudentId(),
            'lesson_id' => 'required|find_class_lesson_student:'.apiStudentId(),
        ];
        $message = [
            'class_id.find_class_student' => getValidation('tutor_rating_comment.class_not_found'),
            'lesson_id.find_class_lesson_student' => getValidation('tutor_rating_comment.lesson_not_found'),
        ];
        $rules = array_merge($this->_rules(),$rules);
        $message = array_merge($this->_messageDefault(),$message);
        return $this->validate($data, $rules,$message);
    }

    public function updateValidate($request)
    {
        return $this->validate($request, $this->_rules());
    }
}


