<?php
namespace App\Validations;

// use Illuminate\Validation\Validator;
use App\Validations\BaseValidation;

class TutorPrioBox extends BaseValidation
{

    protected $updateRule = [
        'notify_name' => 'required|max:200',
        'notify_type' => 'required',
        'notify_status' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'teacher_id' => 'required',
        'content' => 'required',
    ];

    protected $storeRule = [
        'notify_name' => 'required|max:200',
        'notify_type' => 'required',
        'notify_status' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'teacher_id' => 'required',
        'content' => 'required',
    ];

    protected $messages = [
        'notify_name.required' => 'Tiêu đề thông báo không được để trống',
        'notify_name.max' => 'Tiêu đề thông báo dài quá 200 ký tự',
        'notify_type.required' => 'Loại hình thông báo không được để trống',
        'notify_status.required' => 'Trạng thái thông báo không được để trống',
        'start_time.required' => 'Thời gian bắt đầu không được đê trống',
        'end_time.required' => 'Thời gian kết thúc không được đê trống',
        'teacher_id.required' => 'Giảng viên không được để trống',
        'content.required' => 'Nội dung thông báo không được để trống',
    ];
    public function storeValidate($request)
    {
        return $this->validate($request, $this->storeRule, $this->messages);
    }

    public function updateValidate($request)
    {
        return $this->validate($request, $this->updateRule, $this->messages);
    }
}
