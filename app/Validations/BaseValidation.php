<?php

namespace App\Validations;

use Illuminate\Support\Facades\Validator;

class BaseValidation
{
    public function validate($data = [], $rules = [], $messages = [])
    {
        foreach ($rules as $key => $required){
            if(is_string($required)) {
                $lstRequired = explode('|', $required);
                foreach ($lstRequired as $item){
                    if(!in_array($key.'.'.$item, array_keys($messages))){
                        $messages[$key.'.'.$item] = getValidation($item,['attribute' => $key]);
                    }
                }
            }
        }
        return Validator::make($data, $rules, $messages);
    }
}
