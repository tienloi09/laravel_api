<?php

if (!function_exists('getConstant')) {
    function getConstant($name = '')
    {
        return Config::get('constant.' . $name);
    }
}
if (!function_exists('getConfig')) {
    function getConfig($name = '')
    {
        return Config::get('config.' . $name);
    }
}
if (!function_exists('getValidation')) {
    function getValidation($name = '')
    {
        return Config::get('validations.' . $name);
    }
}
if (!function_exists('whereColumnDeleted')) {
    function whereColumnDeleted(&$join, $tableName = '', $columnName = '')
    {
        $columnName = $columnName ? $columnName : getConfig('deleted_name');
        $columnName = $tableName ? $tableName . '.' . $columnName : $columnName;
        $join->where($columnName, '=', getConfig('deleted_failed'));
    }
}
if (!function_exists('getMessage')) {
    function getMessage($name = '')
    {
        return getConstant('message.' . $name);
    }
}

if (!function_exists('logSql')) {
    function logSql($builder)
    {
        logDebug($builder->toSql());
        return dd($builder->toSql());
    }
}
if (!function_exists('logError')) {
    function logError($exception, $message = '')
    {
        \Illuminate\Support\Facades\Log::error($exception);
    }
}
if (!function_exists('logInfo')) {
    function logInfo($exception, $message = '')
    {
        \Illuminate\Support\Facades\Log::info($exception);
    }
}
if (!function_exists('logDebug')) {
    function logDebug($exception, $message = '')
    {
        \Illuminate\Support\Facades\Log::debug($exception);
    }
}
if (!function_exists('logWarning')) {
    function logWarning($exception, $message = '')
    {
        \Illuminate\Support\Facades\Log::warning($exception);
    }
}
if (!function_exists('logRequest')) {
    function logRequest($request, $message = '')
    {
        \Illuminate\Support\Facades\Log::notice($request->all());
    }
}
if (!function_exists('loadMigrationDefault')) {
    function loadMigrationDefault(&$table)
    {
        $table->tinyInteger('deleted')->default(getConfig('deleted_failed'));
        $table->integer('create_user')->nullable();
        $table->integer('update_user')->nullable();
        $table->timestamp('create_date')->nullable();
        $table->timestamp('update_date')->nullable();
    }
}
if (!function_exists('formatDateTime')) {
    function formatDateTime($dateTime = '', $formatDate = 'd/m/Y H:i')
    {
        if (empty($dateTime)) $dateTime = \Carbon\Carbon::now();
        return \Carbon\Carbon::parse($dateTime)->format($formatDate);
    }
}
if (!function_exists('explodeMapInt')) {
    function explodeMapInt($str, $separator = ',')
    {
        return array_map('intval', explode($separator, $str));
    }
}
if (!function_exists('loadImageUrl')) {
    function loadImageUrl($url)
    {
        return env('IMAGE_URL', env('APP_URL', '')) . $url;
    }
}
if(!function_exists('parserTokenApi')){
    function parserTokenApi($bearerToken){
        $responseToken = [
            'success' => false,
            'statusCode' => ''
        ];

        if (!$bearerToken) {
            return $responseToken;
        }
        $tokenParts = explode(".", $bearerToken);
        if (count($tokenParts) < 2) {
            return $responseToken;
        }
        $tokenPayload = base64_decode($tokenParts[1]);
        $objUserDevice = json_decode($tokenPayload);
        if (empty($objUserDevice->id)) {
            return $responseToken;
        }
        $responseToken['success'] = true;
        $responseToken['user_device'] = $objUserDevice;
        return $responseToken;
    }
}

if (!function_exists('apiGuard')) {
    /**
     * @param string $default
     * @return mixed
     */
    function apiGuard($default = 'api')
    {
        return Auth::guard($default);
    }
}


if (!function_exists('apiStudentId')) {
    /**
     * @param string $default
     * @return mixed
     */
    function apiStudentId($default = 'api')
    {
        $user = apiGuard($default)->user();
        return (isset($user->student_id) ? $user->student_id : $user->id);
    }
}


if (!function_exists('apiUserId')) {
    /**
     * @param string $default
     * @return mixed
     */
    function apiUserId($default = 'api')
    {
        $user = apiGuard($default)->user();
        return (isset($user->lms_core_user_id)) ? $user->lms_core_user_id : $user->id;
    }
}

if (!function_exists('clauseSearch')) {
    /**
     * @param string $str
     * @param string $default
     */
    function clauseSearch($str = '', $default = '_')
    {

    }
}

if (!function_exists('convertSchedule')) {
    /**
     * @param array $listSchedule
     * @return array
     */

    function convertSchedule($listSchedule = []){
        $listData = [];
        foreach($listSchedule as $schedule){
            $startTime = formatDateTime($schedule['start_time'],"H:i");
            $endTime = formatDateTime($schedule['end_time'],"H:i");
            if(!isset($listData[$startTime.'-'.$endTime]))
                $listData[$startTime.'-'.$endTime] = [
                    'start_time' => $startTime,
                    'end_time' => $endTime,
                    'day_of_weeks' => [$schedule['day_of_week']]
                ];
            else  array_push($listData[$startTime.'-'.$endTime]['day_of_weeks'] ,$schedule['day_of_week']);
        }

        return array_values($listData);
    }
}

if (!function_exists('apiLmsCoreUserId')) {
    /**
     * @param string $default
     * @return mixed
     */
    function apiLmsCoreUserId($default = 'api')
    {
        $userDevice = apiGuard($default)->user();
        return $userDevice->lms_core_user_id;
    }
}
