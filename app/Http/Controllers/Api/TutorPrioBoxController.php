<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorPrioBoxRepository;
use App\Repositories\TutorClassStudentRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TutorPrioBoxController extends ApiController
{
    public function __construct(TutorPrioBoxRepository $TutorPrioBoxRepository){
        $this->setRepository($TutorPrioBoxRepository);
    }

    public function index()
    {
        try {
            $userId = apiStudentId();
            $gradeId = request()->grade_id ?? null;
            if (!empty($userId)) {
                $allNotifs = $this->getRepository()->getNotifyByUser($userId);
                $allNotifsKeyValue = [];
                if(!empty($allNotifs)) {
                    foreach($allNotifs as $notif) {
                        $allNotifsKeyValue[$notif['id']] = $notif;
                    }
                }
                
                if(count($allNotifsKeyValue) < 3) {
                    $csRepo = app(TutorClassStudentRepository::class);
                    $classIds = $csRepo->getClassesOfStudent($userId, $gradeId);
                    
                    if(!empty($classIds)) {
                        foreach($classIds as $classId) {
                            $notifs = $this->getRepository()->getNotifyByClass($classId);
                            if(!empty($notifs)) {
                                foreach($notifs as $notif) {
                                    if(!array_key_exists($notif['id'], $allNotifsKeyValue)) {
                                        $allNotifsKeyValue[$notif['id']] = $notif;
                                    }
                                    if(count($allNotifs) > 3) {
                                        break;
                                    }
                                }
                            }
                            
                        }
                    }
                    if(count($allNotifs) < 3) {
                        $notifs = $this->getRepository()->getNotifyByGrade($gradeId);
                        if(!empty($notifs)) {
                            foreach($notifs as $notif) {
                                if(!array_key_exists($notif['id'], $allNotifsKeyValue)) {
                                    $allNotifsKeyValue[$notif['id']] = $notif;
                                }
                                if(count($allNotifs) > 3) {
                                    break;
                                }
                            }
                        }
                    }
                }

                $res = [];
                foreach ($allNotifsKeyValue as $noti) {
                    $noti['target_class_id'] = $noti['target_class_id'] ? explode(',', $noti['target_class_id']) : [];
                    $noti['target_user_id'] = $noti['target_user_id'] ? explode(',', $noti['target_user_id']) : [];
                    $noti['target_subject_id'] = $noti['target_subject_id'] ? explode(',', $noti['target_subject_id']) : [];
                    $noti['target_grade_id'] = $noti['target_grade_id'] ? explode(',', $noti['target_grade_id']) : [];

                    array_push($res, $noti);
                }

                return $this->renderJson($res);
            } else {
                return $this->renderJsonError([], 'User không tồn tại');
            }
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }
}
