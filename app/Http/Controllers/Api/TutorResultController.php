<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorResultRepository;

class TutorResultController extends ApiController
{
    public function __construct(TutorResultRepository $tutorResultRepository)
    {
        $this->setRepository($tutorResultRepository);
    }

    public function getResultByClassId($classId = null)
    {
        try {
            
            $lstResult = $this->getRepository()->getListByClass([ 'class_id' => $classId ]);
            
            $res = [];
            $res['total_question'] = 0;
            $res['number_record'] = count($lstResult);
            $res['total_correct_question'] = 0;
            if(!empty($lstResult)) {
                foreach ($lstResult as $item) {
                    $res['total_question'] += $item['total_question'];
                    $res['total_correct_question'] += $item['total_correct_question'];
                }
            }

            return $this->renderJson($res);
        } catch (\Exception $exception) {}
    }
}
