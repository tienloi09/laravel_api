<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\TutorClass;
use App\Repositories\TutorClassRepository;
use App\Repositories\TutorClassLessonRepository;
use Illuminate\Http\Request;

class TutorClassController extends ApiController
{
    /**
     * @param TutorClassRepository $tutorClassRepository
     */
    protected $tutorClassLessonRepository;

    public function __construct(TutorClassRepository $tutorClassRepository, TutorClassLessonRepository $tutorClassLessonRepository)
    {
        $this->setRepository($tutorClassRepository);
        $this->tutorClassLessonRepository = $tutorClassLessonRepository;
    }

    public function findById($classId)
    {
        try{
            $valid = $this->getRepository()->getValidate()->validateById(['id' => $classId]);
            if($valid->fails()){
                return $this->renderJsonError($valid->errors()->first());
            }
            $class = $this->getRepository()->findById($classId);
            $dataClass = [];
            if(!empty($class)){
                $dataClass = $class->toArray();
                $dataClass['teacher'] = $class->hasOneTeacher;
                $dataClass['schedule'] = convertSchedule($class->hasManySchedule);
            }
            return $this->renderJson($dataClass);

        }catch(\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }
    public function findByClassLesson($classLessonId){
        try{
            $query = [
                'id' => $classLessonId
            ];
            $dataClassLesson = $this->tutorClassLessonRepository->findClassLessonById($query);
            $data = [];
            if(!empty($dataClassLesson)){
                $data = $dataClassLesson->toArray();
                $class = $dataClassLesson->hasOneClass;
                $data['class'] = $class->toArray();
                $data['subject'] = [
                    'name' => $class->hasOneSubject->name
                ];
                $data['teacher'] = [
                    'name' => $class->hasOneTeacher->name,
                    'image' => $class->hasOneTeacher->image,
                ];
                $data['rating'] = $dataClassLesson->hasOneRatingByUser;
                $data['study'] = $dataClassLesson->hasOneSectionVideoStudy;
                //check time study finish
                $timeEnd = formatDateTime($dataClassLesson->lesson_end_time, 'Y-m-d H:i');
                $currentTime = formatDateTime(now(),'Y-m-d H:i');
                $data['video'] = null;
                if($timeEnd < $currentTime){
                    $videos = $dataClassLesson->hasOneSectionTypeVideo;
                    $data['video'] = !empty($dataClassLesson) ? $videos : null;
                }
            }
            return $this->renderJson($data);
        }catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }
}
