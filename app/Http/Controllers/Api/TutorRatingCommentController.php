<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorRatingCommentRepository;
use App\Repositories\TutorClassLessonRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutorRatingCommentController extends ApiController
{
    protected $tutorClassLessonRepository;

    public function __construct(TutorRatingCommentRepository $tutorRatingCommentRepository, TutorClassLessonRepository $tutorClassLessonRepository)
    {
        $this->setRepository($tutorRatingCommentRepository);
        $this->tutorClassLessonRepository = $tutorClassLessonRepository;
    }

    public function store(Request $request)
    {
        $valid = $this->getRepository()->getValidate()->storeValidate($request->all());
        if ($valid->fails()) {
            return $this->renderJsonError($valid->errors()->first());
        }
        try {
            $dataRating = $this->getRepository()->search([
                'class_id' => $request->class_id,
                'lesson_id' => $request->lesson_id,
                'student_id' => apiStudentId(),
            ])->get()->toArray();
            if (!empty($dataRating)) {
                return $this->renderJsonError(getValidation('duplicate'));
            }
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }

        DB::BeginTransaction();
        try {
            $dataRequest = $request->all();
            $dataRequest['student_id'] = apiStudentId();
            $dataCreate = $this->getRepository()->create($dataRequest);
            if(!$dataCreate){
                DB::rollBack();
                return $this->renderJsonError(getMessage('create_error'));
            }
            //update class lesson
            $classLesson = $this->tutorClassLessonRepository->findById($dataRequest['lesson_id']);
            $classLesson->total_rate_point += $dataRequest['score'];
            $classLesson->total_num_of_rate += 1;
            $dataUpdate = $classLesson->save();
            if(!$dataUpdate){
                DB::rollBack();
                return $this->renderJsonError(getMessage('create_error'));
            }
            DB::commit();
            $item = $this->getRepository()->find($dataCreate->getKey());
            return $this->renderJson($item, getMessage('create_success'));
        } catch (\Exception $exception) {
            DB::rollback();
            return $this->renderJsonException($exception);
        }
    }
}
