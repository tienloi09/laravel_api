<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorClassProgramRepository;

class TutorClassProgramController extends ApiController
{
    /**
     * @param TutorClassProgramRepository $tutorClassProgramRepository
     */

    public function __construct(TutorClassProgramRepository $tutorClassProgramRepository)
    {
        $this->setRepository($tutorClassProgramRepository);
    }
}
