<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorProgramRepository;

class TutorProgramController extends ApiController
{
    public function __construct(TutorProgramRepository $tutorProgramRepository)
    {
        $this->setRepository($tutorProgramRepository);
    }
}
