<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorCoreConfigRepository;

class TutorCoreConfigController extends ApiController
{
    public function __construct(TutorCoreConfigRepository $tutorCoreConfigRepository)
    {
        $this->setRepository($tutorCoreConfigRepository);
    }

    public function getIsEnableTutor()
    {
        try {
            $res = $this->getRepository()->getByKey('IS_ENABLE_TUTOR');
            if (!$res) {
                return $this->renderJsonError(getConstant('not_found_config'));
            }

            return $this->renderJson($res);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }
}
