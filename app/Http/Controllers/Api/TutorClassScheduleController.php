<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorClassScheduleRepository;


class TutorClassScheduleController extends ApiController
{
    /**
     * @param TutorClassScheduleRepository $tutorClassScheduleRepository
     */

    public function __construct(TutorClassScheduleRepository $tutorClassScheduleRepository)
    {
        $this->setRepository($tutorClassScheduleRepository);
    }

    public function getListByUser()
    {
        try {
            $data = $this->getRepository()->getListByWeek();
            return $this->renderJson($data);
        }catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }
}
