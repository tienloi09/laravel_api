<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorStudentRepository;
use App\Repositories\TutorClassStudentRepository;
use Illuminate\Http\Request;

class TutorStudentController extends ApiController
{
    /**
     * @param TutorStudentRepository $tutorStudentRepository
     * @param TutorClassStudentRepository $tutorClassStudentRepository
     */
    public function __construct(TutorStudentRepository $tutorStudentRepository, TutorClassStudentRepository $tutorClassStudentRepository)
    {
        $this->setRepository($tutorStudentRepository);
        $this->setMultiRepository([$tutorClassStudentRepository]);
    }

    public function getListByUser()
    {
        try {
            $dataStudent = $this->getRepository()->getListStudentByUser(apiUserId());
            $totalClass = [];
            foreach ($dataStudent->target_grade_ids as $gradeId){
                $totalClass[$gradeId] = $dataStudent->findTotalClassByGrade($gradeId);
            }
            $dataStudent->total_class = $totalClass;

            return $this->renderJson($dataStudent);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function getSubjectByUser(Request $request)
    {
        try {
            $gradeId = !empty($request->get('grade_id')) ? $request->get('grade_id'): null;
            $listSubject = $this->getRepository()->getSubjectByUser();
            foreach ($listSubject as $item){
                $item->logo = loadImageUrl(getConstant('list_mon_hoc_avatar.'.$item->id));
                $lstClass = $item->hasManyClassByStudent($gradeId);
                $item->list_class = $lstClass;
                $item->total_class = $lstClass->count();
            }
            return $this->renderJson( $listSubject);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }
}
