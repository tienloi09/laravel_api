<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorTeacherRepository;

class TutorTeacherController extends ApiController
{
    public function __construct(TutorTeacherRepository $tutorTeacherRepository)
    {
        $this->setRepository($tutorTeacherRepository);
    }
}
?>
