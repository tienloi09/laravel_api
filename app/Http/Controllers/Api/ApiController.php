<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    protected $repository;

    public function getKey($key)
    {
        return isset($key) ? $this->{$key}: $this->id;
    }

    public function setKey($key)
    {
        return isset($key) ? $this->{$key}: $this->id;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function setMultiRepository($repository)
    {
        foreach ($repository as $itemRepo) {
            $this->{class_basename($itemRepo)} = $itemRepo;
        }
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function fetchRepository($repository)
    {
        return $this->{class_basename($repository)};
    }

    public function index()
    {
        $listData = $this->getRepository()->getListData();
        return $this->renderJson($listData);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($id)
    {
        try {
            $itemModel = $this->getRepository()->findById($id);
            if (empty($itemModel)) {
                return $this->renderJsonError(getMessage('not_found_detail'));
            }
            return $this->renderJson($itemModel, getMessage('not_found_data'));
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(Request $request)
    {
        /*
        $valid = $this->getRepository()->validate()->storeValidate($request->all());
        dd($valid);
        if($valid->fails()){
            return $this->renderJsonError($valid);
        }*/
        DB::BeginTransaction();
        try {
            $dataRequest = $request->all();
            $dataCreate = $this->getRepository()->create($dataRequest);
            DB::commit();
            $item = $this->getRepository()->find($dataCreate->getKey());
            return $this->renderJson($item, getMessage('create_success'));
        } catch (\Exception $exception) {
            DB::rollback();
            return $this->renderJsonException($exception);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($id, Request $request)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError( getMessage('not_found_data'), 200);
        }

        DB::beginTransaction();
        try {
            $dataRequest = $request->all();
            $dataRequest['update_user'] = apiUserId();
            $status = $itemModel->fill($dataRequest)->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJsonError(getMessage('update_error'),$dataRequest);
            }
            DB::commit();
            $item = $this->getRepository()->find($id);
            return $this->renderJson($item, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->renderJsonException($exception);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError(getMessage('not_found_data'), [], 200);
        }
        DB::beginTransaction();
        try {
            $itemModel->{getConfig('deleted_name')} = getConfig('deleted_active');
            $status = $itemModel->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJsonError(getMessage('delete_error'));
            }
            DB::commit();
            return $this->renderJson([],getMessage('delete_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->renderJsonException($exception);
        }

    }

    /**
     * @param string or array $message
     * @param array $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */

    public function renderJson($data = [], $message = '', $statusCode = 200)
    {
        return response()->json([
            'statusCode' => $statusCode,
            'success' => true,
            'message' => $message,
            'time_current' => formatDateTime('','d/m/Y H:i:s'),
            'data' => $data
        ]);
    }

    /**
     * @param string or array $message
     * @param array $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */

    public function renderJsonError($message = '', $data = [], $statusCode = 400)
    {
        return response()->json([
            'statusCode' => $statusCode,
            'success' => false,
            'message' => $message,
            'data' => $data
        ]);
    }
    public function renderJsonException($exception){
        logError($exception);
        return $this->renderJsonError($exception->getMessage());
    }
}
