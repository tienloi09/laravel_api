<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorAnnouncementRepository;

class TutorAnnouncementController extends ApiController
{
    public function __construct(TutorAnnouncementRepository $tutorAnnouncementRepository)
    {
        $this->setRepository($tutorAnnouncementRepository);
    }
}
