<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\TutorMissionCompleted;
use App\Repositories\TutorMissionCompletedRepository;
use App\Repositories\TutorClassLessonSectionRepository;
use App\Repositories\TutorClassLessonRepository;
use App\Repositories\TutorClassStudentRepository;
use App\Models\TutorClassLessonSection;
use Illuminate\Http\Request;
use App\Models\LmsHomeWork;
use Carbon\Carbon;

class TutorMissionCompletedController extends ApiController
{
    private $clsRepo = null;
    private $clRepo = null;

    public function __construct(
        TutorMissionCompletedRepository $tutorMissionCompletedRepository, 
        TutorClassLessonSectionRepository $clsRepo,
        TutorClassLessonRepository $clRepo)
    {
        $this->setRepository($tutorMissionCompletedRepository);
        $this->clsRepo = $clsRepo;
        $this->clRepo = $clRepo;
    }

    public function getListByStudent(Request $request)
    {
        try{
            $lstMissionCompleted = $this->getRepository()->getListByStudent($request->all());
            $listData = [];
            foreach ($lstMissionCompleted as $missionCompleted){
                $itemMission = $missionCompleted->toArray();
                $itemMission['class'] = $missionCompleted->hasOneClassMission;
                $itemMission['class_lesson'] = $missionCompleted->hasOneClassLesson;
                array_push($listData,$itemMission);
            }
            return $this->renderJson($listData);
        }catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }

    public function getInprogress(Request $request)
    {
        $now = Carbon::now();
        $studentId = apiStudentId();
        $gradeId = $request->get('grade_id', 1);
        $classId = $request->get('class_id', null);
        $lessonId = $request->get('lesson_id', null);
        $prevAndNextLessonIds = [];
        $ret = [];
        try {
            $listData = [];
            if ($lessonId) {
                $prevAndNextLessonIds = [$lessonId];
            } else {
                if($classId != null) {
                    $prevAndNextLessonIds = $this->clRepo->getPrevNextLessons($studentId, $gradeId, $now, $classId);
                } else {
                    $classIds = app(TutorClassStudentRepository::class)->getClassBySubject();
                    if(count($classIds) > 0) {
                        foreach($classIds as $classId) {
                            $prevAndNextLessonIds = array_merge($prevAndNextLessonIds, $this->clRepo->getPrevNextLessons($studentId, $gradeId, $now, $classId));
                        }
                    }
                }
            }
            
            if(!empty($prevAndNextLessonIds)) {
                $listData = $this->clsRepo->getMissionInRange($studentId, $prevAndNextLessonIds);
            }
            if(!empty($listData)) {
                foreach($listData as $row) {
                    $fileLink = null;
                    $fileKeyLink = null;
                    if ($row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                        $lms = (new LmsHomeWork)->find($row['lms_id']);
                        $fileLink = $lms->file_link ?? null;
                        $fileKeyLink = $lms->file_key_link ?? null;
                    }
                    $ret[] = array_merge($row, [
                        'file_link' => $fileLink,
                        'file_key_link' => $fileKeyLink,
                    ]);
                }
            }
            return $this->renderJson($ret);
        } catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }

    public function getOldMissions(Request $request)
    {
        $studentId = apiStudentId();
        $subjectId = $request->get('subjectId', null);
        $gradeId = $request->get('gradeId', null);
        $ret = [];
        try {
            $listData = $this->clsRepo->getOldMissions($studentId, $subjectId, $gradeId);
            if(!empty($listData)) {
                foreach($listData as $row) {
                    $fileLink = null;
                    $fileKeyLink = null;
                    if ($row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                        $lms = (new LmsHomeWork)->find($row['lms_id']);
                        $fileLink = $lms->file_link ?? null;
                        $fileKeyLink = $lms->file_key_link ?? null;
                    }
                    $ret[] = array_merge($row, [
                        'file_link' => $fileLink,
                        'file_key_link' => $fileKeyLink,
                    ]);
                }
            }
            return $this->renderJson($ret);
        } catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }
}
