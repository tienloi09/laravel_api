<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorClassStudentRepository;
use App\Repositories\TutorClassScheduleRepository;
use App\Repositories\TutorClassLessonRepository;
use App\Repositories\TutorClassLessonSectionRepository;
use App\Repositories\TutorClassRepository;
use App\Models\TutorClassLessonSection;
use App\Models\LmsHomeWork;
use App\Models\TutorProgramLesson;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TutorClassStudentController extends ApiController
{
    private $clsRepo = null;
    protected $classRepository;
    /**
     * @param TutorClassStudentRepository $tutorClassStudentRepository
     */

    public function __construct(
        TutorClassStudentRepository $tutorClassStudentRepository,
        TutorClassScheduleRepository $tutorClassScheduleRepository,
        TutorClassLessonRepository $tutorClassLessonRepository,
        TutorClassLessonSectionRepository $clsRepo,
        TutorClassRepository $tutorClassRepository
        )
    {
        $this->setRepository($tutorClassStudentRepository);
        $this->setMultiRepository([$tutorClassScheduleRepository, $tutorClassLessonRepository]);
        $this->classRepository = $tutorClassRepository;
        $this->clsRepo = $clsRepo;
    }

    public function getListNearDay(Request $request)
    {
        $valid = $this->getRepository()->getValidate()->validateListNearDay($request->all());
        if($valid->fails()){
            return $this->renderJsonError($valid->errors()->first());
        }
        try {
            $query = [
                'target_grade_id' => $request->grade_id
            ];
            if($request->class_id) {
                $query['tutor_class_lesson.class_id'] = $request->class_id;
            }
            
            $listClassLesson = $this->fetchRepository(TutorClassLessonRepository::class)->getListLessonNearDay($query);
            $listData = [];
            foreach ($listClassLesson as $lesson){
                $item = $lesson->toArray();
                $item['class'] = $item['has_one_class'];

                $item['image'] = $item['has_one_section_video_study']
                    ? $item['has_one_section_video_study']['image']
                    : '';

                unset($item['has_one_class']);
                unset($item['has_one_section_video_study']);

                $item['subject'] = $lesson->hasOneClass->hasOneSubject->toArray();
                array_push($listData, $item);
            }
            return $this->renderJson($listData);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function getClassBySubject(Request $request)
    {
        try {
            if(empty($request->get('subject_id'))){
                return $this->renderJsonError(getMessage('not_found_request'));
            }
            
            $listData = $this->getListLesson($request->get('subject_id'), $request->get('grade_id'));
            return $this->renderJson($listData);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function getListClassOfStudent(Request $request)
    {
        try {
            $listClassId = $this->getRepository()->getClassBySubject();
            $listClass = $this->classRepository->getListClassOfStudent($listClassId);
            $data = [];

            foreach ($listClass as $class) {
                $itemClass = $class->toArray();
                $itemClass['lessons'] = [];
                $lessons = $class->classLessons;
                foreach ($lessons as $lesson) {
                    $item = $lesson->toArray();
                    $item['video_online'] = $lesson->hasOneSectionVideoStudy;
                    $itemClass['lessons'][] = $item;
                }

                $now = Carbon::now();
                $studentId = apiStudentId();
                $classId = $class->id;

                $prevAndNextLessonIds = [];
                $ret = [];
                $listData = [];

                $prevAndNextLessonIds = $this->fetchRepository(TutorClassLessonRepository::class)
                    ->getPrevNextLessons($studentId, $class->target_grade_id, $now, $classId);

                if(!empty($prevAndNextLessonIds)) {
                    $listData = $this->clsRepo->getMissionInRange($studentId, $prevAndNextLessonIds);
                }

                $oldLessons = $this->fetchRepository(TutorClassLessonRepository::class)->getOldLessons($studentId, $class->target_grade_id, $now, $classId);

                $itemClass['schedule'] = convertSchedule($class->hasManySchedule);
                $itemClass['missons_of_last_lesson'] = $listData;

                if(!empty($oldLessons)) {
                    $oldLessons = $this->clsRepo->getMissionInRange($studentId, $oldLessons);
                }
                $oldLessonRes = [];
                foreach ($oldLessons as $oldLesson) {
                    if ($oldLesson['type'] == TutorClassLessonSection::TYPE_PRACTICE) {
                        $oldLesson['result'] = $this->clsRepo->getPraceticeResult($oldLesson['lms_id'], $studentId);
                    }

                    $oldLessonRes[] = $oldLesson;
                }
                $itemClass['all_old_missons'] = $oldLessonRes;
                array_push($data, $itemClass);
            }

            return $this->renderJson($data);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    protected function getListLesson($subjectId = null, $gradeId = null)
    {
        $listClassId = $this->getRepository()->getClassBySubject($subjectId, $gradeId);
        $listClass = $this->classRepository->getListClassInfoByIds($listClassId);
        $data = [];
        foreach ($listClass as $class){
            $itemClass = $class->toArray();
            $itemClass['subject'] = $itemClass['has_one_gradle'];
            $itemClass['teacher'] = $itemClass['has_one_teacher'];

            unset($itemClass['has_one_gradle']);
            unset($itemClass['has_one_teacher']);

            $itemClass['schedule'] = convertSchedule($class->hasManySchedule);
            array_push($data,$itemClass);
        }

        return $data;
    }
}
