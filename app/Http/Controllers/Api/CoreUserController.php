<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\CoreUserRepository;

class CoreUserController extends ApiController
{
    public function __construct(CoreUserRepository $coreUserRepository)
    {
        $this->setRepository($coreUserRepository);
    }
}
