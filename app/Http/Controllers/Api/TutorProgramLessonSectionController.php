<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorProgramLessonSectionRepository;
use App\Models\TutorClassLessonSection;


class TutorProgramLessonSectionController extends ApiController
{
    public function __construct(TutorProgramLessonSectionRepository $tutorProgramLessonSectionRepository)
    {
        $this->setRepository($tutorProgramLessonSectionRepository);
    }

    public function findListProgramLesson()
    {
        try {
            $listProgram = $this->getRepository()->findListProgramLesson(apiStudentId());
            $dataLesson = [];
            //get list nhiem vu can lam theo program
            foreach ($listProgram as $program) {
                $dataProgram = $program->toArray();
                $lms = [];
                switch($program->type){
                    case TutorClassLessonSection::TYPE_VIDEO:
                        $lms = $program->hasOneRevise->toArray();
                        break;
                    case TutorClassLessonSection::TYPE_PRACTICE:
                        $lms = $program->hasOnePractice->toArray();
                        break;
                    default:
                        break;
                }
                if(!isset($dataLesson[$dataProgram['lesson_id']])){
                    $dataProgram['lms'] = [];
                    $dataLesson[$dataProgram['lesson_id']] = $dataProgram;
                }
                $lstLesson = $dataLesson[$dataProgram['lesson_id']];
                if(!empty($lms)){
                    $lms['completed'] = $program->hasManyMissionCompleted(apiStudentId());
                    array_push($lstLesson['lms'],$lms);
                }
                $dataLesson[$dataProgram['lesson_id']] = $lstLesson;
            }

            // lay dc list program theo student
            return $this->renderJson(array_values($dataLesson));
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }
}
