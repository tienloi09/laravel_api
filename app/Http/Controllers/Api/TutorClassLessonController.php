<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TutorClassLessonRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\TutorClassLessonSectionRepository;
use App\Models\LmsHomeWork;
use App\Models\TutorClassLessonSection;

class TutorClassLessonController extends ApiController
{
    private $clsRepo;
    
    /**
     * @param TutorClassLessonRepository $tutorClassLessonRepository
     */

    public function __construct(TutorClassLessonRepository $tutorClassLessonRepository, TutorClassLessonSectionRepository $clsRepo)
    {
        $this->setRepository($tutorClassLessonRepository);
        $this->clsRepo = $clsRepo;
    }

    public function getListLessonFinish(Request $request){
        try{
            $classId = isset($request->class_id) ? $request->class_id : null;
            $data = $this->getRepository()->getListLessonFinish($classId);
            return $this->renderJson($data);
        }catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }

    public function getListByClass($classId)
    {
        try {
            $query = [
                'class_id' => $classId,
                'lesson_type' => getConfig('class_lesson_type.active')
            ];
            $listLesson = $this->getRepository()->getListByClass($query);

            return $this->renderJson([
                'data' => $listLesson
            ]);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function getListReferrences($lessonId) {
        try{
            $data = $this->clsRepo->getListReferrences($lessonId);
            $ret = [];
            if(!empty($data)) {
                foreach($data as $row) {
                    $fileLink = null;
                    $fileKeyLink = null;
                    if ($row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                        $lms = (new LmsHomeWork)->find($row['lms_id']);
                        $fileLink = $lms->file_link ?? null;
                        $fileKeyLink = $lms->file_key_link ?? null;
                    }
                    $ret[] = array_merge($row, [
                        'file_link' => $fileLink,
                        'file_key_link' => $fileKeyLink,
                    ]);
                }
            }
            return $this->renderJson($ret);
        }catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }
}
