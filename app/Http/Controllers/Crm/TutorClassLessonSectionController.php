<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorClassLessonSectionRepository;
use App\Repositories\TutorClassLessonRepository;
use App\Repositories\TutorClassProgramRepository;
use App\Repositories\TutorProgramRepository;
use App\Repositories\TutorProgramLessonRepository;
use App\Repositories\TutorProgramLessonSectionRepository;
use App\Repositories\TutorClassRepository;
use Illuminate\Http\Request;
use App\Models\LmsPractice;
use Illuminate\Support\Facades\DB;
use App\Models\LmsLessonVideo;
use App\Models\TutorClassLessonSection;

class TutorClassLessonSectionController extends CrmController
{
    protected $tutorProgramRepository;
    protected $tutorProgramLessonRepository;
    protected $tutorProgramLessonSectionRepository;
    protected $tutorClassProgramRepository;
    protected $tutorClassLessonRepository;
    protected $tutorClassRepository;
    /**
     * @param TutorClassLessonSectionRepository $tutorClassLessonSectionRepository
     */

    public function __construct(
        TutorClassLessonSectionRepository $tutorClassLessonSectionRepository,
        TutorProgramRepository $tutorProgramRepository,
        TutorProgramLessonRepository $tutorProgramLessonRepository,
        TutorProgramLessonSectionRepository $tutorProgramLessonSectionRepository,
        TutorClassLessonRepository $tutorClassLessonRepository,
        TutorClassProgramRepository $tutorClassProgramRepository,
        TutorClassRepository $tutorClassRepository
    )
    {
        $this->setRepository($tutorClassLessonSectionRepository);
        $this->tutorProgramRepository = $tutorProgramRepository;
        $this->tutorProgramLessonRepository = $tutorProgramLessonRepository;
        $this->tutorProgramLessonSectionRepository = $tutorProgramLessonSectionRepository;
        $this->tutorClassProgramRepository = $tutorClassProgramRepository;
        $this->tutorClassLessonRepository = $tutorClassLessonRepository;
        $this->tutorClassRepository = $tutorClassRepository;
    }

    public function getListByClass(Request $request)
    {
        /**
         * 1: lấy lộ trình học của lớp đã có
         *  => lấy thông tin từ bảng class thông qua id class
         *  => sau đó vào thông tin bảng class_lesson
         *  => từ class_lesson thì lấy trong bảng program_class
         *  => có id của program_class thì tìm được thông tin program_class_lesson_section
         * 2: lấy lộ trình học theo khung chương trình
         *
         */
        $valid = $this->tutorClassProgramRepository->getValidate()->searchByClassValid($request->all());
        if($valid->fails()){
            return $this->renderJsonError($valid->errors());
        }
        try {
            $res = $this->tutorClassRepository->getAllProgramAdded($request->class_id);
            return $this->renderJson($res);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }

    }

    public function store(Request $request)
    {
        DB::BeginTransaction();
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->storeValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonErrorMsgOnly($valid->errors());
            }
            $lesson = $this->tutorClassLessonRepository->findById($request->lesson_id);
            $lesson->update(['is_custom' => getConfig('class_lesson_type.active')]);
            $countSectionCurrent = $this->getRepository()->search([ 'lesson_id' => $request->lesson_id ])->count();
            $data['position'] = $countSectionCurrent + 1;
            if ($request->type == TutorClassLessonSection::TYPE_VIDEO && $request->lms_id) {
                $lmsVideoModel = new LmsLessonVideo();
                $lmsVideo = $lmsVideoModel->find($request->lms_id);
                $data['lms_duration'] = $lmsVideo->total_time ?? 0;
            }
            if ($request->type == TutorClassLessonSection::TYPE_PRACTICE && $request->lms_id) {
                $lmsPractice = new LmsPractice();
                $data['lms_num_question'] = $lmsPractice->countLmsPracticeQuestion($request->lms_id);
            }
            

            if ($request->type == TutorClassLessonSection::TYPE_LIVESTREAM) {
                $lesson->lesson_start_time = $request->start_time;
                $lesson->lesson_end_time = $request->end_time;
                $data = [
                    'lesson_start_time' => $request->start_time,
                    'lesson_end_time' => $request->end_time,
                    'class_id' => $lesson->class_id,
                ];
                $checkChangeTime = $this->tutorClassLessonRepository->checkChangeTime($data);
                if ($checkChangeTime) {
                    return $this->renderJsonError([], getMessage('duplicate_schedule'));
                }
                $lesson->save();
            }

            $dataCreate = $this->getRepository()->create(array_merge($request->all(), $data));
            DB::commit();
            $item = $this->getRepository()->find($dataCreate->getKey());

            return $this->renderJson($item, getMessage('create_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function update($id, Request $request)
    {
        $valid = $this->getRepository()
            ->getValidate()
            ->updateValidate($request->all());
        if($valid->fails()){
            return $this->renderJsonErrorMsgOnly($valid->errors());
        }

        $data = [];
        if ($request->type == TutorClassLessonSection::TYPE_VIDEO && $request->lms_id) {
            $lmsVideoModel = new LmsLessonVideo();
            $lmsVideo = $lmsVideoModel->find($request->lms_id);
            $data['lms_duration'] = $lmsVideo->total_time ?? 0;
        }
        if ($request->type == TutorClassLessonSection::TYPE_PRACTICE && $request->lms_id) {
            $lmsPractice = new LmsPractice();
            $data['lms_num_question'] = $lmsPractice->countLmsPracticeQuestion($request->lms_id);
        }

        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }

        DB::beginTransaction();
        try {
            $lesson = $this->tutorClassLessonRepository->findById($request->lesson_id);
            $lesson->update(['is_custom' => getConfig('class_lesson_type.active')]);
            $status = $itemModel->fill(array_merge($request->all(), $data))->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJson($id, getMessage('update_error'));
            }
            if ($request->type == TutorClassLessonSection::TYPE_LIVESTREAM) {
                $lesson->lesson_start_time = $request->start_time;
                $lesson->lesson_end_time = $request->end_time;
                $data = [
                    'lesson_start_time' => $request->start_time,
                    'lesson_end_time' => $request->end_time,
                    'class_id' => $lesson->class_id,
                ];
                $checkChangeTime = $this->tutorClassLessonRepository->checkChangeTime($data);
                if ($checkChangeTime) {
                    return $this->renderJsonError([], getMessage('duplicate_schedule'));
                }
                $lesson->save();
            }
            DB::commit();
            $item = $this->getRepository()->find($id);
            return $this->renderJson($item, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function destroy($id)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }
        DB::beginTransaction();
        try {
            $lesson = $this->tutorClassLessonRepository->findById($itemModel->lesson_id);
            $lesson->update(['is_custom' => getConfig('class_lesson_type.active')]);
            $itemModel->{getConfig('deleted_name')} = getConfig('deleted_active');
            $status = $itemModel->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJsonError([], getMessage('delete_error'));
            }
            DB::commit();
            return $this->renderJson([], getMessage('delete_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function sortPositionByLesson($lessonId, Request $request)
    {
        $ids = explode(',', $request->ids);
        $listLessonSections = $this->getRepository()->getListByLesson($lessonId);
        $listData = [];

        if (count($ids) != count($listLessonSections)) {
            return $this->renderJsonError([], getMessage('update_error'));
        }

        DB::BeginTransaction();
        try {
            foreach ($listLessonSections as $lessonSection) {
                $position_in_ids = array_search($lessonSection->id, $ids);

                if ($position_in_ids === false) {
                    return $this->renderJsonError([], getMessage('update_error'));
                }

                $lessonSection->update([
                    'position' => $position_in_ids + 1,
                ]);
                $listData[] = [
                    'id' => $lessonSection->id,
                    'position' => $position_in_ids + 1,
                ];
            }
            DB::commit();

            return $this->renderJson($listData, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->renderJsonException($exception);
        }
    }
}
