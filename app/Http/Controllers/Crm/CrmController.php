<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrmController extends Controller
{
    protected $repository;

    public function getKey($key)
    {
        return isset($key) ? $this->{$key} : $this->id;
    }

    public function setKey($key)
    {
        return isset($key) ? $this->{$key} : $this->id;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function setMultiRepository($repository)
    {
        foreach ($repository as $itemRepo) {
            $this->{class_basename($itemRepo)} = $itemRepo;
        }
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function fetchRepository($repository)
    {
        return $this->{class_basename($repository)};
    }

    public function index(Request $request)
    {
        $listData = $this->getRepository()->getListData();
        return $this->renderJson($listData);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function show($id)
    {
        try {
            $itemModel = $this->getRepository()->findById($id);
            if (empty($itemModel)) {
                return $this->renderJsonError([], getMessage('not_found_detail'));
            }
            return $this->renderJson($itemModel);
        } catch (\Exception $exception) {
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(Request $request)
    {
        DB::BeginTransaction();
        try {
            $dataCreate = $this->getRepository()->create($request->all());
            DB::commit();
            $item = $this->getRepository()->find($dataCreate->getKey());
            return $this->renderJson($item, getMessage('create_success'));
        } catch (\Exception $exception) {
            DB::rollback();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function update($id, Request $request)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }

        DB::beginTransaction();
        try {
            $status = $itemModel->fill($request->all())->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJson($id, getMessage('update_error'));
            }
            DB::commit();
            $item = $this->getRepository()->find($id);
            return $this->renderJson($item, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }
        DB::beginTransaction();
        try {
            $itemModel->{getConfig('deleted_name')} = getConfig('deleted_active');
            $status = $itemModel->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJsonError([], getMessage('delete_error'));
            }
            DB::commit();
            return $this->renderJson([], getMessage('delete_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }

    }

    /**
     * @param string or array $message
     * @param array $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */

    public function renderJson($data = [], $message = '', $statusCode = 200)
    {
        $result = [
            'code' => $statusCode,
            'success' => true,
            'message' => $message,
            'data' => $data,
            'time_current' => formatDateTime('','d/m/Y H:i:s'),
        ];

        return response()->json($result);
    }

    public function renderJsonToPage($data = [], $total = 0, $message = '', $statusCode = 200)
    {
        $result = [
            'code' => $statusCode,
            'success' => true,
            'message' => $message,
            'time_current' => formatDateTime('','d/m/Y H:i:s'),
            'data' => array('data' =>$data)
        ];
        if ($total > 0) {
            $result['data']['totalItem'] = $total;
        }

        return response()->json($result);
    }

    /**
     * @param string or array $message
     * @param array $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */

    public function renderJsonError($data = [], $message = '', $statusCode = 400)
    {
        return response()->json([
            'code' => $statusCode,
            'success' => false,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function renderJsonException($exception){
        logError($exception);
        return $this->renderJsonError([], $exception->getMessage());
    }

    /**
     * @param string or array $message
     * @param array $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */

    public function renderJsonErrorMsgOnly($data = [], $message = '', $statusCode = 400)
    {
        if(!empty($data) && $data instanceof \Illuminate\Support\MessageBag) {
            foreach($data->getMessages() as $key => $erros) {
                if(is_string($erros)) {
                    $message .= $erros;
                }
                if(is_array($erros)) {
                    foreach($erros as $e) {
                        $message .= $e;
                    }
                }
            }
        }
        
        return response()->json([
            'code' => $statusCode,
            'success' => false,
            'message' => $message,
            'data' => $data
        ]);
    }
}
