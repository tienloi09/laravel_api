<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorProgramLessonRepository;
use App\Repositories\TutorClassLessonRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TutorProgramLessonController extends CrmController
{
    protected $tutorClassLessonRepository;

    public function __construct(
        TutorProgramLessonRepository $tutorProgramLessonRepository,
        TutorClassLessonRepository $tutorClassLessonRepository
        )
    {
        $this->setRepository($tutorProgramLessonRepository);
        $this->tutorClassLessonRepository = $tutorClassLessonRepository;
    }

    public function store(Request $request)
    {
        DB::BeginTransaction();
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->storeValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }

            $countLessonCurrent = $this->getRepository()->search([ 'program_id' => $request->program_id ])->count();

            $dataCreate = $this->getRepository()->create(array_merge($request->all(), [ 'position' => $countLessonCurrent + 1 ]));
            $this->tutorClassLessonRepository->updateWhenEditProgram($request->program_id);
            DB::commit();
            $item = $this->getRepository()->find($dataCreate->getKey());

            return $this->renderJson($item, getMessage('create_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function update($id, Request $request)
    {
        $valid = $this->getRepository()
        ->getValidate()
        ->updateValidate($request->all());
        if($valid->fails()){
            return $this->renderJsonError($valid->errors());
        }

        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }

        DB::beginTransaction();
        try {
            $status = $itemModel->fill($request->all())->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJson($id, getMessage('update_error'));
            }
            $this->tutorClassLessonRepository->updateWhenEditProgram($itemModel->program_id);
            DB::commit();

            $item = $this->getRepository()->find($id);
            return $this->renderJson($item, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function destroy($id)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }
        DB::beginTransaction();
        try {
            $itemModel->{getConfig('deleted_name')} = getConfig('deleted_active');
            $status = $itemModel->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJsonError([], getMessage('delete_error'));
            }
            DB::commit();

            $this->tutorClassLessonRepository->updateWhenEditProgram($itemModel->program_id);

            return $this->renderJson([], getMessage('delete_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function getListByProgram($programId)
    {
        try {
            $listProgramLessons = $this->getRepository()->getListByProgram($programId);

            return $this->renderJson($listProgramLessons);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function sortPositionByProgram($programId, Request $request)
    {
        $ids = explode(',', $request->ids);
        $listProgramLessons = $this->getRepository()->getListByProgram($programId);
        $listData = [];

        if (count($ids) != count($listProgramLessons)) {
            return $this->renderJsonError([], getMessage('update_error'));
        }

        DB::BeginTransaction();
        try {
            foreach ($listProgramLessons as $programLesson) {
                $position_in_ids = array_search($programLesson->id, $ids);

                if ($position_in_ids === false) {
                    return $this->renderJsonError([], getMessage('update_error'));
                }

                $programLesson->update([
                    'position' => $position_in_ids + 1,
                ]);
                $listData[] = [
                    'id' => $programLesson->id,
                    'position' => $position_in_ids + 1,
                ];
            }
            DB::commit();

            return $this->renderJson($listData, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->renderJsonException($exception);
        }
    }
}
