<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorProgramLessonSectionRepository;
use App\Repositories\TutorClassLessonRepository;
use App\Models\LmsPractice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\TutorProgramLessonSection;
use App\Models\LmsLessonVideo;

class TutorProgramLessonSectionController extends CrmController
{
    protected $tutorClassLessonRepository;

    public function __construct(
        TutorProgramLessonSectionRepository $tutorProgramLessonSectionRepository,
        TutorClassLessonRepository $tutorClassLessonRepository
        )
    {
        $this->setRepository($tutorProgramLessonSectionRepository);
        $this->tutorClassLessonRepository = $tutorClassLessonRepository;
    }

    public function store(Request $request)
    {
        DB::BeginTransaction();
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->storeValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonErrorMsgOnly($valid->errors());
            }

            $data = [];
            $countSectionCurrent = $this->getRepository()->search([ 'lesson_id' => $request->lesson_id ])->count();
            $data['position'] = $countSectionCurrent + 1;
            if ($request->type == TutorProgramLessonSection::TYPE_VIDEO && $request->lms_id) {
                $lmsVideoModel = new LmsLessonVideo();
                $lmsVideo = $lmsVideoModel->find($request->lms_id);
                $data['lms_duration'] = $lmsVideo->total_time ?? 0;
            }
            if ($request->type == TutorProgramLessonSection::TYPE_PRACTICE && $request->lms_id) {
                $lmsPractice = new LmsPractice();
                $data['lms_num_question'] = $lmsPractice->countLmsPracticeQuestion($request->lms_id);
            }

            $dataCreate = $this->getRepository()->create(array_merge($request->all(), $data));
            $this->tutorClassLessonRepository->updateWhenEditProgram($request->program_id);
            DB::commit();
            $item = $this->getRepository()->find($dataCreate->getKey());

            return $this->renderJson($item, getMessage('create_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function update($id, Request $request)
    {
        $valid = $this->getRepository()
            ->getValidate()
            ->updateValidate($request->all());
        if($valid->fails()){
            return $this->renderJsonErrorMsgOnly($valid->errors());
        }

        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }

        DB::beginTransaction();
        try {
            $data = [];
            if ($request->type == TutorProgramLessonSection::TYPE_VIDEO && $request->lms_id) {
                $lmsVideoModel = new LmsLessonVideo();
                $lmsVideo = $lmsVideoModel->find($request->lms_id);
                $data['lms_duration'] = $lmsVideo->total_time ?? 0;
            }
            if ($request->type == TutorProgramLessonSection::TYPE_PRACTICE && $request->lms_id) {
                $lmsPractice = new LmsPractice();
                $data['lms_num_question'] = $lmsPractice->countLmsPracticeQuestion($request->lms_id);
            }
            
            $status = $itemModel->fill(array_merge($request->all(), $data))->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJson($id, getMessage('update_error'));
            }
            $this->tutorClassLessonRepository->updateWhenEditProgram($itemModel->program_id);
            DB::commit();

            $item = $this->getRepository()->find($id);
            return $this->renderJson($item, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function destroy($id)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }
        DB::beginTransaction();
        try {
            $itemModel->{getConfig('deleted_name')} = getConfig('deleted_active');
            $status = $itemModel->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJsonError([], getMessage('delete_error'));
            }
            $this->tutorClassLessonRepository->updateWhenEditProgram($itemModel->program_id);
            DB::commit();

            return $this->renderJson([], getMessage('delete_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function sortPositionByProgramLesson($programLessonId, Request $request)
    {
        $ids = explode(',', $request->ids);
        $listProgramLessonSections = $this->getRepository()->getListByProgramLesson($programLessonId);
        $listData = [];
        
        if (count($ids) != count($listProgramLessonSections)) {
            return $this->renderJsonError([], getMessage('update_error'));
        }

        DB::BeginTransaction();
        try {
            foreach ($listProgramLessonSections as $programLessonSection) {
                $position_in_ids = array_search($programLessonSection->id, $ids);

                if ($position_in_ids === false) {
                    return $this->renderJsonError([], getMessage('update_error'));
                }

                $programLessonSection->update([
                    'position' => $position_in_ids + 1,
                ]);
                $listData[] = [
                    'id' => $programLessonSection->id,
                    'position' => $position_in_ids + 1,
                ];
            }
            DB::commit();

            return $this->renderJson($listData, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->renderJsonException($exception);
        }
    }
}
