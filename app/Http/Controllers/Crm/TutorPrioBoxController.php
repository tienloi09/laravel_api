<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorPrioBoxRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TutorPrioBoxController extends CrmController
{
    public function __construct(TutorPrioBoxRepository $TutorPrioBoxRepository){
        $this->setRepository($TutorPrioBoxRepository);
    }

    public function index(Request $request)
    {
        $accept_params = [
            'status',
            'start_date',
            'end_date',
            'page',
            'limit',
            'search_name'
        ];
        $res = $this->getRepository()->getList($request->only($accept_params));

        return $this->renderJsonToPage($res['data'], $res['total']);
    }

    public function show($id)
    {
        try {
            $data = $this->getRepository()->getById($id);
            return $this->renderJson($data);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function store(Request $request)
    {
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->storeValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            //check user used
            $getUser = $this->getRepository()->getListUserSendNoti($request);
            if ($getUser == "") {
                return $this->renderJsonError(["user_used" => ["Không tìm thấy đối tượng hiển thị phù hợp"]]);
            }
            $currentTime = date('Y/m/d H:i:s', time());
            $startDate = Carbon::parse($request['start_time'])->format('Y/m/d H:i:s');
            if ($startDate <= $currentTime) {
                return $this->renderJsonError(["start_time" => ["Thời gian bắt đầu phải lớn hơn thời gian tạo thông báo"]]);
            }
            $data = $this->getRepository()->create($request);
            if ($data) {
                return $this->renderJson([], 'Tạo thông báo thành công.');
            } else {
                return $this->renderJsonError([], 'Tạo thông báo thất bại.');
            }
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function update($id, Request $request)
    {
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->updateValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $getDataById = $this->getRepository()->getById($id);
            if (empty($getDataById)) {
                return $this->renderJsonError([], 'Không tồn tại thông báo ' . $id);
            }
            $paramRequest = $request->all();
            if ($paramRequest['target_grade_id_used'] == null 
                && $paramRequest['target_subject_id_used'] == null
                && $paramRequest['target_class_id_used'] == null
                && $paramRequest['level'] == null
                && $paramRequest['user_id_used'] == null) {
                    return $this->renderJsonError([], 'Vui lòng chọn đối tượng áp dụng');
                }
            $update = $this->getRepository()->update($id, $request);
            return $this->renderJson([], 'Update thông báo thành công.');
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function getNotifyByUser($userId)
    {
        if (!empty($userId)) {
            $notify = $this->getRepository()->getNotifyByUser($userId);
            if (count($notify)) {
                return $this->renderJson($notify);
            } else {
                return $this->renderJsonError([], 'Không có thông báo');
            }
        } else {
            return $this->renderJsonError([], 'Không tồn tại giá trị userId');
        }
    }
}
