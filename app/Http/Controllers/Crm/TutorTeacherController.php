<?php
namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorTeacherRepository;
use Illuminate\Http\Request;
use App\Services\BaseLogger;
use App\Models\CoreUser;
use Carbon\Carbon;

class TutorTeacherController extends CrmController
{
    protected $baseLogger;
    public function __construct(TutorTeacherRepository $tutorTeacherRepository, BaseLogger $baseLogger)
    {
        $this->setRepository($tutorTeacherRepository);
        $this->baseLogger = $baseLogger;
    }

    public function index(Request $request)
    {
        try {
            $dataTeacher = $this->getRepository()->getListTeacher($request);
            return $this->renderJsonToPage($dataTeacher['data'], $dataTeacher['total']);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function show($teacherId)
    {
        try {
            $data = $this->getRepository()->getById($teacherId);
            $data['logs'] = [];
            return $this->renderJson($data);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function store(Request $request)
    {
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->storeValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $getDataByMobile = $this->getRepository()->getTeacherByPhone($request['phone']);
            if (isset($getDataByMobile) && count($getDataByMobile) > 0) {
                return $this->renderJsonError(['phone' => ['Trùng số điện thoại trên hệ thống']]);
            }
            $data = $this->getRepository()->create($request);
            return $this->renderJson([], 'Tạo giáo viên thành công.');
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function update($teacherId, Request $request)
    {
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->updateValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $getDataById = $this->getRepository()->getById($teacherId);
            if (empty($getDataById)) {
                return $this->renderJsonError([], 'Không tồn tại giảng viên ' . $teacherId, 200);
            }
            $getDataByMobile = $this->getRepository()->getTeacherByPhone($request['phone']);
            if ($getDataByMobile) {
                for ($i = 0; $i < count($getDataByMobile); $i++) {
                    if ($getDataByMobile[$i]['id'] !== (int) $teacherId) {
                        return $this->renderJsonError(['phone' => ['Trùng số điện thoại trên hệ thống']]);
                    }
                }
            }
            $update = $this->getRepository()->update($teacherId, $request);
            // ghi log
            $author = apiStudentId();
            $log = [
                    'teacher_id' => (int) $teacherId,
                    'action' => 'update',
                    'type' => 1,
                    'create_by' => $author,
                    'content' => 'đã cập nhật thông tin giáo viên'
            ];
            $this->baseLogger->writeLogTeacher($log);
            return $this->renderJson([], 'Update thông tin giáo viên thành công.');
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function search(Request $request)
    {
        try {
            return $this->renderJson($this->getRepository()->getListByOrQuery($request->only(['name'])));
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function getListLogs($teacherId, Request $request)
    {
        try {
            $page = $request['page'] ?? 1;
            $limit = $request['limit'] ?? 6;
            $data = $this->baseLogger->readLogTeacher((int) $teacherId, (int) $page, (int) $limit);
            for($i = 0; $i < count($data['data']); $i ++) {
                $createBy = $data['data'][$i]['create_by'];
                if ($createBy == 0) {
                    $data['data'][$i]['create_user'] = 'Hệ thống';
                } else {
                    $author = CoreUser::find($createBy);
                    $data['data'][$i]['create_user'] = $author['full_name'];
                    $data['data'][$i]['created_at'] = Carbon::parse($data['data'][$i]['created_at'])->format('H:i d/m/Y');
                }
            }
            return $this->renderJsonToPage($data['data'], $data['totalItem']);
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }
}
?>
