<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorStudentRepository;
use App\Repositories\TutorClassLessonRepository;
use App\Services\PixelService;
use Illuminate\Http\Request;
use App\Models\CoreUser;
use App\Services\BaseLogger;
use Carbon\Carbon;
use App\Models\LmsHomeWork;
use App\Models\TutorClassLessonSection;
use App\Repositories\TutorClassStudentRepository;

class TutorStudentController extends CrmController
{
    /**
     * @param TutorStudentRepository $tutorStudentRepository
     */
    protected $baseLogger;
    protected $pixelService;
    protected $tutorClassLessonRepository;

    public function __construct(
        TutorStudentRepository $tutorStudentRepository,
        BaseLogger $baseLogger,
        PixelService $pixelService,
        TutorClassLessonRepository $tutorClassLessonRepository
    ){
        $this->setRepository($tutorStudentRepository);
        $this->baseLogger = $baseLogger;
        $this->pixelService = $pixelService;
        $this->tutorClassLessonRepository = $tutorClassLessonRepository;
    }

    public function index(Request $request)
    {
        try {
            $dataStudent = $this->getRepository()->getListStudent($request);
            return $this->renderJsonToPage($dataStudent['data'], $dataStudent['total']);
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function searchForAddToClass(Request $request)
    {
        try {
            return $this->renderJson($this->getRepository()->searchForAddToClass($request->only(['name'])));
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function show($studentId)
    {
        try {
            $data = $this->getRepository()->getById($studentId);
            $data['logs'] = [];
            return $this->renderJson($data);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function update($studentId, Request $request)
    {
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->updateValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $item = $this->getRepository()->getById($studentId);
            if (empty($item)) {
                return $this->renderJsonError([], getMessage('not_found_data'), 200);
            }
            $update = $this->getRepository()->update($studentId, $request);
            // ghi log
            $author = apiStudentId();
            $log = [
                    'student_id' => (int) $studentId,
                    'action' => 'update',
                    'type' => 1,
                    'create_by' => $author,
                    'content' => 'đã cập nhật thông tin học viên'
            ];
            $this->baseLogger->writeLogStudent($log);
            return $this->renderJson([], 'Cập nhật thông tin học viên thành công.');
            
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function store(Request $request)
    {
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->storeValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $userLms = CoreUser::find($request['lms_core_user_id']);
            if (empty($userLms)) {
                return $this->renderJsonError([], 'Chưa có tài khoản trên LMS. Vui lòng kiểm tra lại.', 404);
            }
            //insert queue sqs
            $object = [
                'TopicArn' => "insert_sqs_queue_add_student_tutor",
                'Message' => "insert_sqs_queue_add_student_tutor",
                'lms_core_user_id' => $request['lms_core_user_id'],
                'level' => 1,
                'target_grade_ids' => $request['target_grade_ids'],
                'target_subject_ids' => $request['target_subject_ids']
            ];
            \Queue::connection('sqs')->pushRaw(json_encode($object), env('AWS_SQS_QUEUE_NAME'));

            return $this->renderJson([], 'Tạo thông tin học viên thành công.');

        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function setLessonForStudent(Request $request)
    {
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->setLessonValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $data = $this->getRepository()->setLessonForStudent($request->all());
            if ($data) {
                return $this->renderJson([], 'Cập nhật số buổi học thành công.');
            } else {
                return $this->renderJsonError([], 'Cập nhật số buổi học thất bại');
            }
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function getListLogs($studentId, Request $request)
    {
        try {
            $page = $request['page'] ?? 1;
            $limit = $request['limit'] ?? 6;
            $data = $this->baseLogger->readLogStudent((int) $studentId, (int) $page, (int) $limit);
            for($i = 0; $i < count($data['data']); $i ++) {
                $createBy = $data['data'][$i]['create_by'];
                if ($createBy == 0) {
                    $data['data'][$i]['create_user'] = 'Hệ thống';
                } else {
                    $author = CoreUser::find($createBy);
                    $data['data'][$i]['create_user'] = $author['full_name'];
                    $data['data'][$i]['created_at'] = Carbon::parse($data['data'][$i]['created_at'])->format('H:i d/m/Y');
                }
            }

            return $this->renderJsonToPage($data['data'], $data['totalItem']);
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function getPixelLogs($studentId, Request $request)
    {
        try {
            $student = $this->getRepository()->findById($studentId);
            $data = $this->pixelService->getList([ 'uid' => $student->lms_core_user_id . '' ]);
            $lessonIds = [];
            $lmsIds = [];
            $classIds = [];
            $videoIds = [];

            foreach ($data['data'] as $item) {
                $lessonIds[] = $item['lesson_id'] ?? null;
                $lmsIds[] = $item['lms_id'] ?? null;
                $classIds[] = $item['class_id'] ?? null;
                $videoIds[] = $item['video_id'] ?? null;
            };
            $lessonIds = array_unique($lessonIds);
            $lmsIds = array_unique($lmsIds);
            $classIds = array_unique($classIds);
            $videoIds = array_unique($videoIds);

            $lessons = $this->tutorClassLessonRepository
                ->search()
                ->with('class:id,title,target_grade_id,target_subject_id,level,status,teacher_id,logo,logo_thumb')
                ->whereIn('id', $lessonIds)
                ->get()
                ->mapToGroups(function ($item, $key) {
                    return [$item['id'] => [
                        'id' => $item['id'],
                        'class_id' => $item['class_id'],
                        'title' => $item['title'],
                        'desc' => $item['desc'],
                        'class_title' => $item['class']['title'],
                    ]];
                })->toArray();

            $res = [];

            foreach ($data['data'] as $log) {
                $item = [
                    'mission_type' => $log['mission_type'],
                    'uid' => $log['uid'],
                    'lms_id' => $log['lms_id'] ?? null,
                    'student_id' => $studentId,
                ];
                if (isset($log['lesson_id']) && isset($lessons[$log['lesson_id']])) {
                    $item = array_merge($item, $lessons[$log['lesson_id']][0]);
                }

                $res[] = $item;
            }

            return $this->renderJsonToPage($res, $data['totalItem']);
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function getInprogress($studentId, Request $request)
    {
        $now = Carbon::now();
        $gradeId = $request->get('grade_id', 1);
        $classId = $request->get('class_id', null);
        $lessonId = $request->get('lesson_id', null);
        $prevAndNextLessonIds = [];
        $ret = [];
        try {
            $listData = [];
            if ($lessonId) {
                $prevAndNextLessonIds = [$lessonId];
            } else {
                if($classId != null) {
                    $prevAndNextLessonIds = $this->clRepo->getPrevNextLessons($studentId, $gradeId, $now, $classId);
                } else {
                    $classIds = app(TutorClassStudentRepository::class)->getClassBySubject();
                    if(count($classIds) > 0) {
                        foreach($classIds as $classId) {
                            $prevAndNextLessonIds = array_merge($prevAndNextLessonIds, $this->clRepo->getPrevNextLessons($studentId, $gradeId, $now, $classId));
                        }
                    }
                }
            }

            if(!empty($prevAndNextLessonIds)) {
                $listData = $this->clsRepo->getMissionInRange($studentId, $prevAndNextLessonIds);
            }
            if(!empty($listData)) {
                foreach($listData as $row) {
                    $fileLink = null;
                    $fileKeyLink = null;
                    if ($row['type'] == TutorClassLessonSection::TYPE_DOCUMENT) {
                        $lms = (new LmsHomeWork)->find($row['lms_id']);
                        $fileLink = $lms->file_link ?? null;
                        $fileKeyLink = $lms->file_key_link ?? null;
                    }
                    $ret[] = array_merge($row, [
                        'file_link' => $fileLink,
                        'file_key_link' => $fileKeyLink,
                    ]);
                }
            }
            return $this->renderJson($ret);
        } catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }
}
