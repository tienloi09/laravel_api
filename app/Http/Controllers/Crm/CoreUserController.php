<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\CoreUserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CoreUserController extends CrmController
{
    public function __construct(CoreUserRepository $coreUserRepository)
    {
        $this->setRepository($coreUserRepository);
    }

    public function uploadFileS3(Request $request)
    {
        try {
            $file = $request->file('file');
            if(empty($file)){
                return $this->renderJsonError([], 'Not found file upload');
            }
            $linkImage = env('CDN_LINK_IMAGE', 'upload') . formatDateTime('', 'Y/m/d') . '/' . substr(md5(time()), 0, 4) . '_' . $file->getClientOriginalName();
            Storage::disk()->getDriver()->getAdapter()->getClient()->putObject([
                'Bucket' => env('AWS_BUCKET'),
                'Key' => $linkImage,
                'Body' => $file->getContent(),
                'ContentType' => $file->getClientMimeType()
            ]);
            $linkImage = env('AWS_URL') . $linkImage;
            return $this->renderJson([
                'path' => $linkImage
            ], getMessage('create_success'));
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }
}
