<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Models\TutorMissionCompleted;
use App\Repositories\TutorMissionCompletedRepository;

class TutorMissionCompletedController extends CrmController
{
    public function __construct(TutorMissionCompletedRepository $tutorMissionCompletedRepository)
    {
        $this->setRepository($tutorMissionCompletedRepository);
    }
}
