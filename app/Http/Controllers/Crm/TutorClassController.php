<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Models\TutorClass;
use App\Models\TutorProgram;
use App\Repositories\TutorClassRepository;
use App\Repositories\TutorTeacherRepository;
use App\Repositories\TutorProgramRepository;
use App\Repositories\TutorClassProgramRepository;
use App\Repositories\CocDmMonHocRepository;
use App\Repositories\CocDmKhoiRepository;
use App\Repositories\TutorClassLessonRepository;
use App\Repositories\TutorClassScheduleRepository;
use App\Repositories\TutorClassStudentRepository;
use App\Repositories\TutorStudentRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutorClassController extends CrmController
{
    protected $teacherRepo;
    protected $programRepo;
    protected $monHocRepo;
    protected $khoiRepo;
    protected $tutorClassProgramRepository;
    protected $tutorClassLessonRepository;
    protected $tutorClassScheduleRepository;
    protected $tutorClassStudentRepository;
    protected $tutorStudentRepository;
    /**
     * @param TutorClassRepository $tutorClassRepository
     */

    public function __construct(
        TutorClassRepository $tutorClassRepository,
        TutorTeacherRepository $tutorTeacherRepository,
        TutorProgramRepository $programRepository,
        CocDmMonHocRepository $cocDmMonHocRepository,
        TutorClassProgramRepository $tutorClassProgramRepository,
        TutorClassLessonRepository $tutorClassLessonRepository,
        TutorClassScheduleRepository $tutorClassScheduleRepository,
        TutorClassStudentRepository $tutorClassStudentRepository,
        TutorStudentRepository $tutorStudentRepository,
        CocDmKhoiRepository $cocDmKhoiRepository
        )
    {
        $this->setRepository($tutorClassRepository);
        $this->teacherRepo = $tutorTeacherRepository;
        $this->monHocRepo = $cocDmMonHocRepository;
        $this->khoiRepo = $cocDmKhoiRepository;
        $this->programRepo = $programRepository;
        $this->tutorClassLessonRepository = $tutorClassLessonRepository;
        $this->tutorClassProgramRepository = $tutorClassProgramRepository;
        $this->tutorClassScheduleRepository = $tutorClassScheduleRepository;
        $this->tutorClassStudentRepository = $tutorClassStudentRepository;
        $this->tutorStudentRepository = $tutorStudentRepository;
    }

    public function getListFilter(Request $request)
    {
        try{
            //get subject
            $listSubject = $this->monHocRepo->getListForFilter();
            //get gradle
            $listGrade = $this->khoiRepo->getListForFilter();
            // list trạng thái
            $listStatus = array_values(getConfig('tutor_class.status'));
            $listLevel = array_values(getConfig('program_levels'));
            // list khung chương trình
            $listPrograms = $this->programRepo->getListData();
            // list giảng viên
            $listTeacher = $this->teacherRepo->getListData();
            //list level teacher
            $levelTeacher = array_values(getConfig('tutor_teacher.level'));
            //list status in class teacher
            $listStatusInClassTeacher = array_values(getConfig('tutor_teacher.status_in_class'));
            // list status student
            $listStatusStudent = array_values(getConfig('tutor_student.status_student'));
            // list status teacher
            $listStatusTeacher = array_values(getConfig('tutor_teacher.status'));
            //listClassByStudent
            $listClassTutor = $this->getRepository()->getListClassStudent();

            return $this->renderJson(compact('listSubject','listGrade','listTeacher','listStatus','listPrograms','listLevel', 'levelTeacher', 'listStatusInClassTeacher', 'listStatusStudent', 'listStatusTeacher', 'listClassTutor'));
        }catch (\Exception $exception){
            return $this->renderJsonException($exception);
        }
    }

    public function index(Request $request)
    {
        try{
            $params = $request->all();
            $res = $this->getRepository()->getListClass($params);

            return $this->renderJsonToPage($res['data'], $res['total']);
        } catch (\Exception $exception) {
            return $this->renderJsonException($exception);
        }
    }

    public function show($id)
    {
        try {
            $itemModel = $this->getRepository()->findById($id);
            if (empty($itemModel)) {
                return $this->renderJsonError([], getMessage('not_found_detail'));
            }
            $res = $itemModel->toArray();
            $res['student_ids'] = array_unique($this->getRepository()->getStudents($itemModel->id)->toArray());
            $res['teacher'] = $itemModel->hasOneTeacher;
            $res['list_student'] = $this->tutorStudentRepository->getStudentsInfo($res['student_ids']);
            $res['schedule'] = convertSchedule($itemModel->hasManySchedule);

            return $this->renderJson($res);
        } catch (\Exception $exception) {
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function addProgramToClass($classId, Request $request)
    {
        try {
            $valid = $this->getRepository()->getValidate()->addProgramToClassValid($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $res = $this->tutorClassProgramRepository->getByClassAndProgram($classId, $request->program_id);
            if ($res) {
                return $this->renderJsonError([], getMessage('program_was_added'));
            }
            $res = $this->getRepository()->addProgramToClass($classId, $request->program_id, $request->start_lesson_id);

            if (!$res) {
                return $this->renderJsonError([], getMessage('class_has_been_finished'));
            }

            if (!isset($res->id)) {
                return $this->renderJsonException($res);
            }

            $listStudent = array_unique($this->getRepository()->getStudents($classId)->toArray());
            $listClassStudent = [];
            foreach ($listStudent as $studentId){
                array_push($listClassStudent,[
                    'class_id' => $classId,
                    'student_id' => $studentId,
                    'program_id' => $request->program_id,
                    'total_num_join_lesson' => 0,
                    'create_date' => formatDateTime('','Y-m-d H:i:s')
                ]);
            }
            if(!empty($listClassStudent)) $this->tutorClassStudentRepository->insert($listClassStudent);

            return $this->renderJson($res);
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function getProgramWillBeAdd(Request $request)
    {
        try{
            $valid = $this->getRepository()->getValidate()->searchByClassProgramValid($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $res = $this->tutorClassProgramRepository->getByClassAndProgram($request->class_id, $request->program_id);
            if ($res) {
                return $this->renderJsonError([], getMessage('program_was_added'));
            }
            $program = $this->programRepo->getModel()->with('programLessons')->find($request->program_id);

            return $this->renderJson($program);
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function store(Request $request)
    {
        $valid = $this->getRepository()->getValidate()->storeValidate($request->all());
        if ($valid->fails()) {
            return $this->renderJsonError($valid->errors());
        }
        DB::beginTransaction();
        try {
            $paramRequest = $request->all();
            //convert time check
            $startDate = formatDateTime(isset($paramRequest['start_date']) ? $paramRequest['start_date'] : '', 'Y-m-d');
            $endDate = formatDateTime(isset($paramRequest['end_date']) ? $paramRequest['end_date'] : '', 'Y-m-d');
            if(!isset($paramRequest['end_date'])){
                $dateEnd = Carbon::parse($startDate)->addDays(7);
                $endDate = formatDateTime($dateEnd, 'Y-m-d');
            }

            $validateTeacher = $this->getRepository()->checkTimeInFuture([$paramRequest['teacher_id']], 'teacher', $paramRequest);
            if ($validateTeacher) {
                return $this->renderJsonError([], getMessage('time_study_teacher_error'));
            }

            $validateStudents = $this->getRepository()->checkTimeInFuture($paramRequest['student_ids'], 'student', $paramRequest);
            if ($validateStudents) {
                return $this->renderJsonError([], getMessage('time_study_student_error'));
            }

            $dataCreate = [
                'title' => $paramRequest['title'],
                'target_grade_id' => $paramRequest['target_grade_id'],
                'target_subject_id' => $paramRequest['target_subject_id'],
                'level' => $paramRequest['level'],
                'status' => $paramRequest['status'],
                'slot' => $paramRequest['slot'],
                'logo' => $paramRequest['logo'],
                'logo_thumb' => $paramRequest['logo_thumb'],
                'start_date' => $startDate,
                'end_date' => $endDate,
                'create_date' => formatDateTime('','Y-m-d H:i:s'),
                'num_of_lesson' => isset($paramRequest['num_of_lesson']) ? $paramRequest['num_of_lesson'] : 0,
                'num_of_lesson_passed' => isset($paramRequest['num_of_lesson_passed']) ? $paramRequest['num_of_lesson_passed'] : 0
            ];
            if(isset($paramRequest['teacher_id'])) $dataCreate['teacher_id'] = $paramRequest['teacher_id'];
            $dataClass = $this->getRepository()->create($dataCreate);
            if (!$dataClass) {
                DB::rollBack();
                return $this->renderJsonError([],getMessage('create_error'));
            }
            $listSchedule = [];
            foreach ($paramRequest['day_of_weeks'] as $dayOfWeek) {
                array_push($listSchedule, [
                    'class_id' => $dataClass->id,
                    'day_of_week' => $dayOfWeek['day_of_week'],
                    'start_time' => $startDate . ' ' . $dayOfWeek['start_time'],
                    'end_time' => $endDate . ' ' . $dayOfWeek['end_time'],
                    'create_date' => formatDateTime('','Y-m-d H:i:s')
                ]);
            }
            //insert class schedule
            $dataSchedule = $this->tutorClassScheduleRepository->insert($listSchedule);
            if (!$dataSchedule) {
                DB::rollBack();
                return $this->renderJsonError([],getMessage('create_error'));
            }
            //insert class student
            if (isset($paramRequest['student_ids'])) {
                $students = $this->tutorStudentRepository->search()->whereIn('id', $paramRequest['student_ids'])->get()->keyBy('id');
                $listClassStudent = [];
                foreach ($paramRequest['student_ids'] as $studentId) {
                    array_push($listClassStudent,[
                        'class_id' => $dataClass->id,
                        'student_id' => $studentId,
                        'total_num_join_lesson' => 0,
                        'create_date' => formatDateTime('','Y-m-d H:i:s')
                    ]);
                    if (isset($students[$studentId])) {
                        $currentTargetGradeIds = $students[$studentId]->target_grade_ids;
                        $currentTargetSubjectIds = $students[$studentId]->target_subject_ids;
                        $targetGradeIds = [];
                        $targetSubjectIds = [];
                        if ($currentTargetGradeIds) {
                            $targetGradeIds = explode(',', $currentTargetGradeIds);
                        }
                        if (!in_array($paramRequest['target_grade_id'], $targetGradeIds)) {
                            $targetGradeIds[] = $paramRequest['target_grade_id'];
                        }
                        $students[$studentId]->target_grade_ids = implode(',', $targetGradeIds);
                        if ($currentTargetSubjectIds) {
                            $targetSubjectIds = explode(',', $currentTargetSubjectIds);
                        }
                        if (!in_array($paramRequest['target_subject_id'], $targetSubjectIds)) {
                            $targetSubjectIds[] = $paramRequest['target_subject_id'];
                        }
                        $students[$studentId]->target_subject_ids = implode(',', $targetSubjectIds);
                        $students[$studentId]->save();
                    }
                }
                if(!empty($listClassStudent)) $this->tutorClassStudentRepository->insert($listClassStudent);
            }
            // check schedule student
            DB::commit();
            $response = $paramRequest;
            $response['id'] = $dataClass->id;
            return $this->renderJson($response, getMessage('create_success'));
        } catch (\Exception $exception) {
            logError($exception);
            DB::rollBack();
            return $this->renderJsonException($exception);
        }
    }

    public function update($id, Request $request)
    {
        $valid = $this->getRepository()->getValidate()->updateValidate($request->all());
        if ($valid->fails()) {
            return $this->renderJsonError($valid->errors());
        }
        DB::beginTransaction();
        try {
            $paramRequest = $request->all();
            //convert time check
            $startDate = formatDateTime(isset($paramRequest['start_date']) ? $paramRequest['start_date'] : '', 'Y-m-d');
            $endDate = formatDateTime(isset($paramRequest['end_date']) ? $paramRequest['end_date'] : '', 'Y-m-d');
            //create query time check teacher or student empty study in time
            $currentClass = $this->getRepository()->findById($id);
            $oldTargetGradeId = $currentClass->target_grade_id;
            $oldTargetSubjectId = $currentClass->target_subject_id;
            if ($currentClass->status == TutorClass::CLASS_FINISHED) {
                return $this->renderJsonError([], getMessage('class_has_been_finished_cant_update_info'));
            }
            $currentProgramId = $this->tutorClassProgramRepository->getCurrentProgram($id)->program_id ?? 0;

            $paramRequest['start_date'] = $startDate;
            $paramRequest['end_date'] = $endDate;

            // lấy danh sách học sinh hiện tại so sánh với danh sách truyền vào
            $listStudent = array_unique($this->getRepository()->getStudents($id)->toArray());
            $lstStudentInsert = array_diff($paramRequest['student_ids'], $listStudent);

            $validateStudents = $this->getRepository()->checkTimeInFuture($lstStudentInsert, 'student', $paramRequest);
            if ($validateStudents) {
                return $this->renderJsonError([], getMessage('time_study_student_error'));
            }

            $validateTeacher = $this->getRepository()->checkTimeInFuture([$paramRequest['teacher_id']], 'teacher', $paramRequest, $id);
            if ($validateTeacher) {
                return $this->renderJsonError([], getMessage('time_study_teacher_error'));
            }

            $responseUpdate = $currentClass->update($paramRequest);
            if (!$responseUpdate) {
                DB::rollBack();
                return $this->renderJsonError([],getMessage('update_error'));
            }

            if (isset($paramRequest['student_ids'])) {
                $students = $this->tutorStudentRepository->search()->whereIn('id', $paramRequest['student_ids'])->get()->keyBy('id');
                $lstStudentDelete = array_diff($listStudent, $paramRequest['student_ids']);
                $lstStudentInsert = array_diff($paramRequest['student_ids'], $listStudent);

                // $lstStudentUpdate = array_intersect($paramRequest['student_ids'], $listStudent);
                if (!empty($lstStudentInsert)) {
                    $listInsertStudent = [];
                    foreach ($lstStudentInsert as $idStudent) {
                        array_push($listInsertStudent, [
                            'class_id' => $id,
                            'student_id' => $idStudent,
                            'program_id' => $currentProgramId,
                            'create_date' => formatDateTime('', 'Y-m-d H:i:s')
                        ]);
                    }
                    if (isset($students[$idStudent])) {
                        $currentTargetGradeIds = $students[$idStudent]->target_grade_ids;
                        $currentTargetSubjectIds = $students[$idStudent]->target_subject_ids;
                        $targetGradeIds = [];
                        $targetSubjectIds = [];
                        if ($currentTargetGradeIds) {
                            $targetGradeIds = explode(',', $currentTargetGradeIds);
                        }
                        if (!in_array($paramRequest['target_grade_id'], $targetGradeIds)) {
                            $targetGradeIds[] = $paramRequest['target_grade_id'];
                        }
                        $students[$idStudent]->target_grade_ids = implode(',', $targetGradeIds);
                        if ($currentTargetSubjectIds) {
                            $targetSubjectIds = explode(',', $currentTargetSubjectIds);
                        }
                        if (!in_array($paramRequest['target_subject_id'], $targetSubjectIds)) {
                            $targetSubjectIds[] = $paramRequest['target_subject_id'];
                        }
                        $students[$idStudent]->target_subject_ids = implode(',', $targetSubjectIds);
                        $students[$idStudent]->save();
                    }
                    $this->tutorClassStudentRepository->insert($listInsertStudent);
                }
            } else {
                $lstStudentDelete = $listStudent;
            }

            if (!empty($lstStudentDelete)) {
                foreach ($lstStudentDelete as $idStudent) {
                    $this->tutorClassStudentRepository->deleteByClassStudent($id, $idStudent);
                    $this->tutorStudentRepository->removeGradeAndSubject($idStudent, $oldTargetGradeId, $oldTargetSubjectId);
                }
            }
            $this->updateListSchedule($id, $paramRequest['day_of_weeks'], $startDate, $endDate);

            $listTimeLesson = $this->getRepository()->genDates($id);
            $this->updateClassLesson($id, $listTimeLesson);
            DB::commit();
            return $this->renderJson(['id' => $id], getMessage('update_success'));

        } catch (\Exception $exception) {
            logError($exception);
            DB::rollBack();
            return $this->renderJsonException($exception);
        }
    }

    public function updateListSchedule($idClass, $listDayOfWeeks = [], $startDate , $endDate )
    {
        $listSchedule = $this->tutorClassScheduleRepository->getListByClassId($idClass); // ;->search(['class_id' => $idClass])->get();
        $listIdSchedule = [];
        $lstIdNotDeleted = [];
        foreach ($listSchedule as $schedule) {
            foreach ($listDayOfWeeks as $key => $dayOfWeek) {
                if ($dayOfWeek['day_of_week'] == $schedule->day_of_week
                    && $dayOfWeek['start_time'] == formatDateTime($schedule->start_time, 'H:i')
                    && $dayOfWeek['end_time'] == formatDateTime($schedule->end_time, 'H:i')
                ) {
                    array_push($lstIdNotDeleted, $schedule->id);
                    unset($listDayOfWeeks[$key]);
                }
            }
            array_push($listIdSchedule, $schedule->id);
        }

        try {
            /**
             * Insert schedule
             */
            $listInsert = [];
            foreach ($listDayOfWeeks as $dayOfWeek) {
                array_push($listInsert, [
                    'class_id' => $idClass,
                    'day_of_week' => $dayOfWeek['day_of_week'],
                    'start_time' => $startDate . ' ' . $dayOfWeek['start_time'],
                    'end_time' => $startDate . ' ' . $dayOfWeek['end_time'],
                    'create_date' => formatDateTime('','Y-m-d H:i:s')
                ]);
            }
            if (!empty($listInsert)) $this->tutorClassScheduleRepository->insert($listInsert);
            /**
             * delete schedule not in list
             * list schedule Insert : $listDayOfWeeks
             */
            $lstIdDeleted = array_diff($listIdSchedule, $lstIdNotDeleted);
            foreach ($lstIdDeleted as $idSchedule){
                $this->tutorClassScheduleRepository->findById($idSchedule)->update([
                    'update_date' => formatDateTime('','Y-m-d H:i:s'),
                    getConfig('deleted_name') => getConfig('deleted_active')
                ]);
            }
            return true;
        }catch (\Exception $exception){
            logError($exception);
            return false;
        }
    }

    public function updateClassLesson($idClass, $listTime)
    {
        $timeSearch = formatDateTime('', 'Y-m-d H:i');
        $indexTime = -1;
        foreach ($listTime as $key => $time) {
            if (strtotime($timeSearch) <= strtotime($time['st'])) {
                $indexTime = $key;
                break;
            }
        }
        try {
            //check list lesson next day
            $listLessonNextDay = $this->tutorClassLessonRepository->getListByClassAndTime($idClass, $timeSearch);
            foreach ($listLessonNextDay as $timeLesson) {
                if ($indexTime > -1 || isset($listTime[$indexTime])) {
                    $dataUpdate = [
                        'lesson_start_time' => $listTime[$indexTime]['st'],
                        'lesson_end_time' => $listTime[$indexTime]['et'],
                        'update_date' => formatDateTime('', 'Y-m-d H:i:s')
                    ];
                    $indexTime++;
                } else {
                    $dataUpdate = [
                        getConfig('deleted_name') => getConfig('deleted_active'),
                        'update_date' => formatDateTime('', 'Y-m-d H:i:s')
                    ];
                }
                $this->tutorClassLessonRepository->updateInfo($timeLesson->id, $dataUpdate);
            }
            return true;
        } catch (\Exception $exception) {
            logError($exception);
            return false;
        }
    }
}
