<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorProgramRepository;
use App\Repositories\CocDmMonHocRepository;
use App\Repositories\CocDmKhoiRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TutorProgramController extends CrmController
{
    protected $cocDmKhoiRepository;
    protected $cocDmMonHocRepository;

    public function __construct(
        TutorProgramRepository $tutorProgramRepository,
        CocDmMonHocRepository $cocDmMonHocRepository,
        CocDmKhoiRepository $cocDmKhoiRepository
        )
    {
        $this->setRepository($tutorProgramRepository);
        $this->cocDmMonHocRepository = $cocDmMonHocRepository;
        $this->cocDmKhoiRepository = $cocDmKhoiRepository;
    }

    public function index(Request $request)
    {
        $accept_params = [
            'target_subject_id',
            'target_grade_id',
            'level',
            'status',
            'apply_status',
            'page',
            'perPage',
            'id',
            'name',
            'product_code'
        ];
        $res = $this->getRepository()->getList($request->only($accept_params));

        return $this->renderJsonToPage($res['data'], $res['total']);
    }

    public function show($id)
    {
        try {
            $itemModel = $this->getRepository()->findById($id);
            if (empty($itemModel)) {
                return $this->renderJsonError([], getMessage('not_found_detail'));
            }
            $classRunning = $this->getRepository()->getClassRunning($id);

            return $this->renderJson(array_merge($itemModel->toArray(), ['class_running' => $classRunning]));
        } catch (\Exception $exception) {
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function store(Request $request)
    {
        $valid = $this->getRepository()
            ->getValidate()
            ->storeValidate($request->all());
        if($valid->fails()){
            return $this->renderJsonError($valid->errors());
        }

        return parent::store($request);
    }

    public function update($id, Request $request)
    {
        $valid = $this->getRepository()
            ->getValidate()
            ->updateValidate($request->all());;
        if($valid->fails()){
            return $this->renderJsonError($valid->errors());
        }

        $itemModel = $this->getRepository()->findById($id);

        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }

        // Do not change status active to inactive when has Class running 
        $countClassRunning = $this->getRepository()->getClassRunning($itemModel->id)->count();
        if (
            $itemModel->status == getConfig('tutor_program.status.active')
            && $request->status == getConfig('tutor_program.status.inactive')
            && $countClassRunning > 0
        ) {
            return $this->renderJsonError([
                'status' => 'Có ' . $countClassRunning . ' lớp đang dạy áp dụng khung chương trình này.',
            ]);
        }

        DB::beginTransaction();
        try {
            $status = $itemModel->fill($request->all())->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJson($id, getMessage('update_error'));
            }
            DB::commit();
            $item = $this->getRepository()->find($id);
            return $this->renderJson($item, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function getDataFilter()
    {
        $subjects = $this->cocDmMonHocRepository->getListForFilter();
        $grades = $this->cocDmKhoiRepository->getListForFilter();
        $levels = getConfig('program_levels');
        $statuses = array_values(getConfig('tutor_teacher.status'));
        $apply_statuses = getConfig('program_apply_statuses.data_filter');
        $listData = [
            'subjects' => $subjects,
            'grades' => $grades,
            'levels' => $levels,
            'statuses' => $statuses,
            'apply_statuses' => $apply_statuses,
        ];

        return $this->renderJson($listData);
    }

    public function searchByNameOrId(Request $request)
    {
        try {
            $res = $this->getRepository()->searchByNameOrId($request->only(['name']));

            return $this->renderJson($res);
        } catch (\Exception $exception) {
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }
}
