<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorClassLessonRepository;
use App\Repositories\TutorProgramRepository;
use App\Repositories\TutorProgramLessonRepository;
use App\Repositories\TutorClassProgramRepository;
use App\Repositories\TutorClassRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutorClassLessonController extends CrmController
{
    protected $tutorClassProgramRepository;
    protected $tutorProgramRepository;
    protected $tutorClassRepository;
    protected $tutorProgramLessonRepository;

    /**
     * @param TutorClassLessonRepository $tutorClassLessonRepository
     */

    public function __construct(
        TutorClassLessonRepository $tutorClassLessonRepository,
        TutorProgramRepository $tutorProgramRepository,
        TutorClassRepository $tutorClassRepository,
        TutorClassProgramRepository $tutorClassProgramRepository,
        TutorProgramLessonRepository $tutorProgramLessonRepository
        )
    {
        $this->setRepository($tutorClassLessonRepository);
        $this->tutorClassProgramRepository = $tutorClassProgramRepository;
        $this->tutorProgramRepository = $tutorProgramRepository;
        $this->tutorClassRepository = $tutorClassRepository;
        $this->tutorProgramLessonRepository = $tutorProgramLessonRepository;
    }

    public function store(Request $request)
    {
        DB::BeginTransaction();
        try {
            $valid = $this->getRepository()
                ->getValidate()
                ->storeValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
            $class = $this->tutorClassRepository->getModel()->find($request->class_id);

            $programLesson = $this->getRepository()->search()
                ->join('tutor_class_program', 'tutor_class_program.class_id', '=', 'tutor_class_lesson.class_id')
                ->whereNull('tutor_class_program.apply_end_datetime')
                ->where('tutor_class_lesson.class_id', $request->class_id)
                ->orderBy('tutor_class_lesson.id', 'DESC')
                ->select('tutor_class_program.program_id', 'tutor_class_lesson.tutor_program_lesson_id')
                ->first();
            $tutorProgramLessonsIds = $this->tutorProgramLessonRepository->search([ 'program_id' => $programLesson->program_id])->pluck('id');
            $countLessonCurrent = $this->getRepository()->search([ 'class_id' => $request->class_id ])->whereIn('tutor_program_lesson_id', $tutorProgramLessonsIds)->count();

            $date = $this->getRepository()->getNextTimeFreeOfClass($class);
            if (!isset($date)) {
                return $this->renderJsonError([], getValidation('tutor_class.can_not_gen_schedule'));
            }

            $arr = [
                'lesson_start_time' => $date['st'],
                'lesson_end_time' => $date['et'],
                'position' => $countLessonCurrent + 1,
                'lesson_type' => getConfig('class_lesson_type.active'),
                'is_custom' => getConfig('class_lesson_type.active'),
                'tutor_program_lesson_id' => $programLesson->tutor_program_lesson_id
            ];

            $dataCreate = $this->getRepository()->create(array_merge($request->all(), $arr));
            // also need to dec num_of_lesson prop
            $class->num_of_lesson = $class->num_of_lesson + 1;
            $class->save();

            DB::commit();
            $item = $this->getRepository()->find($dataCreate->getKey());

            return $this->renderJson($item, getMessage('create_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonException($exception);
        }
    }

    public function update($id, Request $request)
    {
        $valid = $this->getRepository()
            ->getValidate()
            ->updateValidate($request->all());
        if($valid->fails()){
            return $this->renderJsonError($valid->errors());
        }

        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }

        DB::beginTransaction();
        try {
            $status = $itemModel->fill(array_merge(['is_custom' => getConfig('class_lesson_type.active')], $request->all()))->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJson($id, getMessage('update_error'));
            }
            DB::commit();
            $item = $this->getRepository()->find($id);
            return $this->renderJson($item, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function sortPositionByClass($classId, Request $request)
    {
        $ids = explode(',', $request->ids);
        $listClassLessons = $this->getRepository()->search([ 'class_id' => $classId ])
            ->whereIn('id', $ids)->orderBy('position')->get()->keyBy('id');

        $arrTimeOfLessons = [];
        foreach ($listClassLessons as $lesson) {
            $arrTimeOfLessons[] = [
                'lesson_start_time' => $lesson->lesson_start_time,
                'lesson_end_time' => $lesson->lesson_end_time,
            ];
        }
        $listData = [];
        DB::BeginTransaction();
        try {
            foreach ($ids as $key => $id) {
                if (!isset($listClassLessons[$id])) {
                    return $this->renderJsonError([], getMessage('update_error'));
                }
                $listClassLessons[$id]->update([
                    'position' => $key + 1,
                    'lesson_start_time' => $arrTimeOfLessons[$key]['lesson_start_time'],
                    'lesson_end_time' => $arrTimeOfLessons[$key]['lesson_end_time'],
                ]);
                $listData[] = [
                    'id' => $listClassLessons[$id]->id,
                    'position' => $key + 1,
                ];
            }
            DB::commit();

            return $this->renderJson($listData, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->renderJsonException($exception);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy($id)
    {
        $itemModel = $this->getRepository()->findById($id);
        if (empty($itemModel)) {
            return $this->renderJsonError([], getMessage('not_found_data'), 200);
        }
        $class = $this->tutorClassRepository->getModel()->find($itemModel->class_id);
        
        if($class->status == getConfig('tutor_class.status.finish.id')) {
            return $this->renderJsonError([], getValidation('tutor_class_lesson.class_finised'));
        }
        DB::beginTransaction();
        try {
            $nextLessons = $this->getRepository()
                ->search()
                ->where('class_id', $itemModel->class_id)
                ->where('lesson_start_time', '>', $itemModel->lesson_start_time)->get();
            $tempStartTime = $itemModel->lesson_start_time;
            $tempEndTime = $itemModel->lesson_end_time;
            foreach ($nextLessons as $lesson) {
                $oldStartTime = $lesson->lesson_start_time;
                $oldEndTime = $lesson->lesson_end_time;
                $lesson->lesson_start_time = $tempStartTime;
                $lesson->lesson_end_time = $tempEndTime;
                $tempStartTime = $oldStartTime;
                $tempEndTime = $oldEndTime;
                $lesson->save();
            }
            $itemModel->deleted = getConfig('deleted_active');
            $status = $itemModel->save();
            if (!$status) {
                DB::rollBack();
                return $this->renderJsonError([], getMessage('delete_error'));
            }
            // also need to dec num_of_lesson prop
            $class->num_of_lesson = $class->num_of_lesson - 1;
            $class->save();
            DB::commit();
            return $this->renderJson([], getMessage('delete_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }

    public function suspend($id, Request $request)
    {
        $valid = $this->getRepository()
                ->getValidate()
                ->suspendValidate($request->all());
            if($valid->fails()){
                return $this->renderJsonError($valid->errors());
            }
        DB::beginTransaction();
        try {
            $checkSuspendTime = $this->getRepository()->checkChangeTime($request->all());
            if ($checkSuspendTime) {
                return $this->renderJsonError([], getMessage('duplicate_schedule'));
            }
            $res = $this->getRepository()->suspend($id, $request->all());
            if (!$res) {
                return $this->renderJsonError([], getMessage('cant_update_time_suspend'));
            }
            DB::commit();
            return $this->renderJson($res, getMessage('update_success'));
        } catch (\Exception $exception) {
            DB::rollBack();
            logError($exception);
            return $this->renderJsonError([], $exception->getMessage());
        }
    }
}
