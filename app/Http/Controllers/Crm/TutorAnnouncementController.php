<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Crm\CrmController;
use App\Repositories\TutorAnnouncementRepository;

class TutorAnnouncementController extends CrmController
{
    public function __construct(TutorAnnouncementRepository $tutorAnnouncementRepository)
    {
        $this->setRepository($tutorAnnouncementRepository);
    }
}
