<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Repositories\CoreUserDeviceRepository;
use App\Repositories\TutorCoreConfigRepository;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class ApiAuthenticate extends Middleware
{
    protected $repository;
    protected $tutorCoreConfigRepository;
    protected $loginController;

    public function __construct(Auth $auth, CoreUserDeviceRepository $coreUserDeviceRepository, TutorCoreConfigRepository $tutorCoreConfigRepository)
    {
        parent::__construct($auth);
        $this->repository = $coreUserDeviceRepository;
        $this->tutorCoreConfigRepository = $tutorCoreConfigRepository;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getTutorCoreConfigRepository()
    {
        return $this->tutorCoreConfigRepository;
    }

    public function handle($request, Closure $next)
    {
        $bearerToken = $request->bearerToken();
        $responseToken = parserTokenApi($bearerToken);
        if(!$responseToken['success']){
            return $this->errorAuth();
        }
        $objUserDevice = $responseToken['user_device'];
        try{
            if ($this->tokenExpired($objUserDevice))
                return $this->errorAuth(401, 'Token Expire');
            $config = $this->getTutorCoreConfigRepository()->getByKey('IS_ENABLE_TUTOR');
            if ($config && $config->value == getConfig('is_enable_tutor.disabled')) {
                return $this->errorAuth(403, 'Tutor is disabled');
            }
            $getUser = $this->getRepository()->getUserAndDevice($objUserDevice);
            if (empty($getUser)) {
                return $this->errorAuth(403,getConstant('student_not_found'));
            }
            apiGuard()->setUser($getUser);
            return $next($request);
        }catch(\Exception $exception){
            logError($exception);
            return $this->errorAuth(403, $exception->getMessage());
        }
    }
    private function errorAuth($statusCode = 403, $message = ''){
        return abort(response()->json([
            'statusCode' => $statusCode,
            'success' => false,
            'message' => $message ? $message: getMessage('unauthorized'),
        ]));
    }
    public function tokenExpired($objUserDevice)
    {
        if (Carbon::parse($objUserDevice->exp) < Carbon::now()) {
            return true;
        }
        return false;
    }
}
