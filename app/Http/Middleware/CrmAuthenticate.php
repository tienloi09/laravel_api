<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Repositories\CoreUserRepository;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class CrmAuthenticate extends Middleware
{
    protected $repository;
    protected $loginController;

    public function __construct(
        Auth $auth,
        CoreUserRepository $coreUserRepository
        )
    {
        parent::__construct($auth);
        $this->repository = $coreUserRepository;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function handle($request, Closure $next)
    {
        $bearerToken = $request->bearerToken();
        $responseToken = parserTokenApi($bearerToken);
        if(!$responseToken['success']){
            return $this->errorAuth();
        }
        $objUserDevice = $responseToken['user_device'];
        if ($this->tokenExpired($objUserDevice))
            return $this->errorAuth(401, 'Token Expire');
        $getUser = $this->repository->find($objUserDevice->id);
        if (empty($getUser)) {
            return $this->errorAuth();
        }
        apiGuard()->setUser($getUser);
        return $next($request);
    }
    private function errorAuth($statusCode = 403, $message = ''){
        return abort(response()->json([
            'statusCode' => $statusCode,
            'success' => false,
            'message' => $message ? $message: getMessage('unauthorized'),
        ]));
    }
    public function tokenExpired($objUserDevice)
    {
        if (Carbon::parse($objUserDevice->exp) < Carbon::now()) {
            return true;
        }
        return false;
    }
}
