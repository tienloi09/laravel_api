<?php

use Illuminate\Support\Str;

return [
'message' => [
    'not_found_data' => 'Not found data',
    'create_success' => 'Create success',
    'create_error' => 'Create error',
    'update_success' => 'Cập nhật thành công',
    'update_error' => 'Cập nhật thất bại',
    'delete_success' => 'Delete success',
    'delete_error' => 'Delete failed',
    'not_found_detail' => 'Not found detail',
    'token_not_found' => 'Not found token',
    'unauthorized' => 'Unauthorized',
    'not_found_request' => 'Không tìm thấy thông tin môn gửi lên',
    'not_found_class' => 'Không tìm thấy thông tin lớp',
    'not_found_config' => 'Không tìm thấy thông tin config',
    'student_not_found' => 'Không tìm thấy thông tin học sinh',
    'lesson_not_found_video' => 'Buổi học vừa mới kết thúc ít phút. Bạn có thể xem lại video bài học trong 1 đến 2 giời tới',
    'program_was_added' => 'Khung chương trình đã được thêm trước đó',
    'class_await_not_update' => 'Lớp học đã :status không thể chỉnh sửa',
    'class_has_been_started' => 'Lớp học đã bắt đầu không thể thay đổi khung chương trình',
    'class_has_been_finished' => 'Lớp học đã kết thúc không thể thay đổi khung chương trình',
    'time_study_student_error' => 'Thời gian học của học sinh bị trùng lặp',
    'time_study_teacher_error' => 'Thời gian dạy của giáo viên bị trùng lặp',
    'cant_update_time_suspend' => 'Không thể sắp xếp lịch học cho buổi học tiếp theo.',
    'duplicate_schedule' => 'Trùng với lịch dạy, học của giáo viên và học sinh!',
    'lms_video_not_found' => 'Không tìm thấy thông tin bài giảng',
    'class_has_been_finished_cant_update_info' => 'Lớp đã kết thúc, không thể thay đổi thông tin.',
],
    'day_of_week' => [
        0 => 'Chủ nhật',
        1 => 'Thứ 2',
        2 => 'Thứ 3',
        3 => 'Thứ 4',
        4 => 'Thứ 5',
        5 => 'Thứ 6',
        6 => 'Thứ 7',
    ],
    'list_mon_hoc_avatar' => [
        1 => 'tutor/math.png',
        2 => 'assets/imgs/thpt/thpt_new/hat.png',
        3 => 'assets/imgs/thpt/thpt_new/hat.png',
        4 => 'assets/imgs/thpt/thpt_new/hat.png',
        5 => 'tutor/english.png',
        6 => 'assets/imgs/thpt/thpt_new/hat.png',
        7 => 'assets/imgs/thpt/thpt_new/hat.png',
        8 => 'assets/imgs/thpt/thpt_new/hat.png',
        9 => 'assets/imgs/thpt/thpt_new/hat.png',
        10 => 'assets/imgs/thpt/thpt_new/hat.png',
        11 => 'assets/imgs/thpt/thpt_new/hat.png',
        12 => 'assets/imgs/thpt/thpt_new/hat.png',
        13 => 'assets/imgs/thpt/thpt_new/hat.png',
        14 => 'assets/imgs/thpt/thpt_new/hat.png',
        15 => 'assets/imgs/thpt/thpt_new/hat.png',
        100 => 'assets/imgs/thpt/thpt_new/hat.png',
        101 => 'assets/imgs/thpt/thpt_new/hat.png',
    ],
    'week_maps' => [
        0 => [
            'short' => 'SU',
            'full' => 'Sunday',
        ],
        1 => [
            'short' => 'MO',
            'full' => 'Monday',
        ],
        2 => [
            'short' => 'TU',
            'full' => 'Tuesday'
        ],
        3 => [
            'short' => 'WE',
            'full' => 'Wednesday'
        ],
        4 => [
            'short' => 'TH',
            'full' => 'Thursday'
        ],
        5 => [
            'short' => 'FR',
            'full' => 'Friday'
        ],
        6 => [
            'short' => 'SA',
            'full' => 'Saturday'
        ],
    ],
];

