<?php
return [
    'required' => 'Vui lòng nhập trường :attribute',
    'integer' => 'Vui lòng nhập số ',
    'phone_exists' => 'Số :attribute đã tồn tại',
    'phone_length' => ':attribute số lượng ký tự không thoả mãn',
    'date_month_year' => 'Định dạng sai: d-m-Y',
    'date_time' => 'Định dạng sai: Y-m-d H:i',
    'duplicate' => 'Trùng giá trị có sẵn',
    'tutor_class' => [
        'can_not_gen_schedule' => 'Không chọn được lịch học phù hợp',
        'class_has_been_finished' => 'Lớp học đã kết thúc',
    ],
    'tutor_class_program' => [
        'class_id_not_found' => 'Không tìm thấy thông tin lớp',
        'start_lesson_id_not_found' => 'Thiếu buổi học bắt đầu',
    ],
    'tutor_program' => [
        'class_id_missing' => 'Thiếu class_id',
        'program_id_missing' => 'Thiếu program_id',
    ],
    'tutor_rating_comment' => [
        'class_not_found' => 'Không tìm thấy thông tin lớp',
        'lesson_not_found' => 'Không tìm thấy thông tin buổi học',
    ],
    'tutor_class_lesson' => [
        'class_finised' => 'Không cho cập nhật bài học của lớp đã kết thúc',
    ]
];

