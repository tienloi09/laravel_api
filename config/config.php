<?php

use Illuminate\Support\Str;

return [
    'deleted_active' => 1,
    'deleted_failed' => 0,
    'deleted_name' => 'deleted',
    'created_at' => 'create_date',
    'updated_at' => 'update_date',
    'site_id' => 2,
    'list_subject_id' =>[
        'math' => 1,
        'english' => 5
    ],
    'day_of_week' => [
        '1' => [ 'id' => 1, 'name' => 'Thứ 2' ],
        '2' => [ 'id' => 2, 'name' => 'Thứ 3' ],
        '3' => [ 'id' => 3, 'name' => 'Thứ 4' ],
        '4' => [ 'id' => 4, 'name' => 'Thứ 5' ],
        '5' => [ 'id' => 5, 'name' => 'Thứ 6' ],
        '6' => [ 'id' => 6, 'name' => 'Thứ 7' ],
        '0' => [ 'id' => 0, 'name' => 'Chủ nhật' ],
    ],
    'lms_type' => [
        'default' => '0',
        'revise' => '1',
        'practice' => '2',
        'homework' => '3',
    ],
    'program_lesson_section' =>[
        'is_required' => [
            'active' => 1,
            'inActive' => 0
        ],
    ],
    'model_hidden' => [
        'create_at',
        'update_at',
        'create_user',
        'update_user',
        'delete'
    ],
    'mission_completed_status' => [
        'unfinished' => 0,
        'complete' => 1,
    ],
    'tutor_teacher' => [
        'level' =>[
            'dai_hoc'=> [
            'id' => 1,
            'name' => 'Đại học'
        ],
            'cao_dang'=> [
            'id' => 2,
            'name' => 'Cao đẳng'
        ],
            'thac_si'=> [
            'id' => 3,
            'name' => 'Thạc sĩ'
        ],
            'tien_si'=> [
            'id' => 4,
            'name' => 'Tiến Sĩ'
        ],
            'pho_tien_si'=> [
            'id' => 5,
            'name' => 'Phó giáo sư'
        ],
            'giao_su'=> [
            'id' => 6,
            'name' => 'Giáo sư'
        ]
        ],
        'status' => [
            'active' => [
                'id' => 1,
                'name' => 'Active'
            ],
            'inactive' =>[
                'id' => 0,
                'name' => 'Inactive'
            ]
        ],
        'status_in_class' => [
            'running' => [
                'id' => 1,
                'name' => 'Đang có lớp dạy'
            ],
            'not_running' => [
                'id' => 2,
                'name' => 'Chưa có lớp dạy'
            ]
        ],
    ],
    'error_validate' => [
        'errorPhoneDouble' => 'Số điện thoại đã tồn tại trên hệ thống.',
        'errorPhoneFormat' => 'Sai định dạng số điện thoại.',
        'errorPhoneLength' => 'Vui lòng nhập số điện thoại.',
        'errorLevelLength' => 'Vui lòng chọn trình độ.',
        'errorGradeLength' => 'Vui lòng chọn lớp đăng ký.',
        'errorSubjectLength' => 'Vui lòng chọn môn đăng ký.',
        'errorNameLength' => 'Vui lòng nhập họ tên',
        'errorYearsOfExp' => 'Vui lòng nhập số năm kinh nghiệm',
        'errorStatus' => 'Vui lòng chọn trạng thái',
    ],
    'per_page' => 10,
    'program_levels' => [
        [
            'id' => 1,
            'name' => 'Cơ bản',
        ],
        [
            'id' => 2,
            'name' => 'Nâng cao',
        ],
    ],
    'program_apply_statuses' => [
        'data_filter' => [
            [
                'id' => 1,
                'name' => 'Có lớp đang áp dụng',
            ],
            [
                'id' => 2,
                'name' => 'Chưa có lớp áp dụng',
            ],
        ],
        'is_using' => '1',
        'not_using' => '2',
    ],
    'tutor_class' => [
        'status' => [
            'running' => ['id' => 1, 'name' => 'Đang diễn ra'],
            'await' => ['id' => 0, 'name' => 'Chờ khai giảng'],
            'finish' => ['id' => 2, 'name' => 'Đã kết thúc'],
        ],
    ],
    'tutor_program' => [
        'status' => [
            'active' => ['id'=> 1, 'name' => 'Hoạt động'],
            'inactive' => ['id'=> 0, 'name' => 'Ẩn'],
        ]
    ],
    'tutor_program_section' => [
        'type' => [
            'video_online' => [
                'id' => 0,
                'name' => 'Video trực tuyến',
                'key' => 'video_online'
            ],
            'practice' => [
                'id' => 4,
                'name' => 'Bài luyện tập',
                'key' => 'practice',
            ],
            // 'revise' => [
            //     'id' => 2,
            //     'name' => 'Bài ôn tập',
            //     'key' => 'revise',
            // ],
            'video' => [
                'id' => 1,
                'name' => 'Bài video',
                'key' => 'video',
            ],
            'document' => [
                'id' => 3,
                'name' => 'File tài liệu',
                'key' => 'document',
            ]
        ],
        'software_used' =>[
            'classin' => ['id' => 0, 'name' => 'Classin'],
            'google_meet' => ['id' => 1, 'name' => 'Google meet'],
        ]
    ],
    'tutor_rating_comment' => [
        'status' => [
            'deActive' => ['id' => 0, 'name' => 'Ẩn', 'score' => 2],
            'active' => ['id' => 1, 'name' => 'Hiện', 'score' => 3],
        ],
    ],
    'tutor_class_lesson_section' =>[
        'NAME_BTVN' => 'BTVN: '
    ],
    'where_search' =>[
        'eq' => '=',
        'or' => 'OR',
        'like' => 'like',
        'in' => 'in'
    ],
    'class_lesson_type' => [
        'active' => 1,
        'inActive' => 0,
    ],
    'is_enable_tutor' => [
        'enabled' => '1',
        'disabled' => '0',
    ],
    'tutor_student' => [
        'value' => [
            'studying' => 1,
            'no_class' => 2,
            'await' => 3,
            'finished' => 4,
        ],
        'status_student' => [
            [
                'id' => 1,
                'name' => 'Đang học',
                'key' => 'studying'
            ],
            [
                'id' => 2,
                'name' => 'Chờ lớp',
                'key' => 'no_class'
            ],
            [
                'id' => 3,
                'name' => 'Chờ khai giảng',
                'key' => 'await'
            ],
            [
                'id' => 4,
                'name' => 'Đã nghỉ',
                'key' => 'finished'
            ]
        ]
    ],
];

