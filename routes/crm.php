<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth.crm')->group(function (){
    // Teacher manager
    Route::get('teacher-manager/search','Crm\TutorTeacherController@search')->name('teacher-manager.search');
    Route::get('teacher-manager/logs/{teacherId}','Crm\TutorTeacherController@getListLogs');
    Route::resource('teacher-manager','Crm\TutorTeacherController');

    // Student
    Route::post('student/set-number-lesson-for-student','Crm\TutorStudentController@setLessonForStudent');
    Route::get('student/search-student-for-add-to-class','Crm\TutorStudentController@searchForAddToClass')->name('student.searchForAddToClass');
    Route::get('student/logs/{studentId}','Crm\TutorStudentController@getListLogs');
    Route::get('student/pixel-logs/{studentId}','Crm\TutorStudentController@getPixelLogs');
    Route::get('student/mission-inprogress/{studentId}','Crm\TutorStudentController@getInprogress');
    Route::resource('student','Crm\TutorStudentController');

    // TutorProgram
    Route::get('program/get-data-filter','Crm\TutorProgramController@getDataFilter')->name('program.getDataFilter');
    Route::get('program/search','Crm\TutorProgramController@searchByNameOrId')->name('program.searchByNameOrId');
    Route::resource('program','Crm\TutorProgramController')->only([ 'index', 'show', 'store', 'update' ]);

    // ProgramLesson
    Route::get('program-lesson/get-list-by-program/{programId}','Crm\TutorProgramLessonController@getListByProgram')->name('program-lesson.getListByProgram');
    Route::put('program-lesson/sort-position-by-program/{programId}','Crm\TutorProgramLessonController@sortPositionByProgram')->name('program-lesson.sortPositionByProgram');
    Route::resource('program-lesson','Crm\TutorProgramLessonController')->only([ 'store', 'update', 'destroy' ]);

    // ProgramLessonSection
    Route::put('program-lesson-section/sort-position-by-program-lesson/{programLessonId}','Crm\TutorProgramLessonSectionController@sortPositionByProgramLesson')->name('program-lesson-section.sortPositionByProgramLesson');
    Route::resource('program-lesson-section','Crm\TutorProgramLessonSectionController')->only([ 'store', 'update', 'destroy' ]);

    //Class
    Route::get('class/get-list-filter','Crm\TutorClassController@getListFilter')->name('class.get-list-filter');
    Route::get('class/get-program-will-be-add','Crm\TutorClassController@getProgramWillBeAdd')->name('class.get-program-will-be-add');
    Route::post('class/add-program-to-class/{classId}','Crm\TutorClassController@addProgramToClass')->name('class.add-program-to-class');
    Route::resource('class','Crm\TutorClassController');

    //ClassLesson
    Route::put('class-lesson/sort-position-by-class/{classId}','Crm\TutorClassLessonController@sortPositionByClass')->name('class-lesson.sortPositionByClass');
    Route::put('class-lesson/suspend/{classLessonId}','Crm\TutorClassLessonController@suspend')->name('class-lesson.suspend');
    Route::resource('class-lesson','Crm\TutorClassLessonController')->only([ 'store', 'show', 'update', 'destroy' ]);

    //Class section
    Route::put('class-section/sort-position-by-lesson/{lessonId}','Crm\TutorClassLessonSectionController@sortPositionByLesson')->name('class-section.sortPositionByLesson');
    Route::get('class-section/get-list-by-class','Crm\TutorClassLessonSectionController@getListByClass')->name('class-section.get-by-class');
    Route::resource('class-section','Crm\TutorClassLessonSectionController')->only([ 'store', 'update', 'destroy' ]);
    Route::post('file/upload-file','Crm\CoreUserController@uploadFileS3')->name('user.upload-file');

    //Prio Box
    Route::get('prio-box/notify-by-{userId}','Crm\TutorPrioBoxController@getNotifyByUser');
    Route::resource('prio-box','Crm\TutorPrioBoxController')->only([ 'index', 'store', 'show', 'update']);
});
