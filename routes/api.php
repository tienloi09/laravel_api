<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth.api')->group(function (){
    Route::get('class-student/get-list-near-day','Api\TutorClassStudentController@getListNearDay')->name('class-student.get-list-near-day');
    Route::get('class-student/get-list-class','Api\TutorClassStudentController@getClassBySubject')->name('class-student.get-class-by-mon');
    Route::get('class-student/get-list-class-of-student','Api\TutorClassStudentController@getListClassOfStudent')->name('class-student.get-list-class-of-student');
    Route::get('student/get-list-by-user','Api\TutorStudentController@getListByUser')->name('student.get-list-by-user');
    Route::get('student/get-subject-by-user','Api\TutorStudentController@getSubjectByUser')->name('student.get-subject-by-user');
    Route::get('class/find-by-class/{classId}','Api\TutorClassController@findById')->name('class.find-by-id');
    Route::get('program-lesson-section/find-list-section','Api\TutorProgramLessonSectionController@findListProgramLesson')->name('program-lesson-section.find-list-program');
    Route::get('mission-completed/get-list-by-student','Api\TutorMissionCompletedController@getListByStudent')->name('mission-completed.get-list-by-student');
    Route::get('class-lesson/get-lesson-finish','Api\TutorClassLessonController@getListLessonFinish')->name('class-lesson.get-lesson-finish');
    Route::get('class-schedule/get-list-by-user','Api\TutorClassScheduleController@getListByUser')->name('class-schedule.get-list-by-user');
    Route::get('class-lesson/get-list-by-class/{classId}','Api\TutorClassLessonController@getListByClass')->name('class-lesson.get-list-by-class');
    Route::get('class/find-by-class-lesson/{classLessonId}','Api\TutorClassController@findByClassLesson')->name('class.find-by-class-lesson');
    Route::get('result/get-result-by-class/{classId}','Api\TutorResultController@getResultByClassId')->name('result.get-result-by-class-id');
    Route::resource('student','Api\TutorStudentController');
    Route::resource('rating-comment','Api\TutorRatingCommentController');
    Route::get('mission-completed/inprogress','Api\TutorMissionCompletedController@getInprogress')->name('mission-completed.inprogress');
    Route::get('mission-completed/old-missions','Api\TutorMissionCompletedController@getOldMissions')->name('mission-completed.get-old-missions');

    Route::get('class-lesson/get-references/{lessonId}','Api\TutorClassLessonController@getListReferrences')->name('class-lesson.get-referrences');

    Route::resource('prio-box','Api\TutorPrioBoxController')->only([ 'index' ]);
});
Route::get('core-config/get-is-enable-tutor','Api\TutorCoreConfigController@getIsEnableTutor')->name('core-config.get-is-enable-tutor');
